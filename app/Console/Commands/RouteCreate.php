<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use App\Console\Commands\BaseCommand;

class RouteCreate extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geha:route 
                            {module : Module name} 
                            {controller : Controller Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new controller for Geha Project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get module name from terminal
        $module = ucfirst($this->argument('module'));
        
        // Get controller name from terminal
        $controller = ucfirst($this->argument('controller'));

        // Pass to generate new file
        $this->generateFile($module, $controller);

        // Controller create
        $this->info('Routes created successfully');
    }

    /**
     * Get stub file and generate another controller to the project
     * 
     * @param  string $module     Module's name
     * @param  string $controller Controller's name
     * @return string             Content File
     */
    protected function generateFile($module, $controller)
    {
        // Get path stub file to parse
        $stub = $this->getStub('route');

        // Catch your content
        $content = File::get($stub);

        // Create base dir path
        $baseDir = $this->getFilename('route', $module); 

        // Replace placeholder for real content
        $content = str_replace('{{moduleName}}', $module, $content);
        $content = str_replace('{{className}}', $controller, $content);
        $content = str_replace('{{prefix}}', strtolower($controller), $content);

        // New file to generate...
        $file = $baseDir . DS .  'routes.php';

        // Created!
        File::put($file, $content);
    }
}
