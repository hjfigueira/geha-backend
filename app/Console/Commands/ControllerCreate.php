<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use App\Console\Commands\BaseCommand;

class ControllerCreate extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geha:controller 
                            {module : Module name} 
                            {controller : Controller Name}
                            {api? : API Mode}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new controller for Geha Project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get module name from terminal
        $module = ucfirst($this->argument('module'));
        
        // Get controller name from terminal
        $controller = ucfirst($this->argument('controller'));

        // Get controller name from terminal
        $api = ucfirst($this->argument('api'));

        // Pass to generate new file
        $this->generateFile($module, $controller, $api);

        // Controller create
        $this->info('Controller created successfully');
    }

    /**
     * Get stub file and generate another controller to the project
     * 
     * @param  string $module     Module's name
     * @param  string $controller Controller's name
     * @return string             Content File
     */
    protected function generateFile($module, $controller, $api)
    {
        // Get type of controller
        $type = ($api == true) ? 'api' : 'controller';

        // Get path stub file to parse
        $stub = $this->getStub($type);

        // Catch your content
        $content = File::get($stub);

        // Create namespace
        $namespace = $this->getNamespace('Controllers', $module); 

        // Create base dir path
        $baseDir = $this->getFilename('Controllers', $module); 

        // Replace placeholder for real content
        $content = str_replace('{{moduleName}}', $module, $content);
        $content = str_replace('{{namespace}}', $namespace, $content);
        $content = str_replace('{{className}}', $controller, $content);
        $content = str_replace('{{prefix}}', strtolower($controller), $content);

        // New file to generate...
        $file = $baseDir . DS . ucfirst($controller) . 'Controller.php' ;

        // Created!
        File::put($file, $content);
    }
}
