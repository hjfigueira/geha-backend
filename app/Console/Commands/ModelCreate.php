<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use App\Console\Commands\BaseCommand;

class ModelCreate extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geha:model 
                            {module : Module name} 
                            {model : s Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model for Geha Project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (! defined('DS') ){
            define('DS', DIRECTORY_SEPARATOR);
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get module name from terminal
        $module = ucfirst($this->argument('module'));
        
        // Get controller name from terminal
        $model = ucfirst($this->argument('model'));

        // Pass to generate new file
        $this->generateFile($module, $model);

        // Controller create
        $this->info('Controller created successfully');
    }


    /**
     * Get stub file and generate another controller to the project
     * 
     * @param  string $module     Module's name
     * @param  string $model Controller's name
     * @return string             Content File
     */
    protected function generateFile($module, $model)
    {
        // Get path stub file to parse
        $stub = $this->getStub('model');

        // Catch your content
        $content = File::get($stub);

        // Create namespace
        $namespace = $this->getNamespace('Models', $module); 

        // Create namespace
        $baseDir = $this->getFilename('Models', $module); 

        // Replace placeholder for real content
        $content = str_replace('{{moduleName}}', $module, $content);
        $content = str_replace('{{namespace}}', $namespace, $content);
        $content = str_replace('{{className}}', $model, $content);

        // New file to generate...
        $file = $baseDir . DS . ucfirst($model) . 's.php' ;

        // Created!
        File::put($file, $content);
    }
}
