<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;

class BaseCommand extends Command
{

    public function __construct()
    {
        parent::__construct();

        if (! defined('DS') ){
            define('DS', DIRECTORY_SEPARATOR);
        }
    }

	/**
     * Set namespace for new entity 
     * 
     * @param string $type   
     * @param string $module 
     */
    public function getNamespace($type, $module)
    {
        // Create namespace
        $namespace = 'App\\Modules\\'. ucfirst($module) .'\\'. ucfirst($type); 

        return $namespace;
    }

    /**
     * Get stub files controller 
     * 
     * @return 
     */
    public function getStub($option = null)
    {
        switch ($option) {
            case 'controller':
                return __DIR__.'/stubs/controller.stub';
                break;

            case 'api':
                return __DIR__.'/stubs/controller.api.stub';
                break;
            
            case 'route':
                return __DIR__.'/stubs/route.stub';
                break;

            case 'model':
                return __DIR__.'/stubs/model.stub';
                break;

            default:
                return __DIR__.'/stubs/controller.stub';
                break;
        }
    }

    /**
     * 
     * @param  string $type   [description]
     * @param  string $module [description]
     * @return string         [description]
     */
    public function getFilename($type, $module)
    {
        $type = ($type == 'route') ? '' : DS . ucfirst($type);

        // Set the base dir for controllers
        $baseDir   = 'app' . DS . 'Modules'. DS . ucfirst($module) . $type; 

        return $baseDir;
    }
}