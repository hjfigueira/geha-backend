<?php

namespace App\Console\Commands;

use File;
use Artisan;
use Illuminate\Console\Command;
use App\Console\Commands\BaseCommand;

class ModuleCreate extends BaseCommand
{

    protected $command = 'geha';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geha:module 
                            {name : The ID of the user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {       
        $module = ucfirst($this->argument('name'));
    
        $this->appendFileConfig($module);
        
        $this->createDirModules($module);

        $this->createController($module, ucfirst($module));
        
        $this->createAPI($module, 'Api');
        
        $this->createRoute($module, ucfirst($module));
        
        $this->createModel($module, $module);
        
        $this->info('Module created successfully');
    } 

    /**
     * Execute Artisan Command to create a new controller
     * @param string $module     
     * @param string $controller 
     */
    public function createController($module, $controller)
    {
        $params = ['module'     => $module, 
                   'controller' => $controller];

        Artisan::call($this->command . ':controller', $params);
    }

    /**
     * Execute Artisan Command to create a new controller
     * @param string $module     
     * @param string $controller 
     */
    public function createAPI($module, $controller)
    {
        $params = ['module'     => $module, 
                   'controller' => $controller,
                   'api'        => true];

        Artisan::call($this->command . ':controller', $params);
    }

    /**
     * Execute Artisan Command to create a new route
     * @param string $module     
     * @param string $controller 
     */
    public function createRoute($module, $controller)
    {
        $params = ['module'     => $module, 
                   'controller' => $controller];

        Artisan::call($this->command . ':route', $params);
    }

    /**
     * Execute Artisan Command to create a new module
     * @param string $module     
     * @param string $module 
     */
    public function createModel($module, $model)
    {
        $params = ['module' => $module, 
                   'model'  => $model];

        Artisan::call($this->command . ':model', $params);
    }

    /**
     * Create controller, views, models directory
     * 
     * @param  string $module Module's Name
     */
    protected function createDirModules($module)
    {
        $baseDir = 'app' . DS . 'Modules' . DS . $module . DS;

        $modelDir       = $baseDir . 'Models';
        $viewslDir      = $baseDir . 'Views';
        $controllerlDir = $baseDir . 'Controllers';

        File::makeDirectory($modelDir, 0755, true);
        File::makeDirectory($viewslDir, 0755, true);
        File::makeDirectory($controllerlDir, 0755, true);
    }

    /**
     * Add module to file config
     * 
     * @param  string $module 
     */
    protected function appendFileConfig($module)
    {
        $file     = 'config' . DS . 'module.php';
        $handle   = fopen($file, "r");
        $content  = '';
        $line     = '';

        while(! feof($handle) ){

            $line = fgets($handle);

            if (trim($line) == "]") {
                $line = "       '" . ucfirst($module) . 
                        "',". PHP_EOL ."     ]". PHP_EOL;
            }

            $content .= $line;
        }

        File::put($file, $content);
    }
}
