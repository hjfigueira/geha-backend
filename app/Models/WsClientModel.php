<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class WsClientModel extends Model
{
    /**
     * Disable Laravel timestamp
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Connection database
     * @var string
     */
    protected $connection = 'urania-cliente';


    /**
     * Get the next Id
     * @return Integer
     */
    public function nextId()
    {
        $query = "SELECT max(id) AS id FROM ".$this->table;

        $ret = DB::connection($this->connection)->select(DB::raw($query));

        $item = $ret[0];

        return ($item->id == null) ? 1 : intval($item->id) + 1;
    }

    /**
     * Confere se existe ou não um registro com o id especificado
     * @param $id
     */
    public static function checkExistence($id, \Exception $ifError = null){

        try{

            return self::findOrFail($id);

        }catch(ModelNotFoundException $error){
            if($ifError == null){
                throw $error;
            }else{
                throw $ifError;
            }

        }

    }
}