<?php

namespace App\Http\Middleware;

use App\Modules\User\Models\UserModel;
use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{

    public $secondsActivityTimeout = 600;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // debug enquanto não entendo isso...
        return $next($request);

        $token = $request->header('Authorization',false);
        $tokenParts = explode(' ', $token);
        $model = UserModel::isTokenValid(array_get($tokenParts,1,''));
        if($model == null)
        {
            return [
                'status' => 0,
                'auth' => false,
                'code' => 403,
                'message' => 'Token inválido'
            ];
        }

        $lastCall = new \DateTime();
        $lastCall->setTimestamp($model->api_last_call);

        $now = new \DateTime();
        $now->setTimestamp(time());

        $timeDiff = $lastCall->diff($now);
        if($timeDiff->s >= $this->secondsActivityTimeout)
        {
            return [
                'status' => 0,
                'auth' => false,
                'code' => 403,
                'message' => 'Token timeout'
            ];
        }

        $model->updateCalltime();

        return $next($request);
    }
}
