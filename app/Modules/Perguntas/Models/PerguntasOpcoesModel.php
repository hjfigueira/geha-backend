<?php

namespace App\Modules\Perguntas\Models;

use App\Models\WsClientModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PerguntasOpcoesModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Perguntas_Tipos_Opcoes';

    protected $connection = 'urania-admin';



    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
    public static function busca($id)
    {
        $perguntasQueryBuilder = DB::connection('urania-admin')->select("select * from Perguntas_Tipos_Opcoes WHERE id_pergunta = $id");
        $opcoes=$perguntasQueryBuilder;
//        \DB::enableQueryLog();


//        dd(\DB::getQueryLog());



        return $opcoes;
    }


}