<?php

namespace App\Modules\Perguntas\Models;

use App\Models\WsClientModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PrefDistribuicaoAulasEspecificoModal extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Pref_Distribuicao_Aulas_Semana_Especifico';


    public function edit($params)
    {
        DB::table('Pref_Distribuicao_Aulas_Semana_Especifico')
            ->where('id_matriz_curricular', $params['id_matriz'])
            ->update(['tipo_ideal' => $params['id_opcao'],'tipo_aceitavel' => $params['id_aceitavel'],'uso_tipo_aceitavel' => $params['qualidade']]);
    }





}