<?php

namespace App\Modules\Perguntas\Models;

use App\Models\WsClientModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PerguntasModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Perguntas_Tipos';

    protected $connection = 'urania-admin';



    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
    public static function busca()
    {

//        \DB::enableQueryLog();
        $queryMatriz = PerguntasMatrzModel::select('*');
        $matriz=$queryMatriz
                ->groupBy('qtde_aulas')
                ->get();
        foreach ($matriz as $dados_matriz) {

            $queryPrefDistribuicao = PrefDistribuicaoAulasModal::select('*');

            $distribuicao = $queryPrefDistribuicao
                ->where('id_matriz_curricular',$dados_matriz->id)
                ->get();

            foreach ($distribuicao as $dados_distribuicao)
            {
                $opcao_ideal = $dados_distribuicao->tipo_ideal;
                $opcao_aceitavel = $dados_distribuicao->tipo_aceitavel;
                $opcao_qualidade = $dados_distribuicao->uso_tipo_aceitavel;
            }

            if($dados_matriz->qtde_aulas<=6)
            {
                $perguntaQuery = PerguntasModel::select('*');
                $perguntas = $perguntaQuery
                    ->where('ativa', 1)
                    ->where('tipo_pergunta',$dados_matriz->qtde_aulas)
                    ->groupBy('id')
                    ->orderBy('ordem')
                    ->get();
            }
            else{
                $perguntaQuery = PerguntasModel::select('*');
                $perguntas = $perguntaQuery
                    ->where('ativa', 1)
                    ->where('tipo_pergunta',7)
                    ->groupBy('id')
                    ->orderBy('ordem')
                    ->get();
            }

            foreach ($perguntas as $pergunta) {

                $queryGruposOpcoes = PerguntasOpcoesModel::select('*');
                $gruposOpcoes = $queryGruposOpcoes
                    ->where('id_pergunta', $pergunta->id)
                    ->groupBy('grupo_opcoes')
                    ->orderBy('ordem')
                    ->get();
                foreach ($gruposOpcoes as $dados_grupos)
                {
                  $queryOpcoes = PerguntasOpcoesModel::select('id', 'texto','ordem', 'id_tipo');
                  $opcoes = $queryOpcoes
                      ->where('grupo_opcoes',$dados_grupos->grupo_opcoes)
                      ->get();
                    $grupo_opcoes[]=array_merge(
                        [

                            'grupo_opcoes'=>$dados_grupos->grupo_opcoes,
                            'opcoes'=>[$opcoes->toArray()]
                        ]
                    );

                }
                if(isset($opcao_ideal)){
                    $searchReturn[] = array_merge(
                        $pergunta->toArray(),
                        [
                            'opcao_ideal'=>$opcao_ideal,
                            'opcao_aceitavel'=>$opcao_aceitavel,
                            'opcao_qualidade'=>$opcao_qualidade
                        ],
                        [
                            'opcoes'=>$grupo_opcoes
                        ]);
                }
                else
                {
                    $searchReturn[] = array_merge(
                        $pergunta->toArray(),

                        [
                            'opcoes'=>$grupo_opcoes
                        ]);
                }
            }
        }


//        dd(\DB::getQueryLog());



        return $searchReturn;
    }




}