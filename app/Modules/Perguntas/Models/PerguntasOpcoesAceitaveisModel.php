<?php

namespace App\Modules\Perguntas\Models;

use App\Models\WsClientModel;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PerguntasOpcoesAceitaveisModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Perguntas_Tipos_Opcoes_Aceitaveis';

    protected $connection = 'urania-admin';






    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
    public static function busca($id_opcao,$id_pergunta)
    {
        $perguntasQueryBuilder = PerguntasOpcoesAceitaveisModel::select('*');

//        \DB::enableQueryLog();

        $opcoes = $perguntasQueryBuilder
            ->where('tipo_ideal', $id_opcao )
            ->where('id_pergunta', $id_pergunta )
            ->orderBy('ordem')
            ->get();

//        dd(\DB::getQueryLog());



        return $opcoes;
    }


}