<?php

namespace App\Modules\Perguntas\Models;

use App\Models\WsClientModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PrefDistribuicaoAulasModal extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Pref_Distribuicao_Aulas_Semana';

    public function edit($params,$id)
    {
        DB::table('Pref_Distribuicao_Aulas_Semana')
            ->where('id_matriz_curricular', $id)
            ->update(['tipo_ideal' => $params['id_opcao'],'tipo_aceitavel' => $params['id_aceitavel'],'uso_tipo_aceitavel' => $params['qualidade']]);
    }



    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */





}