<?php

namespace App\Modules\Perguntas\Models;

use App\Models\WsClientModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PerguntasEspecificaOpcoesModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Pergunta_Tipo_Especifico_Opcoes';

    protected $connection = 'urania-admin';



    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
    public static function busca()
    {
        $perguntasQueryBuilder = DB::connection('urania-admin')->select("select * from Perguntas_Tipo_Especifico_Opcoes");
        $opcoes=$perguntasQueryBuilder;
//        \DB::enableQueryLog();


//        dd(\DB::getQueryLog());



        return $opcoes;
    }


}

