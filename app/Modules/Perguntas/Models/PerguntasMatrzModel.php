<?php
namespace App\Modules\Perguntas\Models;

use App\Models\WsClientModel;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PerguntasMatrzModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Matriz_Curricular';





    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
    public static function busca()
    {
        $searchReturn = [];
        $matrizQueryBuilder = PerguntasMatrzModel::select('*');

//        \DB::enableQueryLog();

        $perguntas = $matrizQueryBuilder
            ->groupBy('qtde_aulas')
            ->get();

//        dd(\DB::getQueryLog());



        return $perguntas;
    }


}