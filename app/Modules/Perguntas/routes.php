<?php
/*
|--------------------------------------------------------------------------
| Perguntas Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Turnos module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group([
    'prefix' => 'api/perguntas',
    'namespace' => 'App\Modules\Perguntas\Controllers',
    'middleware' => 'auth'
], function () {
    Route::get('all',   ['as' => 'api/perguntas.all',  'uses' => 'ApiController@all']);
    Route::post('change', ['as' => 'api/perguntas.change', 'uses' => 'ApiController@change']);
    Route::post('save', ['as' => 'api/perguntas.save', 'uses' => 'ApiController@save']);
    Route::post('opcoesValidas',   ['as' => 'api/perguntas.opcoesValidas',  'uses' => 'ApiController@opcoesValidas']);
});

