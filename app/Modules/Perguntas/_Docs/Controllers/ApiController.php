<?php
/**
 * @SWG\Get(
 *     path="/perguntas/all",
 *     operationId="all",
 *     summary="Retorna todas as perguntas e opções disponíveis",
 *     description="Exibe todas as perguntas e opções baseadas nos cadastros de duplas",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_gerais.basico"},
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/perguntas/save",
 *     operationId="save",
 *     summary="Salva as opções ideais e aceitáveis",
 *     description="Salva as opções ideais e aceitáveis
 *     [code=1600] = Efetuado com sucesso.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_gerais.basico"},
 *     @SWG\Parameter(
 *         name="id_pergunta",
 *         in="body",
 *         description="Id da Pergunta",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="id_opcao",
 *         in="body",
 *         description="Id da Opção",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="id_aceitavel",
 *         in="body",
 *         description="Id da Aceitável",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="qualidade",
 *         in="body",
 *         description="Valor do Slider",
 *         required=true
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/perguntas/change",
 *     operationId="change",
 *     summary="Exibe as aceitaveis",
 *     description="Exibe as aceitaveis baseando na opção ideal escolhida"
 *     [code=1600] = Efetuado com sucesso.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_gerais.basico"},
 *     @SWG\Parameter(
 *         name="id_pergunta",
 *         in="body",
 *         description="ID da pergunta",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="id_opcao",
 *         in="body",
 *         description="ID da opção",
 *         required=true
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/perguntas/opcoesValidas",
 *     operationId="opcoesValidas",
 *     summary="Retorna a listagem de tipos",
 *     description="Retorna a listagem de tipos"
 *     [code=1600] = Efetuado com sucesso.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_gerais.basico"},
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */