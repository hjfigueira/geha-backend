<?php

namespace App\Modules\Perguntas\Controllers;

use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use App\Modules\Perguntas\Models\PerguntasMatrzModel;
use App\Modules\Perguntas\Models\PerguntasModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesAceitaveisModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesModel;
use App\Modules\Perguntas\Models\PrefDistribuicaoAulasModal;
use App\Modules\PerguntasEspecificas\Models\TipoValidacaoModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\PerguntasBasicas\Models\DiasAulaModel;
use Illuminate\Database\QueryException as QueryException;
use App\Modules\PerguntasBasicas\Models\InformacoesBasicasModel;

/**
 * Class PerguntasBasicas ApiController
 *
 * @package App\Http\Controllers
 *
 * @SWG\Swagger(
 *     basePath="",
 *     host="localhost:8000",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Sample API",
 *         @SWG\Contact(name="Giuliano Sampaio"),
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */
class ApiController extends Controller
{

    protected $model;

    /**
     * 
     */
    public function __construct()
    {
        $this->model = new PerguntasMatrzModel();
    }


    public function all()
    {
        $this->model = new PerguntasModel();
        $perguntas = $this->model->busca();

        return $perguntas;

    }

    public function change(Request $request)
    {
        $params = $request->input();

        if(isset($params['id_opcao']))
        {
            $id_pergunta=$params['id_pergunta'];
            $id_opcao=$params['id_opcao'];
            $this->model = new PerguntasOpcoesModel();
            $idExiste = $this->model->where('id', $id_opcao)->exists();
            if ($idExiste == false) {
                return ['created' => false, 'code' => '1602', 'status' => '00'];
            }
            else
            {

                $this->model = new PerguntasOpcoesAceitaveisModel();
                $aceitaveis=$this->model->busca($id_opcao,$id_pergunta);

                return $aceitaveis;
            }
        }
    }

    public function opcoesValidas(Request $request)
    {
        /*$queryGrupoOpcoes = PerguntasOpcoesModel::select('*');
        $grupo_opcoes = $queryGrupoOpcoes
            ->groupBy('grupo_opcoes')
            ->orderBy('ordem')
            ->get();
        foreach($grupo_opcoes as $dados_grupo_opcoes)
        {
            $queryOpcoes = PerguntasOpcoesModel::select('id','texto','ordem','id_tipo');
            $opcoes = $queryOpcoes
                ->where('grupo_opcoes',$dados_grupo_opcoes->grupo_opcoes)
                ->get();
            $searchReturn[] = array_merge(['grupo_opcoes'=>$dados_grupo_opcoes->grupo_opcoes,'id_pergunta'=>$dados_grupo_opcoes->id_pergunta,'opcoes'=>["opcao_ideal"=>$opcoes->toArray()]]);
        }

        return $searchReturn;*/




        $params = $request->input();

        $queryMatriz = PerguntasMatrzModel::select('*');
        $matriz = $queryMatriz
            ->where('id_disciplina', $params['id_disciplina'])
            ->where('id_turma',$params['id_turma'])
            ->get();

        foreach ($matriz as $dados_matriz)
        {
            $queryMatriz = PerguntasMatrzModel::select('*');
            $aulas = $queryMatriz
                ->where('qtde_aulas',$dados_matriz->qtde_aulas)
                ->count();


            if($dados_matriz->qtde_aulas==2){
                $queryTipoValidacao = TipoValidacaoModel::select('*');
                $tipoValidacao = $queryTipoValidacao
                    ->where('minimo_dias_2_aulas','<=',$aulas)
                    ->get();
            }
            else if($dados_matriz->qtde_aulas==3){
                $queryTipoValidacao = TipoValidacaoModel::select('*');
                $tipoValidacao = $queryTipoValidacao
                    ->where('minimo_dias_3_aulas','<=',$aulas)
                    ->get();
            }
            else if($dados_matriz->qtde_aulas==4){
                $queryTipoValidacao = TipoValidacaoModel::select('*');
                $tipoValidacao = $queryTipoValidacao
                    ->where('minimo_dias_4_aulas','<=',$aulas)
                    ->get();
            }
            else if($dados_matriz->qtde_aulas==5){
                $queryTipoValidacao = TipoValidacaoModel::select('*');
                $tipoValidacao = $queryTipoValidacao
                    ->where('minimo_dias_5_aulas','<=',$aulas)
                    ->get();
            }
            else if($dados_matriz->qtde_aulas==6){
                $queryTipoValidacao = TipoValidacaoModel::select('*');
                $tipoValidacao = $queryTipoValidacao
                    ->where('minimo_dias_6_aulas','<=',$aulas)
                    ->get();
            }
            else if($dados_matriz->qtde_aulas>6){
                $queryTipoValidacao = TipoValidacaoModel::select('*');
                $tipoValidacao = $queryTipoValidacao
                    ->where('minimo_dias_mais_6_aulas','<=',$aulas)
                    ->get();
            }
        }

        $tipos_validos = array();
        foreach ($tipoValidacao as $dados_validacao)
        {
            $tipos_validos[] =$dados_validacao->id_tipo;
        }

        $queryGrupoOpcoes = PerguntasOpcoesModel::select('*');
        $grupoOpcoes = $queryGrupoOpcoes
            ->groupBy('grupo_opcoes')
            ->orderBy('ordem')
            ->get();

        foreach($grupoOpcoes as $dados_grupo) {
            $queryOpcoes = PerguntasOpcoesModel::select('*');
            $idExiste = $queryOpcoes
                ->where('grupo_opcoes', $dados_grupo->grupo_opcoes)
                ->exists();

            if ($idExiste != false) {

                $queryOpcoes = PerguntasOpcoesModel::select('id', 'texto', 'tipo_opcao', 'ordem', 'id_tipo');
                $opcoes = $queryOpcoes
                    ->where('grupo_opcoes', $dados_grupo->grupo_opcoes)
                    ->get();
                $opcao = array();
                foreach ($opcoes as $dados_opcoes) {
                    $teste['id'] = $dados_opcoes->id;
                    $teste['texto'] = $dados_opcoes->texto;
                    $teste['tipo_opcao'] = $dados_opcoes->tipo_opcao;
                    $teste['ordem'] = $dados_opcoes->ordem;
                    $teste['id_tipo'] = $dados_opcoes->id_tipo;
                    if (in_array($teste['id_tipo'], $tipos_validos)) {
                        $teste['valido'] = 1;
                    } else {
                        $teste['valido'] = 0;
                    }
                    $opcao[] = array_merge($teste);
                }


                $searchReturn[] = array_merge(['grupos_opcoes' => $dados_grupo->grupo_opcoes, "opcao_ideal" => [$opcao]]);
            }
        }
        return $searchReturn;
    }

    public function save(Request $request)
    {
        $params = $request->input();
        $this->model = new PerguntasModel();
        $pergunta = $this->model->find($params['id_pergunta']);

        $this->model = new MatrizCurricularModel();
        $matriz=$this->model->search($pergunta->tipo_pergunta);
        foreach ($matriz as $dados_matriz)
        {
            $this->model = new PrefDistribuicaoAulasModal();
            $idExiste = $this->model->where('id_matriz_curricular', $dados_matriz->id)->exists();
            if($idExiste==false)
            {
                $this->model->id_matriz_curricular = $dados_matriz->id;
                $this->model->tipo_ideal = $params['id_opcao'];
                $this->model->tipo_aceitavel = $params['id_aceitavel'];
                $this->model->uso_tipo_aceitavel = $params['qualidade'];
                $this->model->save();
                return ['created' => true, 'code' => '1600', 'status' => '01', 'id' => $this->model->id];
            }
            else
            {
                $this->model = new PrefDistribuicaoAulasModal();
                $this->model->edit($params,$dados_matriz->id);
                return ['created' => true, 'code' => '1600', 'status' => '01', 'id' => $this->model->id];


            }
        }
    }




}
