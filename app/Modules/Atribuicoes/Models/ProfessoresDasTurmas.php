<?php

namespace App\Modules\Atribuicoes\Models;

use App\Models\WsClientModel;
use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class ProfessoresDasTurmas extends WsClientModel
{
	// Table in database
	public $table = 'Professores_Das_Turmas';

	public $fillable = [
	    'id_matriz_curricular','professores'
    ];

    /**
     * Relacionamento com Turmas
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function turma(){
        return $this->belongsTo('\App\Modules\Disciplinas\Models\DisciplinasModel');
    }

    /**
     * Relacionamento com as Disciplinas
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function disciplina(){
        return $this->belongsTo('\App\Modules\Disciplinas\Models\DisciplinasModel');
    }

    /**
     * Relacionamento com a matriz
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function matriz(){

        return $this->belongsTo(
            '\App\Modules\MatrizCurricular\Models\MatrizCurricularModel',
            'id_matriz_curricular');

    }

    /**
     * Retorna os professores atribuidos para uma dupla
     * @param $turma
     * @param $disciplina
     * @return array
     */
    public static function getAtribuicoes($dupla)
    {
        $arrayData = [];

        $atribuicoes = $dupla->atribuicoes()->get();

        foreach($atribuicoes as $atribuicao)
        {
            $arrayData[] = json_decode($atribuicao->professores);
        }

        return $arrayData;
    }

    /**
     * Função para buscar e retornar Duplas
     * @param array $filter
     * @return array
     */
    public static function search(array $filter = [])
    {
        $arrayData = [];
        /** @var Builder $matrizQuery */
        $matrizQuery = MatrizCurricularModel::select('*');
        //Apply filters

        $matrizQuery
            ->filtroGrupoTurma(array_get($filter,'id_grupo_turma',null))
            ->filtroGrupoDisciplina(array_get($filter, 'id_grupo_disciplina', null))
            ->filtroPalavraChave(array_get($filter, 'keyword', null))
            ->filtroPorProfessor(array_get($filter, 'professor', null));

        /** @var Collection $duplas */
        $duplas = $matrizQuery->get();

        foreach($duplas as $dupla){

            $turma      = $dupla->turma()->first();
            $disciplina = $dupla->disciplina()->first();

            $turmaArray = [];
            if($turma != null)
            {$turmaArray = $turma->toArray();}

            $disciplinaArray = [];
            if($disciplina != null)
            {$disciplinaArray = $disciplina->toArray();}

            $arrayData[] = [
                'turma' => $turmaArray,
                'disciplina' => $disciplinaArray,
                'qtde_aulas' => $dupla->qtde_aulas,
                'professores' => self::getAtribuicoes($dupla),
                'id' =>  $dupla->id
            ];

        }

        return $arrayData;
    }

    /**
     * Remove do sistema os registros duplicados para aquela dupla
     */
    public function clearDuplicated()
    {
        $this->where('id_matriz_curricular', $this->id_matriz_curricular)
            ->delete();
    }

    /**
     * Função para relacionar um array de professores com uma dupla
     * @param array $professores
     */
    public function updateDuplas(array $professores = [])
    {
        foreach($professores as $professor)
        {
            self::create([
                'id_matriz_curricular' => $this->id_matriz_curricular,
                'professores' => json_encode($professor)
            ]);
        }

    }

    /**
     * Procura uma atribuição pelo id do professor, dentro do JSON
     * @param $idProfessor
     * @return mixed
     */
    public static function findPorProfessor($idProfessor)
    {
        return self::whereRaw('json_contains(professores, \''.$idProfessor.'\')')
            ->get();
    }
}