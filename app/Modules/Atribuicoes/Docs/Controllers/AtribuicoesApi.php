<?php

/**
 * @SWG\Get(
 *     path="/atribuicoes/all",
 *     summary="Lista de duplas e professores, com filtros",
 *     description="Listagem de duplas disponíveis para relacionar com os professores.",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     tags={"atribuicoes.basico"},
 *     @SWG\Parameter(
 *         description="Filtro, Professores, Grupo de professores",
 *         in="query",
 *         name="filtro_professores[grupo_id]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Professores, Total de aulas",
 *         in="query",
 *         name="filtro_professores[total_aulas]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Professores, Atribuicoes",
 *         in="query",
 *         name="filtro_professores[atribuicao]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Professores, Busca de Texto",
 *         in="query",
 *         name="filtro_professores[keyword]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Duplas, Grupo de Turmas",
 *         in="query",
 *         name="filtros_duplas[id_grupo_turma]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Duplas, Grupo de Disciplina",
 *         in="query",
 *         name="filtros_duplas[id_grupo_disciplina]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Duplas, Professor",
 *         in="query",
 *         name="filtros_duplas[professor]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Duplas, Busca Texto",
 *         in="query",
 *         name="filtros_duplas[keyword]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/atribuicoes/attach",
 *     operationId="attach",
 *     summary="Anexa disciplinas em uma turma através da Matriz Curricular",
 *     description=" Remove os anexos de uma turma, e anexa as novas disciplinas. Status abaixo :
 *     [code=1500] = Efetuado com sucesso.
 *     [code=1501] = Requisição em formato inválido.
 *     [code=1502] = Turma da dupla não existe.
 *     [code=1503] = Disciplina da dupla não existe.
 *     [code=1504] = Erro ao operar no banco de dados.
 *     ",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"atribuicoes.basico"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/AttachAtribuicao"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */