<?php
/*
|--------------------------------------------------------------------------
| User ModuleOperation Routes
|--------------------------------------------------------------------------
|
| All the routes related to the User module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group([
    'middleware' => ['web','auth'],
    'prefix' => 'api/atribuicoes',
    'namespace' => 'App\Modules\Atribuicoes\Controllers'],
    function () {

	    Route::post('attach', 	  ['as' => 'api/atribuicoes.attach',  'uses' => 'ApiControllerAtribuicoes@postAttach']);
        Route::get('all',         ['as' => 'api/atribuicoes.all', 	'uses' => 'ApiControllerAtribuicoes@getAll']);
        
});