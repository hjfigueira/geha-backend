<?php namespace App\Modules\Atribuicoes\Controllers;

use App\Exceptions\WsException;
use App\Http\Controllers\Controller;
use App\Modules\Atribuicoes\Models\ProfessoresDasTurmas;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use App\Modules\Professores\Models\ProfessoresModel;
use App\Modules\Turmas\Models\TurmasModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

/**
 * Class ApiControllerMatrizCurricular
 * @package App\Modules\MatrizCurricular\Controllers
 */
class ApiControllerAtribuicoes extends Controller
{
    /**
     * Método para anexar uma disciplina a uma turma. Gerando a matriz
     * @param Request $request
     * @return JsonResponse
     */
    public function postAttach(Request $request)
    {
        try{
            /** @var Validator $validator */
            $validator = \Validator::make($request->all(), [
                'atribuicoes'               => 'required|array',
                'atribuicoes.*.id_matriz_curricular'    => 'required|numeric',
                'atribuicoes.*.professores'             => 'required|array',

            ]);
            if ($validator->fails()) {
                throw new WsException('1501','attach');
            }

            $atribuicoes = $request->get('atribuicoes',[]);

            \DB::beginTransaction();
            foreach($atribuicoes as $atribuicao)
            {
                $idMatrizCurricular = array_get($atribuicao,'id_matriz_curricular', null);
                MatrizCurricularModel::checkExistence( $idMatrizCurricular ,new WsException('1502','attach'));

                $professoresTurmas = new ProfessoresDasTurmas();
                $professoresTurmas->id_matriz_curricular = $idMatrizCurricular;

                $professoresTurmas->clearDuplicated();

                try{
                    $arrayProfessores = array_get($atribuicao, 'professores',[]);
                    $professoresTurmas->updateDuplas($arrayProfessores);
                }
                catch(\Exception $error)
                {
                    \DB::rollback();
                    throw new WsException('1504','attached');
                }
            }
            \DB::commit();

            return new JsonResponse(['status' => 1, 'attached' => true, 'code' => '1500']);

        }catch (WsException $error){

            \DB::rollback();
            throw $error;

        }

    }

    /**
     * Método para retornar a lista de turmas disponível
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request)
    {
        $professoresFiltros = $request->get('filtro_professores',[]);
        $duplasfiltros = $request->get('filtros_duplas',[]);

        $professores = ProfessoresModel::search($professoresFiltros);
        $duplas = ProfessoresDasTurmas::search($duplasfiltros);

        return new JsonResponse([
            'duplas' => $duplas,
            'professores' => $professores
        ]);

    }
}