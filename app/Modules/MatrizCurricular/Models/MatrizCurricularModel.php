<?php 

namespace App\Modules\MatrizCurricular\Models;

use App\Models\WsClientModel;
use App\Modules\Atribuicoes\Models\ProfessoresDasTurmas;


/**
 * Esta classe representa o relacionamento de Disciplinas com Turmas
 * Class MatrizCurricularModel
 * @package App\Modules\Disciplinas\Models
 */
class MatrizCurricularModel extends WsClientModel
{
	// Table in database
	public $table = 'Matriz_Curricular';

    /**
     * Relacionamento com Turmas
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function turma(){
        return $this->belongsTo(
            '\App\Modules\Turmas\Models\TurmasModel',
            'id_turma');
    }

    /**
     * Relacionamento com as Disciplinas
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function disciplina(){
	    return $this->belongsTo(
	        '\App\Modules\Disciplinas\Models\DisciplinasModel',
            'id_disciplina');
    }

    /**
     * Relacionametno com as Matrizes Curriculares
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function atribuicoes()
    {
        return $this->hasMany(
            '\App\Modules\Atribuicoes\Models\ProfessoresDasTurmas',
            'id_matriz_curricular'
        );
    }

    /**
     * Escopo de query para aplicar o filtro de Grupos de Turmas
     * @param $query
     * @param $idGrupo
     * @return mixed
     */
    public function scopeFiltroGrupoTurma($query, $idGrupo)
    {
        //Filtro do ID da disciplina
        if(!empty($idGrupo))
        {
            return $query->whereHas('turma.grupos', function($subQuery) use ($idGrupo){
                $subQuery->where('id_turma', $idGrupo);
            });
        }

        return $query;
    }

    /**
     * Escopo de query para aplicar o filtro de grupo de disciplina
     * @param $query
     * @param $idGrupo
     * @return mixed
     */
    public function scopeFiltroGrupoDisciplina($query, $idGrupo)
    {
        //Filtro do ID da disciplina
        if(!empty($idGrupo))
        {
            return $query->whereHas('disciplina.grupos', function($subQuery) use ($idGrupo){
                $subQuery->where('id_disciplina', $idGrupo);
            });
        }

        return $query;
    }

    /**
     * Escopo para aplicar filtro de palavra chave
     * @param $query
     * @param null $texto
     * @return mixed
     */
    public function scopeFiltroPalavraChave($query, $texto = null)
    {
        if(!empty($texto)){

            return $query->where(function($subQuery) use ($texto){

                $wordsArray = explode(' ',$texto);

                $subQuery->whereHas('turma', function($subQuery) use ($wordsArray){
                    foreach($wordsArray as $word){
                        $subQuery->orWhere('nome','like','%'.$word.'%');
                        $subQuery->orWhere('nome_completo','like','%'.$word.'%');
                    }
                });

                $subQuery->orWhereHas('disciplina', function($subQuery) use ($wordsArray){
                    foreach($wordsArray as $word){
                        $subQuery->orWhere('nome','like','%'.$word.'%');
                        $subQuery->orWhere('nome_completo','like','%'.$word.'%');
                    }
                });
            });

        }
    }

    /**
     * @param $query
     * @param null $idProfessor
     */
    public function scopeFiltroPorProfessor($query, $idProfessor = null )
    {
        if(!empty($idProfessor)){

            $duplas = ProfessoresDasTurmas::findPorProfessor($idProfessor);

            $idsMatriz = $duplas->lists('id_matriz_curricular');
            //$query->where( '', function($query) use ( $arrayIdTurma,$arrayIdDisciplina ){
            return $query->whereIn('id', $idsMatriz);
            //});
        }
        return $query;
    }

    public function search($qtde_aulas)
    {
        $matrizQueryBuilder = MatrizCurricularModel::select('*');

//        \DB::enableQueryLog();

        $matriz = $matrizQueryBuilder
            ->where('qtde_aulas', $qtde_aulas )
            ->get();
        return $matriz;
    }

    public function searchId($id=array())
    {
        $duplas = MatrizCurricularModel::find($id);
        return $duplas;
    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }
}