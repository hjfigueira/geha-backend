<?php namespace App\Modules\MatrizCurricular\Controllers;

use App\Exceptions\WsException;
use App\Http\Controllers\Controller;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use App\Modules\Turmas\Models\TurmasGrupoModel;
use App\Modules\Turmas\Models\TurmasModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ApiControllerMatrizCurricular
 * @package App\Modules\MatrizCurricular\Controllers
 */
class ApiControllerMatrizCurricular extends Controller
{
    /**
     * Método para anexar uma disciplina a uma turma. Gerando a matriz
     * @param Request $request
     * @return JsonResponse
     */
    public function postAttach(Request $request)
    {
        try{
            $dataRequest = $request->all();
            $validator = \Validator::make($dataRequest, ['id_turma' => 'required|numeric' ]);
            if ($validator->fails()) {
                throw new WsException('1401','attach');
            }

            $validator = \Validator::make($dataRequest, [
                'disciplinas'               => 'array',
                'disciplinas.*.qtde_aulas'  => 'numeric|min:0',
                'disciplinas.*.id'          => 'numeric'
            ]);
            if ($validator->fails()) {
                throw new WsException('1402','attach');
            }

            $idTurma = $request->get('id_turma');
            $disciplinas = $request->get('disciplinas', []);
            foreach($disciplinas as $disciplina)
            {

                if($disciplina['qtde_aulas']==0)
                {
                    MatrizCurricularModel::
                    where('id_turma', '=', $idTurma)
                        ->where('id_disciplina', '=', $disciplina['id'])
                        ->delete();
                    return new JsonResponse(['status' => 1, 'deleted' => true, 'code' => '1400']);
                }
            }


            /** @var TurmasModel $turma */
            $turma = TurmasModel::checkExistence($idTurma,new WsException('1403','attach'));

            $turma->updateDisciplinas($disciplinas);

            return new JsonResponse(['status' => 1, 'attached' => true, 'code' => '1400']);

        }catch (ModelNotFoundException $error)
        {
            throw new WsException('1404','attach');
        }

    }

    /**
     * Método para retornar a lista de turmas disponível
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request)
    {
        $filtros = $request->get('filtro_turma',[]);
        $turmas = TurmasModel::search($filtros);

        return new JsonResponse($turmas);
    }

    /**
     * Método para consultar um item e disciplinas disponíveis
     * @param $id
     * @param Request $request
     * @return JsonResponse
     * @throws WsException
     */
    public function getItem($id, Request $request)
    {
        $filtros = $request->get('filtro_disciplina',[]);
        $turma = TurmasModel::checkExistence($id, new WsException('1406','getted'));

        $dataset = $turma->toArray();
        $dataset['grupos']      = $turma->grupos()->get()->lists('nome','id');
        $dataset['disciplinas'] = DisciplinasModel::search($filtros);
        $dataset['limite_minimo_aulas'] = $turma->getLimiteMinimo();
        $dataset['limite_maximo_aulas'] = $turma->getLimiteMaximo();
        $selecionadas = $turma->disciplinas()->withPivot('qtde_aulas')->get()->lists('pivot.qtde_aulas','id');

        foreach($dataset['disciplinas'] as &$disciplina)
        {
            $disciplina['pivot']['qtde_aulas']   = 0;
            if(isset($selecionadas[$disciplina['id']]))
            {
                $disciplina['pivot']['qtde_aulas']   = $selecionadas[$disciplina['id']];
            }
        }

        return new JsonResponse($dataset);
    }

    /**
     * Método para executar uma cópia de uma matriz curricular
     * @return JsonResponse
     */
    public function postCopy(Request $request)
    {
        try {
            //Validações
            $validator = \Validator::make($request->all(), ['id_turma' => 'required|numeric']);
            if ($validator->fails()) {
                throw new WsException('1408', 'attach');
            }

            $validator = \Validator::make($request->all(), [
                'id_turma_alvo' => 'required_without:id_grupo_alvo|numeric',
                'id_grupo_alvo' => 'required_without:id_turma_alvo|numeric'
            ]);
            if ($validator->fails()) {
                throw new WsException('1409', 'attach');
            }

            //Captura das variáveis
            $idTurmaFonte = $request->get('id_turma', null);
            $idTurmaAlvo  = $request->get('id_turma_alvo', null);
            $idGrupoAlvo  = $request->get('id_grupo_alvo', null);
            $mode = (bool)$request->get('mode', false);

            $turmaFonte = TurmasModel::checkExistence($idTurmaFonte, new WsException('1407', 'copied'));
            $disciplinas = $turmaFonte->disciplinas()->withPivot('qtde_aulas')->get();

            \DB::beginTransaction();

            if (!empty($idTurmaAlvo)) {

                $turmaAlvo = TurmasModel::findOrFail($idTurmaAlvo);
                $complete = $turmaAlvo->copiarDisciplinas($disciplinas, $mode);
                if (!$complete) {
                    throw new WsException('1410', 'copied');
                }

            } else if (!empty($idGrupoAlvo)) {

                $turmaGrupo = TurmasGrupoModel::findOrFail($idGrupoAlvo);
                foreach ($turmaGrupo->turmas()->get() as $turma) {
                    $complete = $turma->copiarDisciplinas($disciplinas, $mode);
                    if (!$complete) {
                        throw new WsException('1410', 'copied');
                    }
                }
            }

            \DB::commit();
            return new JsonResponse(['status' => 1, 'copied' => true, 'code' => '1400']);

        } catch (\Exception $error) {
            \DB::rollback();
            throw $error;
        }
    }
}