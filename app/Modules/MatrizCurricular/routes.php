<?php
/*
|--------------------------------------------------------------------------
| User ModuleOperation Routes
|--------------------------------------------------------------------------
|
| All the routes related to the User module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group([
    'middleware' => ['web','auth'],
    'prefix' => 'api/matriz',
    'namespace' => 'App\Modules\MatrizCurricular\Controllers'],
    function () {

	    Route::post('attach', 	  ['as' => 'api/matriz.attach',  'uses' => 'ApiControllerMatrizCurricular@postAttach']);
        Route::post('copy',       ['as' => 'api/matriz.copy', 	'uses' => 'ApiControllerMatrizCurricular@postCopy']);
        Route::get('all',         ['as' => 'api/matriz.all', 	'uses' => 'ApiControllerMatrizCurricular@getAll']);
        Route::get('get/{id}',    ['as' => 'api/matriz.get', 	'uses' => 'ApiControllerMatrizCurricular@getItem'])->where('id', '[0-9]+');

});