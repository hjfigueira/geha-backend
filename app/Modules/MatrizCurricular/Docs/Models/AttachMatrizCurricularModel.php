<?php
/**
 * @SWG\Definition(
 *     required={"id_turma", "id_disciplina", "qtde_aulas"},
 *     type="object", 
 *     @SWG\Xml(name="AttachMatrizCurricularModel")
 * )
 */
class AttachMatrizCurricularModel
{

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_turma;

    /**
     * @SWG\Property(format="array")
     * @SWG\Property(@SWG\Xml(name="disciplinas",wrapped=true))
     */
    public $disciplinas;

}