<?php
/**
 * @SWG\Definition(
 *     required={"id_turma", "id_disciplina", "qtde_aulas"},
 *     type="object", 
 *     @SWG\Xml(name="CopyMatrizCurricularModel")
 * )
 */
class CopyMatrizCurricularModel
{

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_turma;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_turma_alvo;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_grupo_alvo;
}