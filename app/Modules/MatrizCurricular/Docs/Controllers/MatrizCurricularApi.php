<?php

/**
 * @SWG\Get(
 *     path="/matriz/all",
 *     summary="Retorna a lista de turmas e matrizes anexadas a elas.",
 *     description="Retorna a lista de turmas e matrizes anexadas a elas.",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     tags={"matriz.basico"},
 *     @SWG\Parameter(
 *         description="Filtro, ID do Grupo",
 *         in="query",
 *         name="filtro_turma[grupo_id]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, ID da Disciplina",
 *         in="query",
 *         name="filtro_turma[disciplina_id]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Apenas Incompletas",
 *         in="query",
 *         name="filtro_turma[only_empty]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, palavras chave",
 *         in="query",
 *         name="filtro_turma[keyword]",
 *         required=false,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Get(
 *     path="/matriz/get/{turmaID}",
 *     summary="Retorna a lista de turmas e matrizes anexadas a elas.",
 *     description="Retorna a lista de turmas e matrizes anexadas a elas.
 *      [code=1406] = Turma não encontrada.
 *     ",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     tags={"matriz.basico"},
 *     @SWG\Parameter(
 *         description="Id da Turma para recuperar a Matriz Curricular",
 *         in="path",
 *         name="turmaID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, ID do Grupo",
 *         in="query",
 *         name="filtro_disciplina[grupo_id]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, ID da Disciplina",
 *         in="query",
 *         name="filtro_disciplina[color]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Filtro, Apenas Incompletas",
 *         in="query",
 *         name="filtro_disciplina[keyword]",
 *         required=false,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/matriz/attach",
 *     operationId="attach",
 *     summary="Anexa disciplinas em uma turma através da Matriz Curricular",
 *     description=" Remove os anexos de uma turma, e anexa as novas disciplinas. Status abaixo :
 *     [code=1400] = Efetuado com sucesso.
 *     [code=1401] = Campo 'id_turma' é obrigatório, e deve ser numérico.
 *     [code=1402] = Campo 'disciplinas' está com formato inválido.
 *     [code=1403] = A Turma com id especificado, não foi encontrada.
 *     [code=1404] = A alguma Disciplina com id especificado, não foi encontrada.
 *     [code=1405] = Ocorreu um erro ao remover as disciplinas antigas e anexar as novas.
 *     ",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"matriz.basico"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/AttachMatrizCurricularModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/matriz/copy",
 *     operationId="copy",
 *     summary="Copia a matriz curricular de uma matéria para a outra.",
 *     description="
 *     [code=1400] = Efetuado com sucesso.
 *     [code=1407] = A turma fonte não pode ser encontrada.
 *     [code=1408] = Campo 'id_turma' é inválido, não existe ou não é numérico.
 *     [code=1409] = O Campo 'id_turma_alvo' ou 'id_grupo_alvo', são obrigatórios, um ou o outro.
 *     [code=1410] = A cópia não foi feita porque o alvo ja possui itens, utilize 'mode' para sobreescrever ou mesclar.
 *     ",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"matriz.basico"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/CopyMatrizCurricularModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */