<?php

namespace App\Modules\Turnos\Controllers;

use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use App\Modules\Turnos\Models\TurnosModel;
use Illuminate\Database\QueryException as QueryException;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 *
 * @SWG\Swagger(
 *     basePath="",
 *     host="localhost:8000",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Sample API",
 *         @SWG\Contact(name="Giuliano Sampaio"),
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */
class ApiController extends Controller
{
    protected $perguntas;

    public function __construct()
    {
        $this->model = new TurnosModel();
    }

    /**
     * Insert a new Api
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/turnos/save",
     *     description="Create a new Api.",
     *     produces={"application/json"},
     *     tags={"api"},
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */
    public function save(Request $request)
    {
        $turnos = $request->get('params');

        if ( empty($turnos))
        {
            $error['code']     = '0301';
            $error['status']   = '00';
            $error['message']  = 'Passagem de paramêtros insuficientes. ';
            return json_encode($error);
        }

        foreach($turnos as $turno) {

            if(!isset($turno['id']) or (!is_numeric($turno['id'])))
            {
                $return['code']    = '0302';
                $return['status']  = '00';
                $return['message'] = 'ID é Obrigatório e deve ser numérico';

                return json_encode($return);
            }

//            if ($this->delete($turno) == false) {
//                $response['code']    = '0304';
//                $response['status']  = '00';
//                $response['message'] = 'Não foi possível remover turno.';
//
//                return $response;
//            }

            if(!$this->valid($turno))
            {
                $return['code']    = '0302';
                $return['status']  = '01';
                $return['message'] = 'Horário fora do intervalo';

                return json_encode($return);
            }
            else
            {
                $this->inject($turno);
            }

        }

        $return['code']    = '0300';
        $return['status']  = '01';
        $return['message'] = 'OK';
        
        return json_encode($return);
    }


    /**
     * Get all 
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/turnos/all",
     *     description="Get a new Api.",
     *     produces={"application/json"},
     *     tags={"api"},
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */
    public function all(Request $request)
    {
        $turnos = $this->model->all();    
        $all    = [];

        foreach ($turnos as $turno) {

            $hrIni = explode(':', $turno->hr_inicio);
            $hrFim = explode(':', $turno->hr_termino);

            $all[] = ['id'         => $turno->id,
                      'nome'       => $turno->nome,
                      'hr_inicio'  => $hrIni[0] .':'.$hrIni[1],
                      'hr_termino' => $hrFim[0] .':'.$hrFim[1]];
        }

        return $all;
    }


    /* --{Private Functions}-- */
    

    /**
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    protected function inject($turno)
    {
        $model = $this->model->find(@$turno['id']);

        if ($model == null) {
            $model     = new TurnosModel();
            $model->id = @$turno['id'];
        }

        $model->nome       = $turno['nome'];
        $model->hr_inicio  = $turno['horaInicio'];
        $model->hr_termino = $turno['horaTermino'];

        $model->save();
    }

    protected function valid($turno)
    {
        $limiteInferior = 0;
        $limiteSuperior = 0;
        $turnoCorrespondente = @$turno['id'];

        switch ($turnoCorrespondente)
        {
            case 0:
                $limiteInferior = '00:00';
                $limiteSuperior = '13:00';
                break;

            case 1:
                $limiteInferior = '12:00';
                $limiteSuperior = '19:00';
                break;

            case 2:
                $limiteInferior = '18:00';
                $limiteSuperior = '24:00';
                break;
        }

        $limiteInferior = new \DateTime($limiteInferior);
        $limiteSuperior = new \DateTime($limiteSuperior);
        $horaInicio     = new \DateTime($turno['horaInicio']);
        $horaTermino    = new \DateTime($turno['horaTermino']);

        return ( $horaInicio >= $limiteInferior and $horaTermino <= $limiteSuperior );
    }


    /**
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    protected function delete($turno)
    {
        if (! isset($turno['id']) ) return true; 

        try {

            $turno = $this->model->find($turno['id']);

            if ($turno == null) return true;

            $turno->delete();  
            
            return true;

        } catch (QueryException $e) {
            return false;
        }
    }
}
