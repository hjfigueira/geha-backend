<?php 

namespace App\Modules\Turnos\Models;

use App\Models\WsClientModel;
use DB;
use Illuminate\Database\Eloquent\Model;

class TurnosModel extends WsClientModel
{
	// Table in database
	protected $table = 'Turnos';

	public function esquemas()
	{
		$namespace = 'App\Modules\Esquemas\Models\EsquemasModel';

		return $this->hasMany($namespace, 'id_turno', 'id');
	}

	public function getEsquemaMaisAtividades()
    {
        $maxAtividade = 0;

        foreach($this->esquemas()->get() as $esquema)
        {
            $atividadesDiarias = $esquema->atividades()
                ->whereIn('tipo_atividade', [1, 3])
                ->groupBy('id_dia')
                ->select(\DB::raw( 'COUNT(id_dia) as quantidade' ))
                ->get();

            foreach($atividadesDiarias as $dia)
            {
                $currentValue = $dia->quantidade;

                if($maxAtividade < $currentValue)
                {
                    $maxAtividade = $currentValue;
                }
            }

        }

        return $maxAtividade;
    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }
}