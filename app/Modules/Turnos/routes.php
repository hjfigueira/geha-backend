<?php
/*
|--------------------------------------------------------------------------
| Turnos Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Turnos module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group([
    'prefix' => 'api/turnos',
    'namespace' => 'App\Modules\Turnos\Controllers',
    'middleware' => 'auth'
], function () {
    Route::get('all',   ['as' => 'api/turnos.all',  'uses' => 'ApiController@all']);
    Route::post('save', ['as' => 'api/turnos.save', 'uses' => 'ApiController@save']);
});

