<?php
/*
|--------------------------------------------------------------------------
| Turmas ModuleOperation Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Turmas module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group([
    'prefix' => 'api/turmas',
    'namespace' => 'App\Modules\Turmas\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',     	   ['as' => 'api/turmas.all',     'uses' => 'ApiControllerTurma@all']);	
	Route::get('get/{id}',     ['as' => 'api/turmas.get',     'uses' => 'ApiControllerTurma@get'])->where('id', '[0-9]+');	
	Route::get('get/{name}',   ['as' => 'api/turmas.search',  'uses' => 'ApiControllerTurma@search'])->where('name', '[A-Za-z ]+');	
	Route::post('save',        ['as' => 'api/turmas.save',    'uses' => 'ApiControllerTurma@save']);	
	Route::post('delete', ['as' => 'api/turmas.destroy', 'uses' => 'ApiControllerTurma@destroy']);
});


Route::group([
    'prefix' => 'api/turmas-grupos',
    'namespace' => 'App\Modules\Turmas\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',     	   ['as' => 'api/turmas-grupos.all',       'uses' => 'ApiControllerGrupo@all']);	
	Route::get('get/{id}',     ['as' => 'api/turmas-grupos.get',       'uses' => 'ApiControllerGrupo@get'])->where('id', '[0-9]+');	
	Route::get('get/{name}',   ['as' => 'api/turmas-grupos.search',    'uses' => 'ApiControllerGrupo@search'])->where('name', '[A-Za-z ]+');	
	Route::post('save',        ['as' => 'api/turmas-grupos.save',      'uses' => 'ApiControllerGrupo@save']);	
	Route::post('attach', 	   ['as' => 'api/turmas-grupos.attach',    'uses' => 'ApiControllerGrupo@attach']);	
	Route::post('delete', ['as' => 'api/turmas-grupos.destroy',   'uses' => 'ApiControllerGrupo@destroy']);
});

	
Route::group([
    'prefix' => 'api/turmas-esquemas',
    'namespace' => 'App\Modules\Turmas\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',     ['as' => 'api/turmas-esquemas.all',     'uses' => 'ApiControllerEsquemas@all']);	
	Route::post('save',   ['as' => 'api/turmas-esquemas.save',    'uses' => 'ApiControllerEsquemas@save']);	
	Route::post('delete', ['as' => 'api/turmas-esquemas.destroy', 'uses' => 'ApiControllerEsquemas@destroy']);	
});