<?php 

namespace App\Modules\Turmas\Models;

use Illuminate\Database\Eloquent\Model;

class TurmasEsquemaModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	protected $table = 'Turmas_Esquemas';

	// Connection Database
	protected $connection = 'urania-cliente';


	public function esquema()
    {
        return $this->hasOne(
            'App\Modules\Esquemas\Models\Esquemas',
            'id'
        );
    }

	/**
	 * Get the next Id
	 * @return Integer
	 */
	public function nextId()
	{
		$query = "SELECT max(id) AS id FROM ".$this->table;

		$ret = DB::connection($this->connection)->select(DB::raw($query));

		$item = $ret[0];

		return ($item->id == null) ? 1 : intval($item->id) + 1; 
	}
}