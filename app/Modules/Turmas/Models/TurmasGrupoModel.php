<?php 

namespace App\Modules\Turmas\Models;

use App\Models\WsClientModel;
use Illuminate\Database\Eloquent\Model;

class TurmasGrupoModel extends WsClientModel
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	protected $table = 'Turmas_Grupos';

	// Connection Database
	protected $connection = 'urania-cliente';

	public function turmas(){

        return $this->belongsToMany(
            'App\Modules\Turmas\Models\TurmasModel',
            'Rel_Turmas_Grupos',
            'id_grupo',
            'id_turma'
        );

    }

    public function scopeFiltrarTurma($query, $idTurma)
    {
        if(!empty($idTurma))
        {
            return $query->whereHas('turmas', function($subquery) use ($idTurma) {
                $subquery->where('id', $idTurma);
            });
        }

        return $query;
    }

    public function scopeFiltrarTexto($query, $texto)
    {
        if(!empty($texto))
        {
            return $query->where(function($subQuery) use ($texto){

                foreach(explode(' ',$texto) as $word)
                {
                    $subQuery->orWhere('nome','like','%'.$word.'%');
                }
            });
        }

        return $query;
    }

    public function search(array $params = [])
    {
        $turmas = TurmasGrupoModel::select('*');

        $turmas->filtrarTurma(array_get($params, 'id_turma'))
            ->filtrarTexto(array_get($params, 'keyword'));

        return $turmas->get();
    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }
}