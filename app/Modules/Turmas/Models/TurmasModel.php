<?php 

namespace App\Modules\Turmas\Models;

use App\Models\WsClientModel;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class TurmasModel extends WsClientModel
{
	/**
	 * Table in database
	 * @var string
	 */
	public $table = 'Turmas';

	public $fillable = ['total_aulas'];


    /**
     * Configuração do relacionamento com disiciplinas
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
	public function disciplinas()
    {
        return $this->belongsToMany(
            'App\Modules\Disciplinas\Models\DisciplinasModel',
            'Matriz_Curricular',
            'id_turma',
            'id_disciplina');
    }

    /**
     * Configuração do relacionamento com grupos
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function grupos(){

        return $this->belongsToMany(
            'App\Modules\Turmas\Models\TurmasGrupoModel',
            'Rel_Turmas_Grupos',
            'id_turma',
            'id_grupo'
        );

    }

    /**
     * Relacionamento com Esquemas
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function esquemas(){

        return $this->belongsToMany(
            'App\Modules\Esquemas\Models\EsquemasModel',
            'Turmas_Esquemas',
            'id_turma',
            'id_esquema');

    }

    public function getLimiteMinimo(){

        $totalLimite = 0;

        foreach($this->esquemas()->get() as $esquema)
        {
            $atividades = $esquema->atividades();
            $totalLimite += $atividades ->where('tipo_atividade',1)->count();
        }

        return $totalLimite;

    }

    public function getLimiteMaximo(){

        $totalLimite = 0;

        foreach($this->esquemas()->get() as $esquema)
        {
            $atividades = $esquema->atividades();
            $totalLimite += $atividades->where('tipo_atividade',3)->orWhere('tipo_atividade',1)->count();
        }

        return $totalLimite;
    }

    /**
     * Filtro de Grupos para as Turmas
     * @param $query Builder
     * @param int $grupo_id
     */
	public function scopeFiltrarGrupo($query, $grupo_id = null)
    {
        //Filtro do ID do grupo
        if(!empty($grupo_id)){
            return $query->whereHas('grupos', function($query) use ($grupo_id){
                $query->where('id', $grupo_id);
            });
        }

        return $query;
    }

    public function scopeFiltrarSemEsquema($query, $enabled = false)
    {
        if(boolval($enabled))
        {
            return $query->doesntHave('esquemas');
        }

        return $query;
    }

    /**
     * Filtro para selecionar turmas por disciplinas
     * @param $query
     * @param null $disciplina_id
     * @return mixed
     */
    public function scopeFiltrarDisciplina($query, $disciplina_id = null)
    {
        //Filtro do ID da disciplina
        if(!empty($disciplina_id))
        {
            return $query->whereHas('disciplinas', function($subQuery) use ($disciplina_id){
                $subQuery->where('id_disciplina', $disciplina_id);
            });
        }

        return $query;
    }

    /**
     * Filtro para selecionar turmas por busca textual
     * @param $query
     * @param null $texto
     * @return mixed
     */
    public function scopeFiltrarPalavraChave($query, $texto = null)
    {
        if(!empty($texto)){

            return $query->where(function($subQuery) use ($texto){

                foreach(explode(' ',$texto) as $word)
                {
                    $subQuery->orWhere('nome','like','%'.$word.'%');
                    $subQuery->orWhere('nome_completo','like','%'.$word.'%');
                }
            });

        }

        return $query;
    }

    /**
     * Filtro para apenas turmas com cadastros incompletos
     * @param $query
     * @param bool $habilitado
     * @return mixed
     */
    public function scopeFiltrarApenasIncompletos($query, $habilitado = false)
    {
        if(boolval($habilitado)){

            $limiteSuperior = $this->getLimiteMaximo();
            $limiteInferior = $this->getLimiteMinimo();

            return $query->where('total_aulas','>',$limiteInferior)
                ->where('total_aulas','<', $limiteSuperior);
        }
    }

    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
	public static function search(array $filtro = [])
    {
        $searchReturn = [];
        $turmasQueryBuilder = TurmasModel::select('*');

        $grupo_id       = array_get($filtro,'grupo_id',null);
        $disciplina_id  = array_get($filtro,'disciplina_id',null);
        $only_empty     = array_get($filtro,'only_empty',null);
        $keyword        = array_get($filtro,'keyword',null);
        $semEsquema     = array_get($filtro,'sem_esquema',false);

//        \DB::enableQueryLog();

        $turmas = $turmasQueryBuilder
            ->filtrarGrupo($grupo_id)
            ->filtrarDisciplina($disciplina_id)
            ->filtrarPalavraChave($keyword)
            ->filtrarApenasIncompletos($only_empty)
            ->filtrarSemEsquema($semEsquema)
            ->get();

//        dd(\DB::getQueryLog());

        foreach($turmas as $turma)
        {
            $searchReturn[] = array_merge($turma->toArray(), [
                'disciplinas' => $turma->disciplinas()->withPivot('qtde_aulas')->where('qtde_aulas','>','0')->get(),
                'grupos' => $turma->grupos()->get()->toArray(),
                'limite_minimo_aulas' => $turma->getLimiteMinimo(),
                'limite_maximo_aulas' => $turma->getLimiteMaximo(),
                'esquemas' => $turma->esquemas()->count(),
            ]);
        }

        return $searchReturn;
    }

    /**
     * Método para atualizar as disciplinas de uma turma. (Matriz Curricular)
     * @param array $disciplinas
     * @throws \Exception
     */
    public function updateDisciplinas(array $disciplinas = [])
    {
        try{
            \DB::beginTransaction();

            $this->disciplinas()->detach($this->disciplinas()->get());
            $totalAulas = 0;

            foreach($disciplinas as $disciplina)
            {
                $modelDisciplina = DisciplinasModel::findOrFail($disciplina['id']);

                $this->disciplinas()->attach($modelDisciplina, [
                    'qtde_aulas' => $disciplina['qtde_aulas']
                ]);
                $totalAulas += $disciplina['qtde_aulas'];
            }

            $this->total_aulas = $totalAulas;
            $this->save();

            \DB::commit();

        }catch(\Exception $error)
        {
           \DB::rollback();
           throw $error;
        }
    }

    /**
     * Copia as disciplinas de uma turma alvo
     * @param $disciplinas
     * @param bool $saveMode
     * @return bool
     * @throws \Exception
     */
    public function copiarDisciplinas( $disciplinas, $saveMode = false)
    {
        try{

            if(!$this->haveAnyDisciplinas() || $saveMode != false){

                if($saveMode == 'overwrite')
                {
                    $this->clearDisciplinas();
                }

                $totalAulas = 0;

                foreach($disciplinas as $disciplina)
                {
                    $totalAulas += $disciplina->pivot->qtde_aulas;

                    $this->disciplinas()->attach($disciplina, [
                        'qtde_aulas' => $disciplina->pivot->qtde_aulas
                    ]);
                }

                $this->total_aulas = $totalAulas;
                $this->save();

                return true;
            }

            return false;

        }catch(\Exception $error)
        {
            \DB::rollback();
            throw $error;
        }
    }

    /**
     * Remove todas as disciplinas anexas a essa turma
     */
    public function clearDisciplinas()
    {
        $this->disciplinas()->detach($this->disciplinas()->get());
    }

    /*
     * Confere se essa turma tem alguma disciplina
     */
    public function haveAnyDisciplinas()
    {
        return ($this->disciplinas()->count()) > 0;
    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }

}