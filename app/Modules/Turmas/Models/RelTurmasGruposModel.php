<?php 

namespace App\Modules\Turmas\Models;

use Illuminate\Database\Eloquent\Model;

class RelTurmasGruposModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Rel_Turmas_Grupos';

	// Connection Database
	protected $connection = 'urania-cliente';

}