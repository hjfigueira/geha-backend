<?php
/**
 * Retorna todos os cadastrados de grupos de turma.
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Get(
 *     path="/turmas-grupos/all",
 *     description="Retorna todos os cadastrados de grupos de turma.",
 *     produces={"application/json"},
 *     tags={"turmas.grupos"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * Retorna um grupo específico por ID
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Get(
 *     path="/turmas-grupos/get/{turmaID}",
 *     description="Retorna um grupo específico, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"turmas.grupos"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID da turma",
 *         in="path",
 *         name="turmaID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 * Retorna um grupo específico por nome
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Get(
 *     path="/turmas-grupos/get/{nome}",
 *     description="Retorna um grupo específico, passando texto como parâmetro.",
 *     produces={"application/json"},
 *     tags={"turmas.grupos"},
 *     @SWG\Parameter(
 *         description="Nome da turma",
 *         in="path",
 *         name="nome",
 *         required=true,
 *         type="string",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * Remove um grupo do banco de dados
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/turmas-grupos/delete",
 *     description="Remove um grupo do banco de dados. Possíveis status:
 *     [code=0700] = Efetuado com sucesso.
 *     [code=0703] = Campo 'id' é obrigatório.
 *     [code=0704] = Item não foi encontrado ou já foi removido.
 *     [code=0713] = Grupo não pode ser excluido pois possui turmas relacionadas",
 *     produces={"application/json"},
 *     tags={"turmas.grupos"},
 *     @SWG\Parameter(
 *         name="turma",
 *         in="body",
 *         description="Objeto JSON com 'ids'",
 *         @SWG\Schema(ref="#/definitions/GrupoModel"),
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * Salva um grupo de turma
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/turmas-grupos/save",
 *     operationId="save",
 *     consumes={"application/json", "application/xml"},
 *     summary="Salva um grupo de turma",
 *     description="Salva o grupo de uma turma no sistema. <br/>
       Esta função pode retornar um campo extra chamado ID, com o ID do objeto recém-criado. <br/>
	   Para atualizar um determinado objeto, basta passar seu ID como parâmetro.	<br/> Possíveis status:
 *     [code=0700] = Efetuado com sucesso.
 *     [code=0701] = Campo 'nome' é obrigatório.
 *     [code=0702] = Campo 'nome' é um valor inválido.
 *     [code=0705] = Item com o ID que foi informado não foi encontrado.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"turmas.grupos"},
 *     @SWG\Parameter(
 *         name="turma",
 *         in="body",
 *         description="Objeto JSON com as propriedades da turma",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/GrupoModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * Anexa uma turma à um grupo
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/turmas-grupos/attach",
 *     operationId="save",
 *     consumes={"application/json", "application/xml"},
 *     summary="Anexa uma turma à um grupo",
 *     description="Anexa turmas à um grupo no sistema. Para limpar todas as turmas basta passar turmas como []. Possíveis status:
 *     [code=0700] = Efetuado com sucesso.
 *     [code=0706] = Campo 'id_grupo' é obrigatório.
 *     [code=0707] = Campo 'turmas' é obrigatório.
 *     [code=0708] = Erro ao desvincular turma. Verifique se ela não está sendo usada.
 *     [code=0709] = Turma não encontrada.
 *     [code=0710] = Grupo não encontrado.
 *     [code=0711] = Erro ao salvar dados no banco de dados.
 *     [code=0712] = Parâmetro nulo ou objeto JSON mal formatado.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"turmas.grupos"},
 *     @SWG\Parameter(
 *         name="turma",
 *         in="body",
 *         description="Objeto JSON com as propriedades da turma",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/RelTurmasGrupoModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */