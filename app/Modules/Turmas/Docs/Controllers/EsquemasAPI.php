<?php
/**
 * Retorna todos os grupos de turma cadastradas
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Get(
 *     path="/turmas-esquemas/all",
 *     description="Retorna todos os esquemas de turmas cadastrados",
 *     produces={"application/json"},
 *     tags={"turmas.esquemas"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */



/**
 * Remove um esquema de turma do banco de dados
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/turmas-esquemas/delete/",
 *     description="Remove um esquema de turma do banco de dados. Possíveis status:
 *     [code=0800] = Efetuado com sucesso.
 *     [code=0801] = Campo 'id_turma' é obrigatório.
 *     [code=0802] = Campo 'id_turno' é obrigatório.
 *     [code=0803] = Campo 'id_esquema' é obrigatório.
 *     [code=0808] = Item não foi encontrado ou já foi removido.
 *     [code=0809] = Não foi possível remover item do banco de dados.",
 *     produces={"application/json"},
 *     tags={"turmas.esquemas"},
 *     @SWG\Parameter(
 *         name="esquma",
 *         in="body",
 *         description="Objeto JSON com as propriedades do esquema",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/EsquemaModel"),
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * Salva um esquema de turma do banco de dados
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/turmas-esquemas/save",
 *     operationId="save",
 *     consumes={"application/json", "application/xml"},
 *     summary="Salva um esquema de turma do banco de dados.",
 *     description="Salva o grupo de uma turma no sistema. Possíveis status:
 *     [code=0800] = Efetuado com sucesso.
 *     [code=0801] = Campo 'id_turma' é obrigatório.
 *     [code=0802] = Campo 'id_turno' é obrigatório.
 *     [code=0803] = Campo 'id_esquema' é obrigatório.
 *     [code=0805] = Nenhuma turma encontrado com este id_turma.
 *     [code=0806] = Nenhum turno encontrado com este id_turno.
 *     [code=0806] = Nenhum esquema encontrado com este id_esquema.
 *     [code=0804] = Falha ao inserir no banco de dados. Verifique os dados enviados e tente novamente.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"turmas.esquemas"},
 *     @SWG\Parameter(
 *         name="turma",
 *         in="body",
 *         description="Objeto JSON com as propriedades da turma",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/EsquemaModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */