<?php
// *     schemes={"https"},    
// *     host="agencia110.com.br/projeto/geha/api/public",

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     host="localhost:8000",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Funções para integração com outros sistemas",
 *         @SWG\Contact(name="Giuliano Sampaio e Hélio Figueira Júnior"),
 *     )
 * )
 */

/**
 * Retorna todas os cadastrados de turma 
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Get(
 *     path="/turmas/all",
 *     description="Retorna todas as turmas cadastradas.",
 *     produces={"application/json"},
 *     tags={"turmas.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * Retorna uma turma específico por ID
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Get(
 *     path="/turmas/get/{turmaID}",
 *     description="Retorna uma turma específica, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"turmas.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID da turma",
 *         in="path",
 *         name="turmaID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 * Retorna uma turma específico por nome
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Get(
 *     path="/turmas/get/{pesquisa}",
 *     description="Retorna uma turma específica, passando texto como parâmetro.",
 *     produces={"application/json"},
 *     tags={"turmas.basico"},
 *     @SWG\Parameter(
 *         description="Nome da turma",
 *         in="path",
 *         name="pesquisa",
 *         required=true,
 *         type="string",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * Remove uma turma do banco de dados
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/turmas/delete",
 *     description="Remove um nome do banco de dados. Possíveis status:
 *     [code=0600] = Efetuado com sucesso.
 *     [code=0603] = Campo 'id' é obrigatório.
 *     [code=0604] = Item não foi encontrado ou já foi removido.
 *     [code=0608] = Item não pode ser removido pois está em um Grupo.
 *     [code=0609] = Item não pode ser removido pois está relacionado a um Esquema.
 *     [code=0610] = Item não pode ser removido pois está relacionado a uma Matriz Curricular.",
 *     produces={"application/json"},
 *     tags={"turmas.basico"},
 *     @SWG\Parameter(
 *         name="turma",
 *         in="body",
 *         description="Objeto JSON com os ids das turmas",
 *         @SWG\Schema(ref="#/definitions/TurmaModel"),
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * Salva uma turma
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/turmas/save",
 *     operationId="save",
 *     consumes={"application/json", "application/xml"},
 *     summary="Salva uma turma",
 *     description="Cria ou atualiza uma nome de turma no sistema. Para atualizar basta passar o id do objeto. <br/> Se for necessário gravar vários itens, basta passar o mesmo objeto dentro de um array \[\]. <br/> Possíveis status:
 *     [code=0600] = Efetuado com sucesso.
 *     [code=0601] = Campo 'nome' é obrigatório.
 *     [code=0602] = Campo 'nome' é um valor inválido.
 *     [code=0605] = Item com o ID que foi informado não foi encontrado.
 *     [code=0606] = Atributo enviado é inválido.
 *     [code=0607] = Já possui um item com este nome.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"turmas.basico"},
 *     @SWG\Parameter(
 *         name="turma",
 *         in="body",
 *         description="Objeto JSON com as propriedades da turma",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/TurmaModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */