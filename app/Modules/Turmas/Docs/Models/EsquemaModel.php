<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="EsquemaModel")
 * )
 */
class EsquemaModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_esquema;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_turma;    

    /**
     * @SWG\Property(format="int64")
     * @var int
     */    
    public $id_turno;
}