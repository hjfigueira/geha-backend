<?php 
/**
 * @SWG\Definition(
 *     required={"id_turma", "id_grupo"}, 
 *     type="object", 
 *     @SWG\Xml(name="RelTurmasGrupoModel")
 * )
 */
class RelTurmasGrupoModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_grupo;

    /**
     * @var int[]
     * @SWG\Property(@SWG\Xml(name="turmas",wrapped=true))
     */
    public $turmas;
}