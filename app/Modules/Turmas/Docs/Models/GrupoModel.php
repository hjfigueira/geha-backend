<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="GrupoModel")
 * )
 */
class GrupoModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property(example="Geografia")
     * @var string
     */
    public $nome;
}