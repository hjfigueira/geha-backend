<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="TurmaModel")
 * )
 */
class TurmaModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property(example="Geografia")
     * @var string
     */
    public $nome;

    /**
     * @SWG\Property(example="Primeira geografia")
     * @var string
     */
    public $nome_completo;

    /**
     * @SWG\Property(example="Geo")
     * @var string
     */
    public $abreviatura;

    /**
     * @SWG\Property(example="3")
     * @var int
     */
    public $total_aulas;

    /**
     * @SWG\Property(example="1º A")
     * @var string
     */
    public $nome_sala;

    /**
     * @SWG\Property(example="1")
     * @var int
     */
    public $id_sede;

    /**
     * @SWG\Property(example="#000")
     * @var string
     */
    public $cor;

    /**
     * @SWG\Property(example="1")
     * @var int
     */
    public $minimo_aulas_dia;

    /**
     * @SWG\Property(example="6")
     * @var int
     */
    public $maximo_aulas_dia;


}