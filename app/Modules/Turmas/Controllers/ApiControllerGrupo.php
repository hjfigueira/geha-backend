<?php

namespace App\Modules\Turmas\Controllers;

use App\Modules\Turmas\Models\RelTurmasGruposModel;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Turmas\Models\ApiModel;
use App\Modules\Turmas\Models\TurmasModel;
use App\Modules\Turmas\Classes\TurmaAttach;
use App\Modules\Turmas\Models\TurmasGrupoModel;
use Illuminate\Database\QueryException as QueryException;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiControllerGrupo extends Controller
{

    private $model;


    public function __construct()
    {
        $this->model = new TurmasGrupoModel();
    }


    /**
     * Retorna todos os grupos de turmas salvos no sistema
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(Request $request)
    {
        $filtros = $request->get('filtros',[]);

        $all = $this->model->search($filtros);

        $attach = new TurmaAttach();
        return $attach->getTurmasElement($all);

    }


    /**
     * Salva o grupo de uma turma
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->input();

        if (! isset($params['nome'])) {           
            return ['created' => false, 'code' => '0701', 'status' => '00'];
        }

        if( strlen($params['nome']) < 1 || strlen($params['nome']) > 60) {
            return ['created' => false, 'code' => '0702', 'status' => '00'];
        }

        if(isset($params['nome']))
        {
            $this->model = $this->model->where('nome', $params['nome'])->first();
            if($this->model != null)
            {
                return ['created' => false, 'code' => '0716', 'status' => '00'];
            }
        }

        if (isset($params['id'])) {

            $this->model = $this->model->find($params['id']);
            if ($this->model == null) {
                return ['created' => false, 'code' => '0705', 'status' => '00'];
            }
        }else {

            $this->model = new TurmasGrupoModel();
            $this->model->nome = $params['nome'];

            $this->model->save();

            $grupo_id=$this->model->id;

            if(isset($param['itens'])) {
                foreach ($params['itens'] as $key => $turma_id) {
                    $this->model = new TurmasModel();
                    $idExiste = $this->model->where('id', $turma_id)->exists();
                    if ($idExiste == false) {
                        return ['created' => false, 'code' => '1217', 'status' => '00'];
                    }
                }

                foreach ($params['itens'] as $key => $turma_id) {
                    $this->model = new RelTurmasGruposModel();
                    $this->model->id_turma = $turma_id;
                    $this->model->id_grupo = $grupo_id;
                    $this->model->save();

                }
            }
            return ['created' => true, 'code' => '1200', 'status' => '01', 'id' => $this->model->id];
        }





        return ['created' => true, 'code' => '0700', 'status' => '01', 'id' => $this->model->id];
    }   


    /**
     * Retorna uma turma específico
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $data = $this->model->find($id);

        if ($data == null) return [];

        $attach = new TurmaAttach();

        $data->turmas = $attach->getTurmas($data->id);

        return $data;
    }


    /**
     * Retorna uma turma específico
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($expression)
    {
        $search = explode(' ', $expression);

        //$attach = new TurmaAttach();


        foreach ($search as $key => $name) {

            $name = '%' . $name . '%';;

            $data = $this->model->where('nome', 'like', $name)->get();

            return $data;
        }

        return [];
    }


    /**
     * Remove um nome do banco de dados
     * @return array
     */
    public function destroy(Request $request)
    {
        $status = [];
        $arrayIds = $request->get('ids',[]);

        foreach($arrayIds as $id) {

            if (!isset($id) || $id == null) {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0703', 'status' => '00'];
                continue;
            }

            try{
                $item = $this->model->findOrFail($id);

            }catch(\Exception $error)
            {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0714', 'status' => '00', 'data' => [ 'nome' => null ]];
                continue;
            }

            $hasTurmas = RelTurmasGruposModel::where('id_grupo', $item->id)->exists();
            if($hasTurmas)
            {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0713', 'status' => '00', 'data' => [ 'nome' => $item->nome ]];
                continue;
            }

            if ($item == null || ! $item->delete($id)) {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0704', 'status' => '00', 'data' => [ 'nome' => $item->nome ]];
                continue;
            }

            $status[] = ['id' => $id,'deleted' => true, 'code' => '0700', 'status' => '01', 'data' => [ 'nome' => $item->nome ]];
            continue;

        }

        return $status;
    }

    /**
     * Anexa/desanexa turmas ao grupo
     */
    public function attach(Request $request)
    {
        $turma = new TurmaAttach();

        return $turma->attach($request);
    }


}