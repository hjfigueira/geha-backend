<?php

namespace App\Modules\Turmas\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Turmas\Models\ApiModel;
use App\Modules\Turmas\Models\TurmasModel;
use App\Modules\Turnos\Models\TurnosModel;
use App\Modules\Esquemas\Models\EsquemasModel;
use App\Modules\Turmas\Models\TurmasEsquemaModel;
use Illuminate\Database\QueryException as QueryException;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiControllerEsquemas  extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new TurmasEsquemaModel();
    }

    /**
     * Retorna todos os nomes de turmas salvos no sistema
     * @return array
     */
    public function all() 
    {
        $data = [];
        $turmas =   TurmasModel::all();

        foreach($turmas as $id => $turma)
        {
            $data[$id] = [
                'id' => $turma->id,
                'nome' => $turma->nome,
                'grupos' => $turma->grupos()->get()->toArray(),
            ];

            foreach(TurnosModel::all() as $turno)
            {
                $esquema = TurmasEsquemaModel::where('id_turma',$turma->id)
                    ->where('id_turno', $turno->id)->first();

                if(($objEsquema = EsquemasModel::find(@$esquema->id_esquema)) != null)
                {
                    $idEsquema = @$esquema->id_esquema;
                    $nomeEsquema = @$objEsquema->nome;
                }else{
                    $idEsquema = null;
                    $nomeEsquema = null;
                }

                $data[$id]['esquemas'][] = [
                    'id_turno' => $turno->id,
                    'id_esquema' => $idEsquema,
                    'nome_esquema' => $nomeEsquema,
                ];
            }

        }

        return $data;
    }

    /**
     * Salva o nome de uma turma
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->input();

        $valid = $this->checkParams($params);

        if ( $valid !== true ) {
            $valid['created'] = false;
            return $valid;
        }               

        $valid = $this->checkIntegraty($params);

        if ( $valid !== true ) {
            $valid['created'] = false;
            return $valid;
        }

        return \DB::transaction(function() use ($params){


            TurmasEsquemaModel::where('id_turma', $params['id_turma'])
                ->where('id_turno', $params['id_turno'])
                ->delete();

            $this->model->id_turno   = $params['id_turno'];
            $this->model->id_turma   = $params['id_turma'];
            $this->model->id_esquema = $params['id_esquema'];

            try {
                $this->model->save();
                return ['created' => true, 'code' => '0800', 'status' => '01'];
            } catch (QueryException $e) {
                return ['created' => false, 'code' => '0804', 'status' => '00'];
            }

        });

    }   


    /**
     * Remove um nome do banco de dados
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $params = $request->input();

        $valid = $this->checkParams($params);

        if ( $valid !== true ) {
            $valid['deleted'] = false;
            return $valid;
        }

        $model = $this->model->where('id_esquema', $params['id_esquema'])
                             ->where('id_turno', $params['id_turno'])
                             ->where('id_turma', $params['id_turma'])
                             ->first();

        if ($model == null) {
            return ['deleted' => false, 'code' => '0808', 'status' => '00'];
        }

        try {

            $this->model->where('id_esquema', $params['id_esquema'])
                        ->where('id_turno', $params['id_turno'])
                        ->where('id_turma', $params['id_turma'])
                        ->delete();

            return ['deleted' => true, 'code' => '0800', 'status' => '01'];

        } catch (QueryException $e) {

            return ['deleted' => false, 'code' => '0809', 'status' => '00'];

        }
    }


    /**
     * Verifica se todos os parâmetros foram passados corretamente
     * @param  array $params 
     * @return mixed
     */
    protected function checkParams($params)
    {
        if (! isset($params['id_turma'])) {           
            return ['created' => false, 'code' => '0801', 'status' => '00'];
        }

        elseif (! isset($params['id_turno'])) {
            return ['created' => false, 'code' => '0802', 'status' => '00'];
        }

        elseif (! isset($params['id_esquema'])) {
            return ['created' => false, 'code' => '0803', 'status' => '00'];
        }   

        return true;
    }

    /**
     * Verifica a integridade no banco de dados dos parâmetros passados
     * @param  array $params 
     * @return mixed         
     */
    protected function checkIntegraty($params)
    {
        if (TurmasModel::find($params['id_turma']) == null) {
            return ['code' => '0805', 'status' => '00'];
        }    

        elseif (TurnosModel::find($params['id_turno']) == null) {
            return ['code' => '0806', 'status' => '00'];
        }

        elseif (EsquemasModel::find($params['id_esquema']) == null) {
            return ['code' => '0807', 'status' => '00'];
        }

        return true;
    }
}
