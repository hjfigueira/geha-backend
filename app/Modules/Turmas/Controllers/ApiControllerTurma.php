<?php

namespace App\Modules\Turmas\Controllers;

use DB;
use Schema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Turmas\Models\ApiModel;
use App\Modules\Turmas\Models\TurmasModel;
use Illuminate\Database\QueryException as QueryException;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiControllerTurma extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new TurmasModel();
    }

    /**
     * Retorna todas as turmas cadastradas
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(Request $request)
    {
        $data = [];
        $params = $request->get('filtros',[]);

        $itens = TurmasModel::search($params);
        return $itens;
    }

    /**
     * Retorna uma turma específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $data = $this->model->find($id);

        if ($data == null) return [];

        return $data;
    }


    /**
     * Retorna uma turma específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($expression)
    {
        $search = explode(' ', $expression);

        foreach ($search as $key => $name) {

            $name = '%' . $name . '%';;

            $data = $this->model->where('nome', 'like', $name)->get();
        
            return $data;
        }

        return null;
    }


    /**
     * Remove um nome do banco de dados
     * @return array
     */
    public function destroy(Request $request)
    {
        $status = [];
        $arrayIds = $request->get('ids',[]);

        foreach($arrayIds as $id)
        {
            if (!isset($id) || $id == null) {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0603', 'status' => '00', 'data' => [ 'nome' => null ]];
                continue;
            }

            try{
                $item = $this->model->findOrFail($id);
            }catch(\Exception $error)
            {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0604', 'status' => '00', 'data' => [ 'nome' => null ]];
                continue;
            }

            if($item->grupos()->count() > 0)
            {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0608', 'status' => '00', 'data' => [ 'nome' => $item->nome ]];
                continue;
            }

            if($item->esquemas()->count() > 0)
            {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0609', 'status' => '00', 'data' => [ 'nome' => $item->nome ]];
                continue;
            }

            if($item->disciplinas()->count() > 0)
            {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0610', 'status' => '00', 'data' => [ 'nome' => $item->nome ]];
                continue;
            }

            if ($item == null || ! $item->delete($id))
            {
                $status[] = ['id' => $id,'deleted' => false, 'code' => '0604', 'status' => '00', 'data' => [ 'nome' => $item->nome ]];
                continue;
            }

            $status[] = ['id' => $id,'deleted' => true, 'code' => '0600', 'status' => '01', 'data' => [ 'nome' => $item->nome ]];
        }
        return $status;
    }


    public function save(Request $request)
    {
        DB::beginTransaction();

        $params = $request->input();
        
        if (! isset($params['nome']) && ! empty($params)) {

            foreach ($params as $key => $value) {

                $ret = $this->saveItem($value);
            
                if ($ret['created'] == false) {
                    return $ret;
                }   
            }    

            DB::commit();

            return $ret;
        } 

        else {
            $ret = $this->saveItem($params);

            if ($ret['created'] == true) {
                DB::commit();
            }

            return $ret;
        }
    }

    /**
     * Salva o grupo de uma turma
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveItem($params)
    {

        if (! isset($params['nome'])) {           
            return ['created' => false, 'code' => '0601', 'status' => '00'];
        }

        if( strlen($params['nome']) < 1 || strlen($params['nome']) > 60) {
            return ['created' => false, 'code' => '0602', 'status' => '00'];
        }

        if (isset($params['id'])) {
            
            $this->model = TurmasModel::find($params['id']);
        
            if ($this->model == null) {
                return ['created' => false, 'code' => '0605', 'status' => '00'];
            }

            print_r($params['id']);

        } else {
            $this->model = new TurmasModel();
        }

        return $this->saveTurma($params);
    }  


    /**
     * Trata as informações e salva no BD
     * @param  array $params  Dados vindo do usuário
     * @return array         
     */
    protected function saveTurma($params)
    {
        foreach ($params as $key => $value) {

            if (Schema::hasColumn($this->model->table, $key) == false) {
                return ['created' => false, 'code' => '0606', 'status' => '00'];
            }
        
            $this->model->$key = $value;
        }

        try {
            
            $this->model->save();

        } catch (QueryException $e) {
            DB::rollBack();
            return ['created' => false, 'code' => '0607', 'status' => '00'];
        }

        return ['created' => true, 'code' => '0600', 'status' => '01'];
    }
}
