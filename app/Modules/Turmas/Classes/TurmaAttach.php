<?php

namespace App\Modules\Turmas\Classes;

use DB;
use QueryException;
use App\Modules\Turmas\Models\TurmasModel;
use App\Modules\Turmas\Models\TurmasGrupoModel;
use App\Modules\Turmas\Models\RelTurmasGruposModel;

class TurmaAttach {

    public function __construct()
    {
        //
    }

    /**
     * Anexa uma turma ao grupo
     * @param  Request $request 
     * @return json           
     */
    public function attach($request)
    {
        DB::beginTransaction();

        $params = $request->input();

        $check = $this->checkAttach($params);

        if ($check !== true) return $check;

        $check = $this->removeAttachs($params);

        if ($check !== true) return $check;

        if (empty($params['turmas'])) {
            DB::commit();
            return ['attach' => true, 'code' => '0700', 'status' => '01'];
        }

        return $this->saveAttach($params);
    }


    /**
     * Pega as turmas de cada elemento
     */
    public function getTurmasElement($all)
    {
        if ($all == null) return [];                

        foreach ($all as &$item) {
            
            $item->turmas = $this->getTurmas($item->id);
        }

        return $all;
    }


    /**
     * Verifica os dados enviados pelo usuário para anexar uma turma ao grupo
     * @param  array $params 
     * @return mixed
     */
    protected function checkAttach($params)
    {
        if (empty($params)) {
           return ['attach' => false, 'code' => '0712', 'status' => '00'];
        }

        if (! isset($params['id_grupo'])) {
           return ['attach' => false, 'code' => '0706', 'status' => '00'];
        }
        if (TurmasGrupoModel::find($params['id_grupo']) == null) {
           return ['attach' => false, 'code' => '0710', 'status' => '00'];
        } 

        if (! isset($params['turmas']) ) {
           return ['attach' => false, 'code' => '0707', 'status' => '00'];
        }

        return true;
    }

    /**
     * Limpa todos as turmas antes de salvar as turmas novas
     * @return mixed
     */
    protected function removeAttachs($params)
    {
        try {
            
            $models = RelTurmasGruposModel::where('id_grupo', $params['id_grupo']);
            
            $models->delete();

            return true;                        

        } catch (QueryException $e) {
            return ['attach' => false, 'code' => '0708', 'status' => $e->getMessage()];
        }
    }

    /**
     * Salva os dados de turma ao grupo
     * @param   $params
     * @return  mixed
     */
    protected function saveAttach($params)
    {
        foreach ($params['turmas'] as $turma) {
            if (TurmasModel::find($turma) == null) {
               return ['attach' => false, 'code' => '0709', 'status' => '00'];
            }   
            
            $model = new RelTurmasGruposModel();

            $model->id_grupo = $params['id_grupo'];
            $model->id_turma = $turma;

            try {

                $model->save();

            } catch (QueryException $e) {
                DB::rollback();
                return ['attach' => false, 'code' => '0711', 'status' => '00'];
            }
        }

        DB::commit();

        return ['attach' => true, 'code' => '0700', 'status' => '01'];
    }


    /**
     * Retorna as turmas do grupo
     * @param  int $id 
     * @return array
     */
    public function getTurmas($id)
    {
        $collection = RelTurmasGruposModel::where('id_grupo', $id)->get();

        $list = [];

        foreach ($collection as $item) {

            $turma = TurmasModel::where('id', $item->id_turma)->first();

            if ($turma == null) continue;

            $list[] = ['id' => $turma->id, 'nome' => $turma->nome];
        }

        return $list;
    } 
}