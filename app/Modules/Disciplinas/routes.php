<?php
/*
|--------------------------------------------------------------------------
| Disciplinas Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Disciplinas module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group([
    'prefix' => 'api/disciplinas',
    'namespace' => 'App\Modules\Disciplinas\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',     	   ['as' => 'api/disciplinas.all',     'uses' => 'ApiControllerDisciplinas@all']);	
	Route::get('get/{id}',     ['as' => 'api/disciplinas.get',     'uses' => 'ApiControllerDisciplinas@get'])->where('id', '[0-9]+');	
	Route::get('get/{name}',   ['as' => 'api/disciplinas.search',  'uses' => 'ApiControllerDisciplinas@search'])->where('name', '[A-Za-z ]+');	
	Route::post('save',        ['as' => 'api/disciplinas.save',    'uses' => 'ApiControllerDisciplinas@save']);	
	Route::post('delete',   ['as' => 'api/disciplinas.destroy', 'uses' => 'ApiControllerDisciplinas@destroy']);
});

Route::group([
    'prefix' => 'api/disciplinas-grupos',
    'namespace' => 'App\Modules\Disciplinas\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',     	   ['as' => 'api/disciplinas-grupos.all',     'uses' => 'ApiControllerDisciplinasGrupo@all']);	
	Route::get('get/{id}',     ['as' => 'api/disciplinas-grupos.get',     'uses' => 'ApiControllerDisciplinasGrupo@get'])->where('id', '[0-9]+');	
	Route::get('get/{name}',   ['as' => 'api/disciplinas-grupos.search',  'uses' => 'ApiControllerDisciplinasGrupo@search'])->where('name', '[A-Za-z ]+');	
	Route::post('save',        ['as' => 'api/disciplinas-grupos.save',    'uses' => 'ApiControllerDisciplinasGrupo@save']);	
	Route::post('delete',       ['as' => 'api/disciplinas-grupos.destroy', 'uses' => 'ApiControllerDisciplinasGrupo@destroy']);
	Route::post('attach', 	   ['as' => 'api/disciplinas-grupos.attach',  'uses' => 'ApiControllerDisciplinasGrupo@attach']);	
});
