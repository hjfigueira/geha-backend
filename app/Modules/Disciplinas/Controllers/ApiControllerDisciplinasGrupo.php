<?php

namespace App\Modules\Disciplinas\Controllers;

use App\Modules\Disciplinas\Models\DisciplinasModel;
use App\Modules\Disciplinas\Models\RelDisciplinasGruposModel;
use Schema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Disciplinas\Classes\DisciplinaAttach;
use App\Modules\Disciplinas\Models\DisciplinasGrupoModel;
use Illuminate\Database\QueryException as QueryException;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiControllerDisciplinasGrupo extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new DisciplinasGrupoModel();
    }

    /**
     * Retorna todos os nomes de disciplinas salvos no sistema
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all() 
    {
        $all = $this->model->all();

        $attach = new DisciplinaAttach();

        return $attach->getDisciplinasElement($all);
    }

    /**
     * Salva o nome de uma disciplina
     *
     * @return \Illuminate\Http\JsonResponse
     * @todo Verificar se tem jeito de unificar a função attach e save
     * Porque basicamente hoje fazem as mesmas coisas 
     */
    public function save(Request $request)
    {
        $params = $request->input();

        if (! isset($params['nome'])) {           
            return ['created' => false, 'code' => '1001', 'status' => '00'];
        }

        if( strlen($params['nome']) < 1 || strlen($params['nome']) > 60) {
            return ['created' => false, 'code' => '1002', 'status' => '00'];
        }

        if(isset($params['nome']))
        {
            $nomeExiste = $this->model->where('nome', $params['nome'])->exists();
            if($nomeExiste)
            {
                return ['created' => false, 'code' => '1016', 'status' => '00'];
            }
        }

        if (isset($params['id'])) {
            $this->model = $this->model->find($params['id']);
        
            if ($this->model == null) {
                return ['created' => false, 'code' => '1005', 'status' => '00'];
            }

        } else {
            $this->model = new DisciplinasGrupoModel();
        }

        $grupo_id = $this->model->id;

        if (isset($param['itens'])) {
            $resp = $this->saveItensGrupo($param['itens']);

            if ($resp == false) {
                return ['created' => false, 'code' => '1017', 'status' => '00'];
            }
        }
        
        $ret = $this->saveData($params);

        if ($ret !== true) {
            return $ret;
        }
            
        return ['created' => true, 'code' => '1000', 
                'status'  => '01',  'id' => $this->model->id];


    }   


    /**
     * Retorna uma disciplina específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $data = $this->model->find($id);

        if ($data == null) return [];

        $attach = new DisciplinaAttach();

        $data->disciplinas = $attach->getDisciplinas($data->id);

        return $data;
    }


    /**
     * Retorna uma disciplina específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($expression)
    {
        $search = explode(' ', $expression);

        $attach = new DisciplinaAttach();

        foreach ($search as $key => $name) {

            $name = '%' . $name . '%';;

            $data = $this->model->where('nome', 'like', $name)->get();
        
            return $attach->getDisciplinasElement($data);
        }

        return [];
    }


    /**
     * Remove um nome do banco de dados
     * @return array
     */
    public function destroy(Request $request)
    {
        $status = [];
        $arrayIds = $request->get('ids',[]);

        foreach($arrayIds as $id)
        {
            try{
                $item = $this->model->findOrFail($id);
            }
            catch(\Exception $error)
            {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1004',
                    'status' => '00',
                    'data' => [
                        'nome' => null
                    ]];
                continue;
            }

            $hasDisciplinas = RelDisciplinasGruposModel::where('id_grupo', $item->id)
                ->exists();

            if($hasDisciplinas)
            {
                $status[] =  [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1013',
                    'status' => '00',
                    'data' => [
                        'nome' => $item->nome
                    ]];
                continue;
            }

            if ($item == null || ! $item->delete($id)) {
                $status[] =  [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1004',
                    'status' => '00',
                    'data' => [
                        'nome' => $item->nome
                    ]];
                continue;
            }

            $status[] =  [
                'id' => $id,
                'deleted' => true,
                'code' => '1000',
                'status' => '01',
                'data' => [
                    'nome' => $item->nome
                ]];
                continue;
        }

        return $status;
    }


    /**
     * Anexa/desanexa disciplinas ao grupo
     * @todo Remover workarround para salvar
     * os outros campos de disciplinas e deixar apenas 
     * em uma função. 
     */
    public function attach(Request $request)
    {
        $params = $request->input();

        $disciplina = new DisciplinaAttach();

        $grupo = DisciplinasGrupoModel::find($params['id_grupo']);

        $grupo->pref_aulas_no_dia   = $params['pref_aulas_no_dia'];
        $grupo->pref_aulas_seguidas = $params['pref_aulas_seguidas'];

        $grupo->save();
        
        return $disciplina->attach($request);
    }


    /**
     * Trata e salva os dados no banco de dados
     * @param  array $params 
     * @return array
     */
    protected function saveData($params)
    {
        foreach ($params as $key => $value) {

            if (Schema::hasColumn($this->model->table, $key) == false) {
                return ['created' => false, 'code' => '1006', 'status' => '00'];
            }
        
            $this->model->$key = $value;
        }

        try {
            
            $this->model->save();

            return true;
            
        } catch (QueryException $e) {
            return ['created' => false, 'code' => '1007', 'status' => '00'];
        }

        return ['created' => true, 'code' => '1000', 'status' => '01', 'id' => $this->model->id];
    }

    /**
     * Função auxiliar para salva os itens dos grupos 
     * para que não seja criados grupos vazios
     * @param  array $itens 
     * @todo  Verificar posteriormente se é possível substitui-la 
     * pela função attach já existente
     * @return bool       
     */
    protected function saveItensGrupo($itens)
    {
        foreach ($itens as $key => $disciplina_id) {
            $model = new DisciplinasModel();
            $idExiste = $model->where('id', $disciplina_id)->exists();
            if ($idExiste == false) {
                return false;
            }
        }
               
        foreach ($params['itens'] as $key => $disciplina_id) {
            $model = new RelDisciplinasGruposModel();
            $model->id_disciplina = $disciplina_id;
            $model->id_grupo = $grupo_id;
            $model->save();
        }

        return true;        
    }
}