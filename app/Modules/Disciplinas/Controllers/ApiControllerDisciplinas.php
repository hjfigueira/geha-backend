<?php

namespace App\Modules\Disciplinas\Controllers;


use Schema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\QueryException as QueryException;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiControllerDisciplinas extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new DisciplinasModel();
    }

    /**
     * Retorna todos os nomes de disciplinas salvos no sistema
     *
     * @return array
     */
    public function all(Request $request)
    {
        $filters = $request->get('filtros',[]);
        $itens = DisciplinasModel::search($filters);
        return $itens;
    }

    /**
     * Salva o nome de uma turma
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->input();

        if (! isset($params['nome'])) {           
            return ['created' => false, 'code' => '0901', 'status' => '00'];
        }

        if( strlen($params['nome']) < 1 || strlen($params['nome']) > 60) {
            return ['created' => false, 'code' => '0902', 'status' => '00'];
        }

        if (isset($params['id'])) {
            $this->model = $this->model->find($params['id']);

            if ($this->model == null) {
                return ['created' => false, 'code' => '0905', 'status' => '00'];
            }
        }

        if(isset($params['nome']))
        {
            $nomeExiste = $this->model->where('nome', $params['nome'])->exists();
            if($nomeExiste)
            {
                return ['created' => false, 'code' => '0916', 'status' => '00'];
            }
        }

        return $this->saveData($params);
    }   


    /**
     * Retorna uma turma específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $data = $this->model->find($id);

        if ($data == null) return [];

        return $data;
    }


    /**
     * Retorna uma turma específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($expression)
    {
        $search = explode(' ', $expression);

        foreach ($search as $key => $name) {

            $name = '%' . $name . '%';;

            $data = $this->model->where('nome', 'like', $name)->get();
        
            if ( ! $data->isEmpty()) {
                return $data;
            } 
        }

        return null;
    }


    /**
     * Remove um nome do banco de dados
     * @return array
     */
    public function destroy(Request $request)
    {
        $status = [];
        $arrayIds = $request->get('ids',[]);

        foreach($arrayIds as $id)
        {
            try{
                $item = $this->model->findOrFail($id);
            }
            catch (\Exception $error)
            {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '0904',
                    'status' => '00',
                    'data' => [
                        'nome' => null
                    ]];
                continue;
            }

            if($item->grupos()->count() > 0)
            {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '0908',
                    'status' => '00',
                    'data' => [
                        'nome' => $item->nome
                    ]];
                continue;
            }

            if($item->turmas()->count() > 0)
            {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '0909',
                    'status' => '00',
                    'data' => [
                        'nome' => $item->nome
                    ]];
                continue;
            }

            if ($item == null || ! $item->delete($id)) {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '0904',
                    'status' => '00'];
                continue;
            }

            $status[] = [
                'id' => $id,
                'deleted' => true,
                'code' => '0900',
                'status' => '01'];
        }

        return $status;
    }



    protected function saveData($params)
    {
        foreach ($params as $key => $value) {

            if (Schema::hasColumn($this->model->table, $key) == false) {
                return ['created' => false, 'code' => '0906', 'status' => '00'];
            }
        
            $this->model->$key = $value;
        }

        try {
            
            $this->model->save();
            
        } catch (QueryException $e) {
            return ['created' => false, 'code' => '0907', 'status' => '00'];
        }

        return ['created' => true, 'code' => '0900', 'status' => '01'];
    }
}
