<?php 
/**
 * @SWG\Definition(
 *     required={"disciplinas", "id_grupo"}, 
 *     type="object", 
 *     @SWG\Xml(name="RelDisciplinasGrupoModel")
 * )
 */
class RelDisciplinasGrupoModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_grupo;

    /**
     * @var int[]
     * @SWG\Property(@SWG\Xml(name="disciplinas",wrapped=true))
     */
    public $disciplinas;
}