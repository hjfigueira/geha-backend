<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="GrupoDisciplinaModel")
 * )
 */
class GrupoDisciplinaModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property(example="Geografia")
     * @var string
     */
    public $nome;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $pref_aulas_no_dia;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $pref_aulas_seguidas;
}