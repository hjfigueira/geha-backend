<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="DisciplinaModel")
 * )
 */
class DisciplinaModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property(example="Geografia")
     * @var string
     */
    public $nome;

    /**
     * @SWG\Property(example="Primeira geografia")
     * @var string
     */
    public $nome_completo;

    /**
     * @SWG\Property(example="Geo")
     * @var string
     */
    public $abreviatura;


    /**
     * @SWG\Property(example="#000")
     * @var string
     */
    public $cor;
}