<?php

     
/**
 *
 * @SWG\Get(
 *     path="/disciplinas-grupos/all",
 *     summary="Retorna todos os registros de grupos de disciplinas salvos no sistema.",
 *     description="Retorna todos os registros de grupos de disciplinas salvos no sistema",
 *     operationId="api.disciplinas.index",
 *     produces={"application/json"},
 *     tags={"disciplinas.grupos"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Get(
 *     path="/disciplinas-grupos/get/{grupoID}",
 *     summary="Retorna um grupo específico, passando um ID parâmetro.",
 *     description="Retorna um grupo específico, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"disciplinas.grupos"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID da disciplina",
 *         in="path",
 *         name="grupoID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 *
 * @SWG\Get(
 *     path="/disciplinas-grupos/get/{pesquisa}",
 *     summary="Retorna um grupo específico, passando texto como parâmetro.",
 *     description="Retorna um grupo específico, passando texto como parâmetro.",
 *     produces={"application/json"},
 *     tags={"disciplinas.grupos"},
 *     @SWG\Parameter(
 *         description="Nome da disciplina",
 *         in="path",
 *         name="pesquisa",
 *         required=true,
 *         type="string",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/disciplinas-grupos/save",
 *     operationId="save",
 *     consumes={"application/json"},
 *     summary="Salva um grupo de disciplina",
 *     description="Cria ou atualiza um grupo de disciplina no sistema. Para atualizar basta passar o id do objeto. Possíveis status:
 *     [code=1000] = Efetuado com sucesso.
 *     [code=1001] = Campo 'nome' é obrigatório.
 *     [code=1002] = Campo 'nome' é um valor inválido.
 *     [code=1005] = Item com o ID que foi informado não foi encontrado.
 *     [code=1006] = Atributo enviado é inválido.
 *     [code=1007] = Já possui um item com este nome.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"disciplinas.grupos"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/GrupoDisciplinaModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/disciplinas-grupos/delete",
 *     summary="Remove um grupo do banco de dados.",
 *     description="Remove um grupo do banco de dados. Possíveis status:
 *     [code=1000] = Efetuado com sucesso.
 *     [code=1003] = Campo 'id' é obrigatório.
 *     [code=1004] = Item não foi encontrado ou já foi removido.
 *     [code=1013] = Grupo não pode ser excluido pois possui disciplinas relacionadas.",
 *     produces={"application/json"},
 *     tags={"disciplinas.grupos"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="Objeto array JSON para exclusão",
 *         @SWG\Schema(ref="#/definitions/DisciplinaModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * Anexa uma turma à um grupo
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/disciplinas-grupos/attach",
 *     operationId="save",
 *     consumes={"application/json", "application/xml"},
 *     summary="Anexa uma turma à um grupo",
 *     description="Anexa disciplinas à um grupo no sistema. Para limpar todas as disciplinas basta passar disciplinas como []. Possíveis status:
 *     [code=1000] = Efetuado com sucesso.
 *     [code=1006] = Campo 'id_grupo' é obrigatório.
 *     [code=1007] = Campo 'disciplinas' é obrigatório.
 *     [code=1008] = Erro ao desvincular turma. Verifique se ela não está sendo usada.
 *     [code=1009] = Turma não encontrada.
 *     [code=1010] = Grupo não encontrado.
 *     [code=1011] = Erro ao salvar dados no banco de dados.
 *     [code=1012] = Parâmetro nulo ou objeto JSON mal formatado.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"disciplinas.grupos"},
 *     @SWG\Parameter(
 *         name="turma",
 *         in="body",
 *         description="Objeto JSON com as propriedades da turma",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/RelDisciplinasGrupoModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */