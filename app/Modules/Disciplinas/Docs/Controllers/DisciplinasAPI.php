<?php

     
/**
 *
 * @SWG\Get(
 *     path="/disciplinas/all",
 *     description="Retorna todos os registros de disciplinas salvos no sistema",
 *     operationId="api.disciplinas.index",
 *     produces={"application/json"},
 *     tags={"disciplinas.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Get(
 *     path="/disciplinas/get/{disciplinaID}",
 *     description="Retorna uma disciplina específica, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"disciplinas.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID da disciplina",
 *         in="path",
 *         name="disciplinaID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 *
 * @SWG\Get(
 *     path="/disciplinas/get/{pesquisa}",
 *     description="Retorna uma disciplina específica, passando texto como parâmetro.",
 *     produces={"application/json"},
 *     tags={"disciplinas.basico"},
 *     @SWG\Parameter(
 *         description="Nome da disciplina",
 *         in="path",
 *         name="pesquisa",
 *         required=true,
 *         type="string",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/disciplinas/save",
 *     operationId="save",
 *     consumes={"application/json"},
 *     summary="Salva uma disciplina",
 *     description="Cria ou atualiza um nome de disciplina no sistema. Para atualizar basta passar o id do objeto. Possíveis status:
 *     [code=0900] = Efetuado com sucesso.
 *     [code=0901] = Campo 'nome' é obrigatório.
 *     [code=0902] = Campo 'nome' é um valor inválido.
 *     [code=0905] = Item com o ID que foi informado não foi encontrado.
 *     [code=0906] = Atributo enviado é inválido.
 *     [code=0907] = Já possui um item com este nome.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"disciplinas.basico"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/DisciplinaModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/disciplinas/delete",
 *     description="Remove um nome do banco de dados. Possíveis status:
 *     [code=0900] = Efetuado com sucesso.
 *     [code=0903] = Campo 'id' é obrigatório.
 *     [code=0904] = Item não foi encontrado ou já foi removido.
 *     [code=0908] = Item não pode ser removido pois está em um Grupo.
 *     [code=0909] = Item não pode ser removido pois está em uma Matriz Curricular.",
 *     produces={"application/json"},
 *     tags={"disciplinas.basico"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="Objeto array JSON para exclusão",
 *         @SWG\Schema(ref="#/definitions/DisciplinaModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */