<?php 

namespace App\Modules\Disciplinas\Models;

use App\Models\WsClientModel;

class DisciplinasModel extends WsClientModel
{
	// Table in database
	public $table = 'Disciplinas';


    public function grupos(){

        return $this->belongsToMany(
            'App\Modules\Disciplinas\Models\DisciplinasGrupoModel',
            'Rel_Disciplinas_Grupos',
            'id_disciplina',
            'id_grupo'
        );

    }

    public function turmas()
    {
        return $this->belongsToMany(
            'App\Modules\Turmas\Models\PerguntasModel',
            'Matriz_Curricular',
            'id_disciplina',
            'id_turma');
    }

    public function scopeFiltrarGrupo($query, $grupo_id = null)
    {
        if(!empty($grupo_id)) {

            return $query->whereHas('grupos', function ($query) use ($grupo_id) {
                $query->where('id', $grupo_id);
            });
        }
    }

    public function scopeFiltrarCor($query, $cor = null)
    {
        if(!empty($cor)) {
            return $query->where('cor', $cor);
        }
    }

    public function scopeFiltrarPalavraChave($query, $texto)
    {
        if(!empty($texto)){
            $query->where(function($subQuery) use ($texto){

                foreach(explode(' ',$texto) as $word)
                {
                    $subQuery->orWhere('nome','like','%'.$word.'%');
                    $subQuery->orWhere('abreviatura','like','%'.$word.'%');
                    $subQuery->orWhere('nome_completo','like','%'.$word.'%');
                }
            });
        }
    }

    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
    public static function search(array $filtro = [])
    {
        $searchReturn = [];
        $disciplinasQuery = DisciplinasModel::select('*');

        $grupo_id       = array_get($filtro,'grupo_id',null);
        $color          = array_get($filtro,'color',null);
        $keyword        = array_get($filtro,'keyword',null);

        //Filtro do ID do grupo
        $disciplinas = $disciplinasQuery
            ->filtrarGrupo($grupo_id)
            ->filtrarCor($color)
            ->filtrarPalavraChave($keyword)
            ->get();

        foreach($disciplinas as $disciplina)
        {
            $searchReturn[] = array_merge($disciplina->toArray(), [
                'grupos' => $disciplina->grupos()->get()->toArray()
            ]);
        }

        return $searchReturn;
    }

    public function busca($id)
    {
        $disciplinasQuery = DisciplinasModel::select('*');
        $disciplinas = $disciplinasQuery
            ->where('id',$id)
            ->get();
    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }
}