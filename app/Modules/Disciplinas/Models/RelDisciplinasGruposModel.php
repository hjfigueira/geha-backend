<?php 

namespace App\Modules\Disciplinas\Models;

use Illuminate\Database\Eloquent\Model;

class RelDisciplinasGruposModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Rel_Disciplinas_Grupos';

	// Connection Database
	protected $connection = 'urania-cliente';

}