<?php 

namespace App\Modules\Disciplinas\Models;

use Illuminate\Database\Eloquent\Model;

class DisciplinasGrupoModel extends Model
{

	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Disciplinas_Grupos';

	// Connection Database
	protected $connection = 'urania-cliente';

	public function disciplinas(){

        return $this->belongsToMany(
            'App\Modules\Disciplinas\Models\DisciplinaModel',
            'Rel_Disciplinas_Grupos',
            'id_disciplina',
            'id_grupo'
        );

    }
}