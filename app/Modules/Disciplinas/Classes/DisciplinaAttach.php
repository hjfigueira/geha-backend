<?php

namespace App\Modules\Disciplinas\Classes;

use DB;
use QueryException;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use App\Modules\Disciplinas\Models\DisciplinasGrupoModel;
use App\Modules\Disciplinas\Models\RelDisciplinasGruposModel;

class DisciplinaAttach {

    public function __construct()
    {
        //
    }

    /**
     * Anexa uma disciplina ao grupo
     * @param  Request $request 
     * @return json           
     */
    public function attach($request)
    {
        DB::beginTransaction();

        $params = $request->input();

        $check = $this->checkAttach($params);

        if ($check !== true) return $check;

        $check = $this->removeAttachs($params);

        if ($check !== true) return $check;

        if (empty($params['disciplinas'])) {
            DB::commit();
            return ['attach' => true, 'code' => '1000', 'status' => '01'];
        }

        return $this->saveAttach($params);
    }


    /**
     * Pega as disciplinas de cada elemento
     */
    public function getDisciplinasElement($all)
    {
        if ($all == null) return [];                

        foreach ($all as &$item) {
            
            $item->disciplinas = $this->getDisciplinas($item->id);
        }

        return $all;
    }


    /**
     * Verifica os dados enviados pelo usuário para anexar uma disciplina ao grupo
     * @param  array $params 
     * @return mixed
     */
    protected function checkAttach($params)
    {
        if (empty($params)) {
           return ['attach' => false, 'code' => '1012', 'status' => '00'];
        }

        if (! isset($params['id_grupo'])) {
           return ['attach' => false, 'code' => '1006', 'status' => '00'];
        }

        if (DisciplinasGrupoModel::find($params['id_grupo']) == null) {
           return ['attach' => false, 'code' => '1010', 'status' => '00'];
        } 

        if (! isset($params['disciplinas']) ) {
           return ['attach' => false, 'code' => '1007', 'status' => '00'];
        }

        return true;
    }

    /**
     * Limpa todos as disciplinas antes de salvar as disciplinas novas
     * @return mixed
     */
    protected function removeAttachs($params)
    {
        try {
            
            $models = RelDisciplinasGruposModel::where('id_grupo', $params['id_grupo']);
            
            $models->delete();

            return true;                        

        } catch (QueryException $e) {
            return ['attach' => false, 'code' => '1008', 'status' => $e->getMessage()];
        }
    }

    /**
     * Salva os dados de disciplina ao grupo
     * @param   $params
     * @return  mixed
     */
    protected function saveAttach($params)
    {
        foreach ($params['disciplinas'] as $disciplina) {

            if (DisciplinasModel::find($disciplina) == null) {
               return ['attach' => false, 'code' => '1009', 'status' => '00'];
            }   
            
            $model = new RelDisciplinasGruposModel();

            $model->id_grupo            = $params['id_grupo'];
            $model->id_disciplina       = $disciplina;

            try {

                $model->save();

            } catch (QueryException $e) {
                DB::rollback();
                return ['attach' => false, 'code' => '1011', 'status' => '00'];
            }
        }

        DB::commit();

        return ['attach' => true, 'code' => '1000', 'status' => '01'];
    }


    /**
     * Retorna as disciplinas do grupo
     * @param  int $id 
     * @return array
     */
    public function getDisciplinas($id)
    {
        $collection = RelDisciplinasGruposModel::where('id_grupo', $id)->get();

        $list = [];

        foreach ($collection as $item) {

            $disciplina = DisciplinasModel::where('id', $item->id_disciplina)->first();

            if ($disciplina == null) continue;

            $list[] = ['id' => $disciplina->id, 'nome' => $disciplina->nome];
        }

        return $list;
    } 
}