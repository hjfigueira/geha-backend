<?php
/*
|--------------------------------------------------------------------------
| Atividades Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Atividades module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/
Route::group([
    'prefix' => 'api/utils',
    'namespace' => 'App\Modules\Utils\Controllers'],
    function () {
	    Route::get('status', ['as' => 'api/utils.status', 'uses' => 'ApiController@status']);
});