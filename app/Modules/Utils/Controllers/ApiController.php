<?php namespace App\Modules\Utils\Controllers;

use App\Modules\Disciplinas\Models\DisciplinasModel;
use App\Modules\Esquemas\Models\EsquemasModel;
use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use App\Modules\PerguntasBasicas\Models\InformacoesBasicasModel;
use App\Modules\Professores\Models\ProfessoresModel;
use App\Modules\Professores\Models\ProfessoresGruposModel;
use App\Modules\Turmas\Models\TurmasGrupoModel;
use App\Modules\Turmas\Models\TurmasModel;
use App\Modules\Turnos\Models\TurnosModel;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    public function status()
    {
        $currentOverallStatus = [
            'perguntas-basicas' => true,
            'turnos' => false,
            'esquemas' => false,
            'turmas' => [
                'nome' => false,
                'grupos' => false,
            ],
            'disciplinas' => [
                'nome' => false,
                'grupos' => false,
            ],
            'professores' => [
                'nome' => false,
                'grupos' => false,
            ],
            'matriz' => false,
            'atribuicoes' => false,
            'tipos-gerais' => false,
            'tipos-especificos' => false,
        ];

        if(InformacoesBasicasModel::isFilled())
        {$currentOverallStatus['turnos'] = true;}

        if(TurnosModel::isFilled())
        {$currentOverallStatus['esquemas'] = true;}

        if(EsquemasModel::isFilled())
        {
            $currentOverallStatus['turmas']['nome'] = true;
            $currentOverallStatus['disciplinas']['nome'] = true;
            $currentOverallStatus['professores']['nome'] = true;
        }

        if(TurmasModel::isFilled())
        {
            $currentOverallStatus['turmas']['grupos'] = true;
        }

        if(DisciplinasModel::isFilled())
        {$currentOverallStatus['disciplinas']['grupos'] = true;}

        if(ProfessoresModel::isFilled())
        {
            $currentOverallStatus['professores']['grupos'] = true;
        }

        if(TurmasModel::isFilled() and DisciplinasModel::isFilled())
        {
            $currentOverallStatus['matriz'] = true;
        }

        if(MatrizCurricularModel::isFilled())
        {
            $currentOverallStatus['atribuicoes'] = true;
        }

        return new JsonResponse($currentOverallStatus);
    }
}