<?php

namespace App\Modules\Professores\Classes;

use DB;
use QueryException;
use App\Modules\Professores\Models\ProfessoresModel;
use App\Modules\Professores\Models\ProfessoresGruposModel;
use App\Modules\Professores\Models\RelProfessoresGruposModel;

class ProfessorAttach {

    public function __construct()
    {
        //
    }

    /**
     * Anexa uma professor ao grupo
     * @param  Request $request 
     * @return json           
     */
    public function attach($request)
    {
        DB::beginTransaction();

        $params = $request->input();

        $check = $this->checkAttach($params);

        if ($check !== true) return $check;

        $check = $this->removeAttachs($params);

        if ($check !== true) return $check;

        if (empty($params['professores'])) {
            DB::commit();
            return ['attach' => true, 'code' => '1200', 'status' => '01'];
        }

        return $this->saveAttach($params);
    }


    /**
     * Pega as professores de cada elemento
     */
    public function getProfessoresElement($all)
    {
        if ($all == null) return [];                

        foreach ($all as &$item) {
            
            $item->professores = $this->getProfessores($item->id);
        }

        return $all;
    }


    /**
     * Verifica os dados enviados pelo usuário para anexar uma professor ao grupo
     * @param  array $params 
     * @return mixed
     */
    protected function checkAttach($params)
    {
        if (empty($params)) {
           return ['attach' => false, 'code' => '1212', 'status' => '00'];
        }

        if (! isset($params['id_grupo'])) {
           return ['attach' => false, 'code' => '1206', 'status' => '00'];
        }

        if (ProfessoresGruposModel::find($params['id_grupo']) == null) {
           return ['attach' => false, 'code' => '1210', 'status' => '00'];
        } 

        if (! isset($params['professores']) ) {
           return ['attach' => false, 'code' => '1207', 'status' => '00'];
        }

        return true;
    }

    /**
     * Limpa todos as professores antes de salvar as professores novas
     * @return mixed
     */
    protected function removeAttachs($params)
    {
        try {
            
            $models = RelProfessoresGruposModel::where('id_grupo', $params['id_grupo']);
            
            $models->delete();

            return true;                        

        } catch (QueryException $e) {
            return ['attach' => false, 'code' => '1208', 'status' => $e->getMessage()];
        }
    }

    /**
     * Salva os dados de professor ao grupo
     * @param   $params
     * @return  mixed
     */
    protected function saveAttach($params)
    {
        foreach ($params['professores'] as $professor) {

            if (ProfessoresModel::find($professor) == null) {
               return ['attach' => false, 'code' => '1209', 'status' => '00'];
            }   
            
            $model = new RelProfessoresGruposModel();

            $model->id_grupo = $params['id_grupo'];
            $model->id_professor = $professor;

            try {

                $model->save();

            } catch (QueryException $e) {
                DB::rollback();
                return ['attach' => false, 'code' => '1211', 'status' => '00'];
            }
        }

        DB::commit();

        return ['attach' => true, 'code' => '1200', 'status' => '01'];
    }


    /**
     * Retorna as professores do grupo
     * @param  int $id 
     * @return array
     */
    public function getProfessores($id)
    {
        $collection = RelProfessoresGruposModel::where('id_grupo', $id)->get();

        $list = [];

        foreach ($collection as $item) {

            $professor = ProfessoresModel::where('id', $item->id_professor)->first();

            if ($professor == null) continue;

            $list[] = ['id' => $professor->id, 'nome' => $professor->nome];
        }

        return $list;
    } 
}