<?php 

namespace App\Modules\Professores\Models;

use App\Modules\Atribuicoes\Models\ProfessoresDasTurmas;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class ProfessoresModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Professores';

	// Connection Database
	protected $connection = 'urania-cliente';

    /**
     * Configuração do relacionamento com grupos
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function grupos(){

        return $this->belongsToMany(
            'App\Modules\Professores\Models\ProfessoresGruposModel',
            'Rel_Professores_Grupos',
            'id_professor',
            'id_grupo'
        );

    }

    public function disponibilidade()
    {
        return $this->hasMany(
            'App\Modules\Professores\Models\ProfessoresDisponibilidadeModel',
            'id_professor'
        );
    }

    /**
     * Filtro por Grupos
     * @param $query
     * @param null $grupo_id
     * @return mixed
     */
    public function scopeFiltrarGrupo($query, $grupo_id = null)
    {
        //Filtro do ID do grupo
        if(!empty($grupo_id)){
            return $query->whereHas('grupos', function($query) use ($grupo_id){
                $query->where('id', $grupo_id);
            });
        }

        return $query;
    }

    public function hasDisponibilidade()
    {
        $item = ProfessoresDisponibilidadeModel::where('id_professor', $this->id)->count();
        return $item >= 1;
    }

    /**
     * Filtra os professores por palavra chave
     * @param $query
     * @param null $texto
     * @return mixed
     */
    public function scopeFiltrarPalavraChave($query, $texto = null)
    {
        if(!empty($texto)){

            return $query->where(function($subQuery) use ($texto){

                foreach(explode(' ',$texto) as $word)
                {
                    $subQuery->orWhere('nome','like','%'.$word.'%');
                    $subQuery->orWhere('nome_completo','like','%'.$word.'%');
                }
            });

        }
        return $query;
    }

    /**
     * Filtro pela quantidade de aulas.
     * @param $query
     * @param null $quantidade
     * @return mixed
     */
    public function scopeFiltrarTotalAulas($query, $quantidade = null){

        if(!empty($quantidade)){

            return $query->where(function($subQuery) use ($quantidade){

                if($quantidade == '20+')
                {
                    $subQuery->where('total_aulas','>',20);
                }else{
                    $subQuery->where('total_aulas',$quantidade);
                }
            });

        }
        return $query;

    }

    /**
     * Filtro de professores, pelas disciplinas relacionadas a eles.
     * @param $query
     * @param null $texto
     */
    public function scopeFiltrarAtribuicao($query, $atribuicao = null)
    {
        if(!empty($atribuicao))
        {

            $arrayProfessores = [];
            $atribuicoes = ProfessoresDasTurmas::where('id_matriz_curricular', $atribuicao)
                ->get()
                ->lists('professores');

            foreach($atribuicoes as $professoresJson)
            {
                $arrayProfessores = array_merge($arrayProfessores, json_decode($professoresJson));
            }

            $arrayProfessores = array_unique($arrayProfessores);

            return $query->where(function($subQuery) use ($arrayProfessores){
                $subQuery->whereIn('id',$arrayProfessores);
            });
        }
        return $query;
    }

    public function scopeFiltrarSemLimitesDefinidos($query, $enabled = false)
    {
         if(boolval($enabled))
         {
             return $query->where(function($subquery){

                 $subquery->whereNull('max_aulas');
                 $subquery->orWhereNull('min_aulas');

             });
         }
    }

    public function scopeFiltrarSemDisponibilidade($query, $enabled = false)
    {
        if(boolval($enabled))
        {
            return $query->doesntHave('disponibilidade');
        }
    }

    /**
     * Método genérico para buscar
     * @param array $filter
     * @return array
     */
	public static function search(array $filter = [])
    {
        $arrayData = [];
        /** @var Builder $query */
        $query = self::select('*');

        //Apply filters
        $query->filtrarGrupo(array_get($filter, 'grupo_id'))
            ->filtrarTotalAulas(array_get($filter, 'total_aulas'))
            ->filtrarAtribuicao(array_get($filter, 'atribuicao'))
            ->filtrarPalavraChave(array_get($filter, 'keyword'))
            ->filtrarSemLimitesDefinidos(array_get($filter, 'sem_limites_definidos', false))
            ->filtrarSemDisponibilidade(array_get($filter, 'sem_disponibilidade', false));

        /** @var Collection $professores */
        $professores = $query->get();

        foreach($professores as $professor){

            $arrayData[] = array_merge($professor->toArray(), [
                'grupos' => $professor->grupos()->get()->toArray(),
                'disponibilidade' => $professor->hasDisponibilidade()
            ]);
        }

        return $arrayData;
    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }
}