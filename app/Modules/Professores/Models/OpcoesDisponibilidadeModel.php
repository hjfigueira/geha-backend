<?php 

namespace App\Modules\Professores\Models;

use Illuminate\Database\Eloquent\Model;

class OpcoesDisponibilidadeModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Opcoes_Disponibilidade';

	// Connection Database
	protected $connection = 'urania-admin';
}