<?php 

namespace App\Modules\Professores\Models;

use Illuminate\Database\Eloquent\Model;

class RelProfessoresGruposModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Rel_Professores_Grupos';

	// Connection Database
	protected $connection = 'urania-cliente';

}