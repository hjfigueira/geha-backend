<?php 

namespace App\Modules\Professores\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessoresDisponibilidadeModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Professores_Disponibilidade_Por_Aula';

	// Connection Database
	protected $connection = 'urania-cliente';

	protected $fillable = ['horario', 
	                       'id_opcao_disponibilidade', 
	                       'id_professor', 
	                       'id_turno', 
	                       'id_dia'];

}