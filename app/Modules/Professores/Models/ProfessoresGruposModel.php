<?php 

namespace App\Modules\Professores\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessoresGruposModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	public $table = 'Professores_Grupos';

	// Connection Database
	protected $connection = 'urania-cliente';

    /**
     * Configura o relacionamento com os professores
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function professores(){

        return $this->belongsToMany(
            'App\Modules\Turmas\Models\Professores',
            'Rel_Professores_Grupos',
            'id_grupo',
            'id_professor'
        );

    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }
}