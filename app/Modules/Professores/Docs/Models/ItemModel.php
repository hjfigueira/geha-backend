<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="ItemModel")
 * )
 */
class ItemModel
{
    /**
     * @var MatrizCurricularModel[]
     * @SWG\Property(@SWG\Xml(name="professor",wrapped=true))
     */
    public $itens;

}