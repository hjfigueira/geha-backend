<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="GrupoProfessorModel")
 * )
 */
class GrupoProfessorModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;


    /**
     * @SWG\Property(example="Grupo")
     * @var string
     */
    public $nome;
}