<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="ProfessorModel")
 * )
 */
class ProfessorModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;


    /**
     * @SWG\Property(example="João")
     * @var string
     */
    public $nome;

    /**
     * @SWG\Property(example="João Silva")
     * @var string
     */
    public $nome_completo;

    /**
     * @SWG\Property(example="jsa")
     * @var string
     */
    public $abreviatura;

    /**
     * @SWG\Property(example="joao@teste.com")
     * @var string
     */
    public $email;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $total_aulas;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $min_aulas;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $max_aulas;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $pref_janelas;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $dias_a_vir;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $max_consecutivas;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $max_deslocamentos_semana;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $pode_voltar_sede_dia;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $pref_mudar_sede;

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $tipo_deslocamento_sedes;

    /**
     * @SWG\Property(example="#000")
     * @var string
     */
    public $cor;

}

