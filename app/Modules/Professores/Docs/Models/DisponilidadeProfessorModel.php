<?php 
/**
 * @SWG\Definition(
 *     required={"id_professor", "id_turno", "id_dia", "horario",
 *     "id_opcao_disponibilidade"}, 
 *     type="object", 
 *     @SWG\Xml(name="DisponilidadeProfessorModel")
 * )
 */
class DisponilidadeProfessorModel
{
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_professor;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_turno;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_dia;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $horario;
    
    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id_opcao_disponibilidade;
    
    /**
     * @SWG\Property(example="Algum comentário")
     * @var string
     */
    public $texto_adicional;
}