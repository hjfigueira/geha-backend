<?php

     
/**
 *
 * @SWG\Get(
 *     path="/professores-grupos/all",
 *     summary="Retorna todos os registros de grupos de  salvos no sistema.",
 *     description="Retorna todos os registros de grupos de  salvos no sistema",
 *     operationId="api.professores.index",
 *     produces={"application/json"},
 *     tags={"professores.grupos"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Get(
 *     path="/professores-grupos/get/{grupoID}",
 *     summary="Retorna um grupo específico, passando um ID parâmetro.",
 *     description="Retorna um grupo específico, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"professores.grupos"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID da disciplina",
 *         in="path",
 *         name="grupoID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 *
 * @SWG\Get(
 *     path="/professores-grupos/get/{pesquisa}",
 *     summary="Retorna um grupo específico, passando texto como parâmetro.",
 *     description="Retorna um grupo específico, passando texto como parâmetro.",
 *     produces={"application/json"},
 *     tags={"professores.grupos"},
 *     @SWG\Parameter(
 *         description="Nome da disciplina",
 *         in="path",
 *         name="pesquisa",
 *         required=true,
 *         type="string",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/professores-grupos/save",
 *     operationId="save",
 *     consumes={"application/json"},
 *     summary="Salva um grupo de professores",
 *     description="Cria ou atualiza um grupo de professores no sistema. Para atualizar basta passar o id do objeto. Possíveis status:
 *     [code=1200] = Efetuado com sucesso.
 *     [code=1201] = Campo 'nome' é obrigatório.
 *     [code=1202] = Campo 'nome' é um valor inválido.
 *     [code=1205] = Item com o ID que foi informado não foi encontrado.
 *     [code=1206] = Atributo enviado é inválido.
 *     [code=1207] = Já possui um item com este nome.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"professores.grupos"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/GrupoProfessorModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/professores-grupos/delete",
 *     summary="Remove um grupo do banco de dados.",
 *     description="Remove um grupo do banco de dados. Possíveis status:
 *     [code=1200] = Efetuado com sucesso.
 *     [code=1203] = Campo 'id' é obrigatório.
 *     [code=1204] = Item não foi encontrado ou já foi removido.
 *     [code=1213] = Grupo não pode ser excluido pois possui professores relacionados.",
 *     produces={"application/json"},
 *     tags={"professores.grupos"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="Objeto JSON com IDS para exclusão",
 *         @SWG\Schema(ref="#/definitions/GrupoProfessorModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * Anexa uma professor à um grupo
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @SWG\Post(
 *     path="/professores-grupos/attach",
 *     operationId="save",
 *     consumes={"application/json", "application/xml"},
 *     summary="Anexa uma professor à um grupo",
 *     description="Anexa professores à um grupo no sistema. Para limpar todas as professores basta passar professores como []. Possíveis status:
 *     [code=1200] = Efetuado com sucesso.
 *     [code=1206] = Campo 'id_grupo' é obrigatório.
 *     [code=1207] = Campo 'professores' é obrigatório.
 *     [code=1208] = Erro ao desvincular professor. Verifique se ela não está sendo usada.
 *     [code=1209] = Professor não encontrada.
 *     [code=1210] = Grupo não encontrado.
 *     [code=1211] = Erro ao salvar dados no banco de dados.
 *     [code=1212] = Parâmetro nulo ou objeto JSON mal formatado.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"professores.grupos"},
 *     @SWG\Parameter(
 *         name="professor",
 *         in="body",
 *         description="Objeto JSON com as propriedades da professor",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/RelProfessoresGrupoModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */