<?php

     
/**
 *
 * @SWG\Get(
 *     path="/professores/all",
 *     summary="Retorna todos os registros de professores salvos no sistema.",
 *     description="Retorna todos os registros de professores salvos no sistema",
 *     operationId="api.professores.index",
 *     produces={"application/json"},
 *     tags={"professores.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Get(
 *     path="/professores/get/{professorID}",
 *     summary="Retorna um professor específico, passando um ID parâmetro.",
 *     description="Retorna um professor específico, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"professores.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID da disciplina",
 *         in="path",
 *         name="professorID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 *
 * @SWG\Get(
 *     path="/professores/get/{pesquisa}",
 *     summary="Retorna um professor específico, passando texto como parâmetro.",
 *     description="Retorna um professor específico, passando texto como parâmetro.",
 *     produces={"application/json"},
 *     tags={"professores.basico"},
 *     @SWG\Parameter(
 *         description="Nome do professor",
 *         in="path",
 *         name="pesquisa",
 *         required=true,
 *         type="string",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/professores/save",
 *     operationId="save",
 *     consumes={"application/json"},
 *     summary="Salva um professor de disciplina",
 *     description="Cria ou atualiza um professor de disciplina no sistema. Possíveis status:
 *     [code=1100] = Efetuado com sucesso.
 *     [code=1101] = Campo 'nome' é obrigatório.
 *     [code=1102] = Campo 'nome' é um valor inválido.
 *     [code=1105] = Item com o ID que foi informado não foi encontrado.
 *     [code=1106] = Atributo enviado é inválido.
 *     [code=1107] = Já possui um item com este nome.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"professores.basico"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/ProfessorModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/professores/delete",
 *     summary="Remove um professor do banco de dados.",
 *     description="Remove um professor do banco de dados. Possíveis status:
 *     [code=1100] = Efetuado com sucesso.
 *     [code=1103] = Campo 'id' é obrigatório.
 *     [code=1104] = Item não foi encontrado ou já foi removido.
 *     [code=1108] = Item não pode ser removido pois está em um grupo.",
 *     produces={"application/json"},
 *     tags={"professores.basico"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="Objeto JSON com 'ids' e remover",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/ProfessorModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */