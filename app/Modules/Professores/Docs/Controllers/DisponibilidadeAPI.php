<?php

     
/**
 *
 * @SWG\Get(
 *     path="/professores-disponibilidade/all",
 *     summary="Retorna todos os registros de grupos de  salvos no sistema.",
 *     description="Retorna todos os registros de grupos de  salvos no sistema",
 *     operationId="api.professores.index",
 *     produces={"application/json"},
 *     tags={"professores.disponibilidade"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Get(
 *     path="/professores-disponibilidade/get/{grupoID}",
 *     summary="Retorna um grupo específico, passando um ID parâmetro.",
 *     description="Retorna um grupo específico, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"professores.disponibilidade"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID da disciplina",
 *         in="path",
 *         name="grupoID",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 * @SWG\Get(
 *     path="/professores-disponibilidade/get-by-professor/{id}",
 *     summary="Retorna as disponibilidades de um professor específico",
 *     description="Retorna as disponibilidades de um professor específico, passando um ID parâmetro.",
 *     produces={"application/json"},
 *     tags={"professores.disponibilidade"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID do professor",
 *         in="path",
 *         name="id",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */

/**
 * @SWG\Get(
 *     path="/professores-disponibilidade/options",
 *     summary="Retorna as opções de disponibilidades",
 *     description="Retorna as opções de disponibilidades.",
 *     produces={"application/json"},
 *     tags={"professores.disponibilidade"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */



/**
 * @SWG\Post(
 *     path="/professores-disponibilidade/save",
 *     operationId="save",
 *     consumes={"application/json"},
 *     summary="Salva um grupo de professores",
 *     description="Cria ou atualiza um grupo de professores no sistema. Para atualizar basta passar o id do objeto. Possíveis status:
 *     [code=1300] = Efetuado com sucesso.
 *	   [code=1301] Parâmetro 'id_professor' não encontrado.
 *	   [code=1302] Parâmetro 'id_turno' não encontrado.
 *	   [code=1303] Parâmetro 'id_dia' não encontrado.
 *	   [code=1304] Parâmetro 'horario' não encontrado.
 *	   [code=1305] Parâmetro 'id_opcao_disponibilidade' não encontrado.
 *	   [code=1306] Disponibilidade não encontrada com este Id.
 *	   [code=1307] Professor não encontrado com este 'id_professor' .
 *	   [code=1308] Turno não encontrado com este 'id_turno'.
 *	   [code=1309] Dia não encontrado com este 'id_dia'.
 *	   [code=1310] Falha ao inserir no banco de dados.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"professores.disponibilidade"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/ItemModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/professores-disponibilidade/change",
 *     operationId="change",
 *     consumes={"application/json"},
 *     summary="Muda as disponibilidades de um ou mais professores",
 *     description="Muda as disponibilidades de um ou mais professores:
 *     [code=1300] = Efetuado com sucesso.
 *	   [code=1301] Parâmetro 'id_professor' não encontrado.
 *	   [code=1302] Parâmetro 'id_turno' não encontrado.
 *	   [code=1303] Parâmetro 'id_dia' não encontrado.
 *	   [code=1304] Parâmetro 'horario' não encontrado.
 *	   [code=1305] Parâmetro 'id_opcao_disponibilidade' não encontrado.
 *	   [code=1306] Disponibilidade não encontrada com este Id.
 *	   [code=1307] Professor não encontrado com este 'id_professor' .
 *	   [code=1308] Turno não encontrado com este 'id_turno'.
 *	   [code=1309] Dia não encontrado com este 'id_dia'.
 *	   [code=1310] Falha ao inserir no banco de dados.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"professores.disponibilidade"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="Objeto JSON com as propriedades da disciplina",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/ItemModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/professores-disponibilidade/delete",
 *     summary="Remove um grupo do banco de dados.",
 *     description="Remove um grupo do banco de dados. Possíveis status:
 *     [code=1300] = Efetuado com sucesso.
 *     [code=1311] = Campo 'id' é obrigatório.
 *     [code=1312] = Item não foi encontrado ou já foi removido.",
 *     produces={"application/json"},
 *     tags={"professores.disponibilidade"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="Objeto JSON com 'ids' para exclusão",
 *         @SWG\Schema(ref="#/definitions/ItemModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */