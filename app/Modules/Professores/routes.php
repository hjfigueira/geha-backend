<?php
/*
|--------------------------------------------------------------------------
| Professores Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Professores module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group([
    'prefix' => 'api/professores',
    'namespace' => 'App\Modules\Professores\Controllers',
    'middleware' => 'auth'],
    function () {
        Route::get('all',     	   ['as' => 'api/professores.all',     'uses' => 'ApiControllerProfessores@all']);
        Route::get('get/{id}',     ['as' => 'api/professores.get',     'uses' => 'ApiControllerProfessores@get'])->where('id', '[0-9]+');
        Route::get('get/{name}',   ['as' => 'api/professores.search',  'uses' => 'ApiControllerProfessores@search'])->where('name', '[A-Za-z ]+');
        Route::post('save',        ['as' => 'api/professores.save',    'uses' => 'ApiControllerProfessores@save']);
        Route::post('delete', ['as' => 'api/professores.destroy', 'uses' => 'ApiControllerProfessores@destroy']);

});


Route::group([
    'prefix' => 'api/professores-grupos',
    'namespace' => 'App\Modules\Professores\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',     	   ['as' => 'api/professores-grupos.all',     'uses' => 'ApiControllerProfessoresGrupos@all']);	
	Route::get('get/{id}',     ['as' => 'api/professores-grupos.get',     'uses' => 'ApiControllerProfessoresGrupos@get'])->where('id', '[0-9]+');	
	Route::get('get/{name}',   ['as' => 'api/professores-grupos.search',  'uses' => 'ApiControllerProfessoresGrupos@search'])->where('name', '[A-Za-z ]+');	
	Route::post('save',        ['as' => 'api/professores-grupos.save',    'uses' => 'ApiControllerProfessoresGrupos@save']);	
	Route::post('delete', ['as' => 'api/professores-grupos.destroy', 'uses' => 'ApiControllerProfessoresGrupos@destroy']);
	Route::post('attach', 	   ['as' => 'api/professores-grupos.attach', 'uses' => 'ApiControllerProfessoresGrupos@attach']);	
});


Route::group([
    'prefix' => 'api/professores-disponibilidade',
    'namespace' => 'App\Modules\Professores\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',     	   ['as' => 'api/professores-disponiblidade.all',     'uses' => 'ApiControllerProfessoresDisponibilidade@all']);	
    Route::get('options',    ['as' => 'api/professores-disponiblidade.options',     'uses' => 'ApiControllerProfessoresDisponibilidade@options']);        
	Route::get('get/{id}',     ['as' => 'api/professores-disponiblidade.get',     'uses' => 'ApiControllerProfessoresDisponibilidade@get'])->where('id', '[0-9]+');	
	Route::get('get-by-professor/{id}',     ['as' => 'api/professores-disponiblidade.get',     'uses' => 'ApiControllerProfessoresDisponibilidade@getByProfessorId'])->where('id', '[0-9]+');
	Route::post('save',        ['as' => 'api/professores-disponiblidade.save',    'uses' => 'ApiControllerProfessoresDisponibilidade@save']);
    
    Route::post('change', ['as' => 'api/professores-disponiblidade.change', 'uses' => 'ApiControllerProfessoresDisponibilidade@change']);


	Route::post('delete', ['as' => 'api/professores-disponiblidade.destroy', 'uses' => 'ApiControllerProfessoresDisponibilidade@destroy']);
    Route::post('edit', ['as' => 'api/professores-disponiblidade.edit', 'uses' => 'ApiControllerProfessoresDisponibilidade@edit']);

});