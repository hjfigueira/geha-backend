<?php

namespace App\Modules\Professores\Controllers;

use DB;
use Schema;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Turnos\Models\TurnosModel;
use App\Modules\Professores\Models\ProfessoresModel;
use App\Modules\PerguntasBasicas\Models\DiasAulaModel;
use Illuminate\Database\QueryException as QueryException;
use App\Modules\Professores\Models\OpcoesDisponibilidadeModel;
use App\Modules\Professores\Models\ProfessoresDisponibilidadeModel;
use App\Modules\PerguntasBasicas\Controllers\ApiController as PerguntasBasicas;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiControllerProfessoresDisponibilidade extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new ProfessoresDisponibilidadeModel();
    }

    /**
     * Retorna todos os nomes de disciplinas salvos no sistema
     * @return array
     * Então, confirmando. O que tu precisa é que no professores-disponibilidade/all
     * retorne um campo com o numero de atividades do maior esquema, para cada turno.
     */
    public function all() 
    {
        $returnData = [];
        $professoresDisponibilidade = $this->model->all();

        $returnData['data']       = $professoresDisponibilidade;
        $returnData['atividades'] = $this->getQtdAtidades();

        return $returnData;
    }

    /**
     * Insert a new Api
     * @return array
     */
    public function save(Request $request)
    {
        $params = $request->input('itens');

        DB::beginTransaction();

        foreach ($params as $key => $param) {

            $ret = $this->saveItem($param);

            if ( ! empty($ret) ) {
                DB::rollback();
                return $ret;
            }
        }

        DB::commit();
        return ['created' => true, 'code' => '1300', 'status' => '01'];
    }

    /**
     * Retorna as disponibilidades pelo ID do professor
     * @param  int $id 
     * @return array     
     */
    public function getByProfessorId($id)
    {
        $data['data'] = ProfessoresDisponibilidadeModel::where('id_professor',$id)->get();
        $data['atividades'] = $this->getQtdAtidades();

        return $data;

    }

    /**
     * Retorna uma turma específico
     * @return array
     */
    public function get($id)
    {
        $data = $this->model->find($id);

        if ($data == null) return [];

        return $data;
    }

    /**
     * Retorna uma turma específico
     * @return array
     */
    public function search($expression)
    {
        $search = explode(' ', $expression);

        foreach ($search as $key => $name) {

            $name = '%' . $name . '%';;

            $data = $this->model->where('nome', 'like', $name)->get();
        
            if ( ! $data->isEmpty()) {
                return $data;
            } 
        }

        return null;
    }

    /**
     * Retorna a lista de opções de disponibilidades
     * do banco de dados do administrador
     * @return 
     */
    public function options()
    {
        return OpcoesDisponibilidadeModel::all();
    }

    /**
     * Remove um nome do banco de dados
     * @return array
     */
    public function destroy(Request $request)
    {
        $status = [];
        $arrayIds = $request->get('ids',[]);

        foreach($arrayIds as $id)
        {
            if (!isset($id) || $id == null) {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1311',
                    'status' => '00',
                    'data' => [
                        'nome' => null
                    ]];
                continue;
            }

            try {
            
                $item = $this->model->findOrFail($id);
            
            } catch(Exception $error) {
                
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1312',
                    'status' => '00',
                    'data' => [
                        'nome' => null
                    ]];

                continue;
            }

            if ($item == null || ! $item->delete($id)) {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1312',
                    'status' => '00',
                    'data' => [
                        'nome' => null
                    ]];
                continue;
            }

            $status[] = [
                'id' => $id,
                'deleted' => true,
                'code' => '1300',
                'status' => '01',
                'data' => [
                    'nome' => null
                ]];
        }

        return $status;

    }

    public function edit(Request $request)
    {
        $params = $request->input();
        if(isset($params['id_professor'])) {

            foreach ($params['id_professor'] as $id_professor)
            {
                if ($params['opcao'] == 1) {
                    $this->model = new ProfessoresDisponibilidadeModel;
                    $this->model
                        ->where('id_opcao_disponibilidade', 3)
                        ->where('id_professor',$id_professor)
                        ->update(['id_opcao_disponibilidade' => 1]);
                }
                else if($params['opcao']==2){
                    $this->model = new ProfessoresDisponibilidadeModel;
                    $this->model
                        ->where('id_opcao_disponibilidade', 4)
                        ->where('id_professor',$id_professor)
                        ->update(['id_opcao_disponibilidade' => 1]);
                }
                else if($params['opcao']==3){
                    $this->model = new ProfessoresDisponibilidadeModel;
                    $this->model
                        ->where('id_professor',$id_professor)
                        ->update(['id_opcao_disponibilidade' => 1]);
                }
            }

        }
    }

    /**
     * Método para salvar um item no banco de dados
     * @param $param
     * @return array|null
     */
    protected function saveItem($param)
    {
        if (! isset($param['id_professor'])) {
            return ['created' => false, 'code' => '1301', 'status' => '00'];
        }

        elseif (! isset($param['id_turno'])) {
            return ['created' => false, 'code' => '1302', 'status' => '00'];
        }

        elseif (! isset($param['id_dia'])) {
            return ['created' => false, 'code' => '1303', 'status' => '00'];
        }

        elseif (! isset($param['horario'])) {
            return ['created' => false, 'code' => '1304', 'status' => '00'];
        }        

        elseif (! isset($param['id_opcao_disponibilidade'])) {
            return ['created' => false, 'code' => '1305', 'status' => '00'];
        }        

        elseif ( ProfessoresModel::find($param['id_professor']) == null) {
            return ['created' => false, 'code' => '1307', 'status' => '00'];
        } 

        elseif ( TurnosModel::find($param['id_turno']) == null) {
            return ['created' => false, 'code' => '1308', 'status' => '00'];
        }

        elseif ( DiasAulaModel::where('id_dia', $param['id_dia'])->get() == null) {
            return ['created' => false, 'code' => '1309', 'status' => '00'];
        }

        if (isset($param['id'])) {
            $model = ProfessoresDisponibilidadeModel::find($param['id']);
            if ($model == null) {
                return ['created' => false, 'code' => '1306', 'status' => '00'];
            }
        }
        else{
            $model = new ProfessoresDisponibilidadeModel();
        }

        $model->id_professor             = $param['id_professor'];
        $model->id_turno                 = $param['id_turno'];
        $model->id_dia                   = $param['id_dia'];
        $model->horario                  = $param['horario'];
        $model->id_opcao_disponibilidade = $param['id_opcao_disponibilidade'];
        $model->texto_adicional          = @$param['texto_adicional'];

        try {
            
            $model->save();
            return null;
            
        } catch (QueryException $e) {
            return ['created' => false, 'code' => '1310', 'status' => '00', 'debug' => $e->getMessage()];
        }
    }

    /**
     * Função para atribuir as disponibilidades dos professores 
     * para um determinado tipo de forma massiva
     * @param  Request $request 
     */
    public function change(Request $request)
    {
        $params          = $request->input();
        $opcao           = $params['disponibilidade_de'];
        $opcNova         = $params['disponibilidade_para'];
        $disponibilidade = $this->all();        
        $atividades      = $disponibilidade['atividades'];
            
        DB::beginTransaction();

        foreach ($params['professores'] as $idProfessor) {

            foreach ($atividades as $turno => $max) {

                $this->changeDispProf($turno, $idProfessor, $opcao, $opcNova, $max);           
            }
        }

        DB::commit();

        return ['changed' => true, 'code' => '1300', 'status' => '01'];
    }

    /**
     * Função auxiliar para mudar a disponiblidade do professor, de acordo com o turno e dias ativos,  
     * @param  int $turno       
     * @param  int $idProfessor 
     * @param  int $opcao       
     * @param  int $opcNova     
     * @return boolean
     * @todo  Verificar a possibilidade unificar a função createOrUpdateAll oe createOrUpdate
     */
    protected function changeDispProf($turno, $idProfessor, $opcao, $opcNova, $max)
    {
        $perguntas  = new PerguntasBasicas();        
        $diasAtivos = $perguntas->get();

        foreach ($diasAtivos['dias'] as $semanas => $dias) {

            foreach ($dias as $dia) {

                if ($dia['check'] == 0) {
                    continue;
                }
                
                $idDia = $dia['id'];

                for ($i=0; $i < $max; $i++) { 

                    if ($opcao == 0) {

                        $this->createOrUpdateAll($idProfessor, $turno, $idDia, $opcNova, $max);

                    } else {

                        $this->createOrUpdate($idProfessor, $turno, $idDia, $opcao, $opcNova);
                    }

                }
            }
        }
    }


    /**
     * Função auxiliar para gravar uma nova disponibilidade de um professor
     * Ou para alterar o status atual da disponibilidade
     * @param  int $idProf 
     * @param  int $idTurno     
     * @param  int $id          
     * @param  int $idOpcao     
     * @todo   Verificar a regra de negócio para eliminar essa função
     * @return bool
     */
    protected function createOrUpdate($idProf, $idTurno, $idDia, $opcAnt, $opcNova)
    {
        $model = new ProfessoresDisponibilidadeModel();

        $celula = $model->where('id_professor', $idProf)
                        ->where('id_turno', $idTurno)
                        ->where('id_dia', $idDia)
                        ->where('id_opcao_disponibilidade', $opcAnt)
                        ->first();

        if ($celula == null) {
        
            $novo = ['id_professor'             => $idProf,
                     'id_turno'                 => $idTurno,
                     'id_dia'                   => $idDia,
                     'horario'                  => '',
                     'id_opcao_disponibilidade' => $opcNova];

            $this->saveItem($novo);

        } else {
            

            $celula->id_opcao_disponibilidade = $opcNova;
            $celula->texto_adicional          = '';

            $celula->save();
        }
    }

    /**
     * Função auxiliar para criar ou atualizar todas as disponibilidades
     * independente do id da disponibilidade atual
     * @param  int $idProf  
     * @param  int $idTurno 
     * @param  int $idDia   
     * @param  int $opcNova 
     * @todo   Verificar a regra de negócio para eliminar essa função
     * @return bool        
     */
    protected function createOrUpdateAll($idProf, $idTurno, $idDia, $opcao, $max)
    {
        $model = new ProfessoresDisponibilidadeModel();

        $collection = $model->where('id_professor', $idProf)
                            ->where('id_turno', $idTurno)
                            ->where('id_dia', $idDia)
                            ->get();


        if (! $collection->isEmpty()) {

            foreach ($collection as $item) {

                $item->id_opcao_disponibilidade = $opcao;
                $item->texto_adicional          = '';
                $item->save();
            }

            return true;
        }

        for ($i=0; $i < $max; $i++) { 
            
            $novo = ['id_professor'             => $idProf,
                    'id_turno'                 => $idTurno,
                    'id_dia'                   => $idDia,
                    'horario'                  => '',
                    'id_opcao_disponibilidade' => $opcao];

            $this->saveItem($novo);
        }

        return true;
    }

    /**
     * Função auxiliar que retorna a quantidade 
     * máxima de atividade separadas por turno
     * @return array
     */
    protected function getQtdAtidades()
    {
        $atividades = [];

        foreach(TurnosModel::all() as $turno){
            /** @var $turno TurnosModel */
            $atividades[$turno->id] = $turno->getEsquemaMaisAtividades();
        }

        return $atividades;
    }
}
