<?php

namespace App\Modules\Professores\Controllers;

use Schema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Professores\Models\ProfessoresModel;
use Illuminate\Database\QueryException as QueryException;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiControllerProfessores extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new ProfessoresModel();
    }

    /**
     * Retorna todos os nomes de disciplinas salvos no sistema
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(Request $request)
    {
        $filters = $request->get('filtros',[]);

        $itens = ProfessoresModel::search($filters);

        return $itens;
    }

    /**
     * Insert a new Api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->input();

        if (! isset($params['nome'])) {           
            return ['created' => false, 'code' => '1101', 'status' => '00'];
        }

        if( strlen($params['nome']) < 1 || strlen($params['nome']) > 60) {
            return ['created' => false, 'code' => '1102', 'status' => '00'];
        }

        if (isset($params['id'])) {
            $this->model = $this->model->find($params['id']);


            if ($this->model == null) {
                return ['created' => false, 'code' => '1105', 'status' => '00'];
            }
        } 

        return $this->saveData($params);
    }  

    /**
     * Retorna uma turma específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $data = $this->model->find($id);

        if ($data == null) return [];

        return $data;
    }

    /**
     * Retorna uma turma específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($expression)
    {
        $search = explode(' ', $expression);

        foreach ($search as $key => $name) {

            $name = '%' . $name . '%';;

            $data = $this->model->where('nome', 'like', $name)->get();
        
            if ( ! $data->isEmpty()) {
                return $data;
            } 
        }

        return null;
    }


    /**
     * Remove um nome do banco de dados
     * @return array
     */
    public function destroy(Request $request)
    {
        $status = [];
        $arrayIds = $request->get('ids', []);

        foreach($arrayIds as $id)
        {
            if (!isset($id) || $id == null) {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1103',
                    'status' => '00',
                    'data' => [
                        'nome' => $this->model->nome
                    ]];
                continue;
            }

            try{
                $item = $this->model->findOrFail($id);

                if($item->grupos()->count() > 0)
                {
                    $status[] = [
                        'id' => $id,
                        'deleted' => false,
                        'code' => '1108',
                        'status' => '00',
                        'data' => [
                            'nome' => $item->nome
                        ]];
                    continue;
                }

                $item->disponibilidade()->delete();

                if ($item == null || ! $item->delete($id)) {
                    $status[] = [
                        'id' => $id,
                        'deleted' => false,
                        'code' => '1104',
                        'status' => '00',
                        'data' => [
                            'nome' => $item->nome
                        ]];
                    continue;
                }

                $status[] = [
                    'id' => $id,
                    'deleted' => true,
                    'code' => '1100',
                    'status' => '01',
                    'data' => [
                        'nome' => $item->nome
                    ]];
                continue;

            }catch(\Exception $error)
            {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1104',
                    'status' => '00',
                    'data' => [
                        'nome' => ''
                    ]];
                continue;
            }
        }

        return $status;
    }

    /**
     * Trata e salva os dados no banco de dados
     * @param  array $params 
     * @return array
     */
    protected function saveData($params)
    {
        foreach ($params as $key => $value) {

            if (Schema::hasColumn($this->model->table, $key) == false) {
                return ['created' => false, 'code' => '1106', 'status' => '00'];
            }
        
            $this->model->$key = $value;
        }

        try {
            
            $this->model->save();
            
        } catch (QueryException $e) {
            return ['created' => false, 'code' => '1107', 'status' => '00'];
        }

        return ['created' => true, 'code' => '1100', 'status' => '01'];
    }    
}
