<?php

namespace App\Modules\Professores\Controllers;

use App\Modules\Professores\Models\ProfessoresModel;
use App\Modules\Professores\Models\RelProfessoresGruposModel;
use Schema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Professores\Classes\ProfessorAttach;
use Illuminate\Database\QueryException as QueryException;
use App\Modules\Professores\Models\ProfessoresGruposModel;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiControllerProfessoresGrupos extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new ProfessoresGruposModel();
    }

    /**
     * Retorna todos os nomes de disciplinas salvos no sistema
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all() 
    {
        $all = $this->model->all();
        $attach = new ProfessorAttach();
        $lista = $attach->getProfessoresElement($all);
        return $lista->toArray();
    }

    /**
     * Insert a new Api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->input();

        if (! isset($params['nome'])) {           
            return ['created' => false, 'code' => '1201', 'status' => '00'];
        }

        if( strlen($params['nome']) < 1 || strlen($params['nome']) > 60) {
            return ['created' => false, 'code' => '1202', 'status' => '00'];
        }

        if(isset($params['nome']))
        {
            $nomeExiste = $this->model->where('nome', $params['nome'])->exists();
            if($nomeExiste)
            {
                return ['created' => false, 'code' => '1216', 'status' => '00'];
            }
        }

        if (isset($params['id']))
        {
            $this->model = $this->model->find($params['id']);
            if ($this->model == null) {
                return ['created' => false, 'code' => '1205', 'status' => '00'];
            }
        }else {

            $this->model = new ProfessoresGruposModel();
            $this->model->nome = $params['nome'];

            $this->model->save();


            $grupo_id=$this->model->id;
            if(isset($param['itens'])) {
                foreach ($params['itens'] as $key => $professor_id) {
                    $this->model = new ProfessoresModel();
                    $idExiste = $this->model->where('id', $professor_id)->exists();
                    if ($idExiste == false) {
                        return ['created' => false, 'code' => '1217', 'status' => '00'];
                    }
                }

                foreach ($params['itens'] as $key => $prefessor_id) {
                    $this->model = new RelProfessoresGruposModel();
                    $this->model->id_professor = $prefessor_id;
                    $this->model->id_grupo = $grupo_id;
                    $this->model->save();

                }
            }
            return ['created' => true, 'code' => '1200', 'status' => '01', 'id' => $this->model->id];
        }

        //return $this->saveData($params);
    }  

    /**
     * Retorna uma professor específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $data = $this->model->find($id);

        $attach = new ProfessorAttach();

        $data->turmas = $attach->getProfessores($data->id);

        return $data;
    }

    /**
     * Retorna uma professor específico
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($expression)
    {
        $search = explode(' ', $expression);

        $attach = new ProfessoresAttach();

        foreach ($search as $key => $name) {

            $name = '%' . $name . '%';;

            $data = $this->model->where('nome', 'like', $name)->get();
        
            return $attach->getProfessoressElement($all);
        }

        return [];
    }


    /**
     * Remove um nome do banco de dados
     * @return array
     */
    public function destroy(Request $request)
    {
        $status = [];
        $arrayIds = $request->get('ids',[]);

        foreach($arrayIds as $id)
        {
            if (!isset($id) || $id == null) {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1203',
                    'status' => '00',
                    'data' => [
                        'nome' => null
                    ]];
                continue;
            }

            try{
                $item = $this->model->findOrFail($id);
            }catch(\Exception $error)
            {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1204',
                    'status' => '00',
                    'data' => [
                        'nome' => null
                    ]];
                continue;
            }

            $hasProfessores = RelProfessoresGruposModel::where('id_grupo', $item->id)->exists();
            if($hasProfessores)
            {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1213',
                    'status' => '00',
                    'data' => [
                        'nome' => $item->nome
                    ]];
                continue;
            }

            if ($item == null || ! $item->delete($id)) {
                $status[] = [
                    'id' => $id,
                    'deleted' => false,
                    'code' => '1204',
                    'status' => '00',
                    'data' => [
                        'nome' => $item->nome
                    ]];
                continue;
            }

            $status[] = [
                'id' => $id,
                'deleted' => true,
                'code' => '1200',
                'status' => '01',
                'data' => [
                    'nome' => $item->nome
                ]];
            continue;
        }

        return $status;
    }


    /**
     * Anexa/desanexa professors ao grupo
     */
    public function attach(Request $request)
    {
        $professor = new ProfessorAttach();

        return $professor->attach($request);
    }


    /**
     * Trata e salva os dados no banco de dados
     * @param  array $params 
     * @return array
     */
    protected function saveData($params)
    {
        foreach ($params as $key => $value) {

            if (Schema::hasColumn($this->model->table, $key) == false) {
                return ['created' => false, 'code' => '1206', 'status' => '00'];
            }
        
            $this->model->$key = $value;
        }

        try {
            
            $this->model->save();


        } catch (QueryException $e) {
            return ['created' => false, 'code' => '1207', 'status' => '00'];
        }

        return ['created' => true, 'code' => '1200', 'status' => '01', 'id' => $this->model->id];
    }   
}
