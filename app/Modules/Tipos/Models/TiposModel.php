<?php

namespace App\Modules\Tipos\Models;

use App\Models\WsClientModel;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;


class TiposModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Tipos';

    protected $connection = 'urania-admin';

}