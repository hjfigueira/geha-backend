<?php 

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordTokenModel extends Model
{
	public $timestamps = false;

	protected $table = 'password_resets';

	protected $connection = 'urania-admin';
}