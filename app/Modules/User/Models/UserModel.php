<?php 

namespace App\Modules\User\Models;

use App\Models\WsClientModel;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class UserModel extends WsClientModel
{
	public $timestamps = false;

	protected $table = 'Usuarios';

	protected $connection = 'urania-admin';

	protected $fillable = ['api_token','api_token_timestamp','api_last_call'];

    public function generateToken()
    {
        $timestamp = time();
        $token = sha1(($timestamp).'-'.$this->login.rand(0,999));
        $this->update([
            'api_token' => $token,
            'api_token_timestamp' => $timestamp,
            'api_last_call' => $timestamp,
        ]);
    }

    public function updateCalltime()
    {
        $this->update([ 'api_last_call' => time()]) ;
    }

    public static function isTokenValid($token)
    {
        try{
            $model = self::where('api_token', $token)
                ->firstOrFail();

            return $model;
        }
        catch(ModelNotFoundException $error)
        {
            return null;
        }
    }

    public static function doAuth($user, $pass)
    {
        try{
            $model = self::where('senha', md5($pass))
                ->where('login', $user)
                ->firstOrFail();

            $model->generateToken();
            $model->updateCalltime();

            return $model;
        }
        catch(ModelNotFoundException $error)
        {
            return null;
        }
    }

}