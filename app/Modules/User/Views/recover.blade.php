<html>
<head></head>
<body style="background: black; color: white">
<h1>{{$title}}</h1>
<p>Você solicitou um e-mail para troca de senha. <br/> Para alterar a senha clique no link abaixo:
	<a href="{{$link}}">{{$link}}</a>
</p>
<p>Caso não tenha solicitado esta mensagem, por gentileza, ignorar</p>
</body>
</html>