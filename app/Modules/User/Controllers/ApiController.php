<?php

namespace App\Modules\User\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\User\Models\UserModel;
use App\Modules\User\Models\PasswordTokenModel;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 *
 * @SWG\Swagger(
 *     basePath="",
 *     host="localhost:8000",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Sample API",
 *         @SWG\Contact(name="Giuliano Sampaio"),
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */
class ApiController extends Controller
{
    /**
     *  s
     * 
     * @var Eloquent s
     */
    protected $model;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }
    
    /**
     * Verifica a entrada do usuário no sistema
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/user/authenticate",
     *     description="Verifica o usuário e senha",
     *     operationId="api.user.index",
     *     produces={"application/json"},
     *     tags={"user"},
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */
    public function authenticate(Request $request)
    {
        $response   = null;
        $username = $request->input('username');
        $password = $request->input('password');

        if ( $password == null || $username == null) {
            $response['code']    = '0101';
            $response['status']  = '00';
            $response['message'] = 'Campos username e password obrigatórios';
            return $response;
        }

        if ($this->checkEmailExists($username) == false) {
            $response['code']    = '0103';
            $response['status']  = '00';
            $response['message'] = 'E-mail nao cadastrado.';
            return $response;
        }

        $user = UserModel::doAuth($username, $password);

        if ($user == null) {
            $response['code']   = '0102';
            $response['status'] = '00';
            $response['message'] = 'Usuário não encontrado';
            return $response;
        }

        if (! $request->session()->has('users')) {
            $request->session()->push('users', 'developers');
        }

        $response['code']    = '0100';
        $response['status']  = '01';
        $response['message'] = 'OK';
        $response['user']['nome']                   = $user->nome;
        $response['user']['email']                  = $user->login;
        $response['user']['api_token']              = $user->api_token;
        $response['user']['api_token_timestamp']    = $user->api_token_timestamp;
        $response['user']['api_last_call']          = $user->api_last_call;

        return $response; 
    }



    /**
     * Envia URL para a troca de senha do usário
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/user/remember-password",
     *     description="Envia URL para a troca de senha do usário",
     *     operationId="api.user.index",
     *     produces={"application/json"},
     *     tags={"user"},
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     * )
     */
    public function rememberMyPassword(Request $request)
    {
        $params = $request->input('params');
        
        $email  = $params['login'];

        $user  =  $this->model->where('login', $email)->first(); 

        if ($user == null) {
            $response['code']    = '0103';
            $response['status']  = '00';
            $response['message'] = 'Campos username e password obrigatórios';
            return $response;
        }

        $token = $this->generateResetToken($email);

        $url = env('URL_FRONT') . '/mudar-senha/'. $token;

        $this->sendRecoverMail($email, $url);

        $response['code']    = '0100';
        $response['status']  = '01';
        $response['message'] = 'OK';

        return $response;
    }



    /**
     * Função para troca de senha
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/user/change-password",
     *     description="Função para troca de senha",
     *     operationId="api.user.index",
     *     produces={"application/json"},
     *     tags={"user"},
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     * )
     */
    public function changePassword(Request $request)
    {
        $params = $request->input('params');
        $reset  = new PasswordTokenModel();

        $reset = $reset->where('token', $params['recoverToken'])->first();

        if ($reset == null) {
            $response['code']    = '0104';
            $response['status']  = '00';
            $response['message'] = 'O token enviado é inválido ou já foi utilizado';

            return $response;
        }

        $user  = $this->model->where('login', $reset['email'])->first();

        $user->senha = md5($params['password']);        
        $user->save();

        $reset->where('token', $params['recoverToken'])->delete();

        $response['code']    = '0100';
        $response['status']  = '00';
        $response['message'] = 'OK';

        return $response;
    }


    # <--- {Private functions} --->


    /**
     * Gera o token para troca de senha
     * 
     * @param   $email 
     * @return  string      
     */
    protected function generateResetToken($email)
    {
        $reset = new PasswordTokenModel();

        $existent = $reset->where('email', $email)->first();

        if ($existent != null) {
            return $existent->token;
        }   
        
        $token = sha1(time());

        $reset->email = $email;
        $reset->token = $token;

        $reset->save();

        return $token;
    } 

    /**
     * Manda e-mail com URL para a troca de senha
     * 
     * @param  $email 
     * @param  $link  
     * @return string
     */
    protected function sendRecoverMail($email, $link)
    {
        $title   = 'Solicitação de troca de senha';

        $this->email = $email;

        Mail::send('User::recover', ['title' => $title, 'link' => $link], function ($message)
        {
            $message->from('from@no-reply.com', 'Recuperação de senha');
            $message->to($this->email);
        });

        return response()->json(['message' => 'Request completed']);
    }


    /**
     * Manda e-mail com URL para a troca de senha
     * 
     * @param  $email 
     * @param  $link  
     * @return string
     */
    protected function checkEmailExists($email) 
    {
        $user  = $this->model->where('login', $email)->first();

        //die($email);

        return ($user == null) ? false : true;
    }
}
