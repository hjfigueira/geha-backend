<?php
/*
|--------------------------------------------------------------------------
| User Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the User module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group([
    'middleware' => ['web'],
    'prefix' => 'api/user',
    'namespace' => 'App\Modules\User\Controllers'
], function () {
	Route::get('teste', ['as' => 'api/user.change', 'uses' => 'ApiController@checkEmailExists']);
	Route::post('authenticate', 	 ['as' => 'api/user.authenticate', 'uses' => 'ApiController@authenticate']);
	Route::post('change-password', 	 ['as' => 'api/user.change', 	   'uses' => 'ApiController@changePassword']);
	Route::post('remember-password', ['as' => 'api/user.remember', 	   'uses' => 'ApiController@rememberMyPassword']);
});