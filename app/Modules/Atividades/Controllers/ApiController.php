<?php

namespace App\Modules\Atividades\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Exceptions\WsException;
use App\Http\Controllers\Controller;
use App\Modules\Esquemas\Models\EsquemasModel;
use App\Modules\Atividades\Models\AtividadesModel;
use Illuminate\Database\QueryException as QueryException;
use App\Modules\PerguntasBasicas\Controllers\ApiController as PerguntasBasicas;

class ApiController extends Controller
{

    protected $model;

    /**
     * Instancia os models e outros atributos da classe
     */
    public function __construct()
    {
        $this->model = new AtividadesModel();
    }


    /**
     * Salva uma grade de atividades no banco de dados
     * @param  Request $request 
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        if ( ! $request->input('params')) {
            return ['created' => false, 'code' => '0501', 'status' => '00'];
        }
        
        $atividades = $request->input('params');

        $this->store($atividades);

        return ['created' => true, 'code' => '0500', 'status' => '01'];
    }

    /**
     * Verifica se e o esquema possui alguma atividade cadastradas, 
     * de acordo com a data inicial indicada pelo user
     * @param  $idEsquema 
     * @return boolean            
     */
    public function has($idEsquema)
    {
        $perguntas = new PerguntasBasicas();
        
        $data  = $perguntas->get();
        $check = [];

        foreach ($data['dias'] as $dias) {

            foreach ($dias as &$dia) {

                $dia['has'] = ! $this->check($idEsquema, $dia['id']);
                    
                $check[] = $dia;
            }
        }

        return $check;
    }


    /**
     * Retorna as atividades de um determinado esquema
     * @param  int $idEsquema
     * @param  int $idDia
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($idEsquema, $idDia)
    {
        $response = [];
        
        $atividades = $this->model
                           ->where('id_esquema', $idEsquema)
                           ->where('id_dia', $idDia)
                           ->get();     

        foreach ($atividades as $atividade) {

            $horaInicio = $this->formatHour($atividade->horario_inicio);

            $response[] = ['atividade'   => $atividade->descricao,
                           'id'          => $atividade->id,
                           'idEsquema'   => $atividade->id_esquema,
                           'idDia'       => $idDia,
                           'horaInicio'  => $horaInicio,
                           'tipo'        => $atividade->tipo_atividade,
                           'duracao'     => $atividade->duracao
                          ];
        }

        return $response;
    }

    /**
     * Duplica as atividades de um dia em todos os outros dias ativos
     * @param  Request $request 
     * @return json           
     */
    public function duplicateAll(Request $request)
    {
        $params = $request->input();

        $validator = Validator::make($params, [
            'id_esquema'  => 'required|numeric',
        ]);

        if ($validator->fails()) {
            throw new WsException('0511', 'duplicate');
        }

        $validator = Validator::make($params, [
            'atividades' => 'required|array|filled',
        ]);

        if ($validator->fails()) {
            throw new WsException('0512', 'duplicate');
        }

        $idEsquema  = $params['id_esquema'];
        $atividades = $params['atividades'];

        $data = $this->model->where('id_esquema', $idEsquema)->get();

        if (! $data->isEmpty()) {            
            if ($this->removeAllFromEsquema($idEsquema) == false) {
                return ['remove' => false, 'code' => '0515', 'status' => '00'];
            }            
        }        

        $this->saveOnDiasAtivos($atividades);                          
                
        return ['remove' => true, 'code' => '0500', 'status' => '01'];
    }


    /**
     * Duplica as atividades de um dia em alguns dias especificos
     * @param  Request $request 
     * @return json           
     */
    public function duplicateDay(Request $request)
    {
        $params = $request->input();

        $validator = Validator::make($params, [
            'id_esquema'  => 'required|numeric',
        ]);

        if ($validator->fails()) {
            throw new WsException('0511', 'duplicate');
        }

        $validator = Validator::make($params, [
            'atividades' => 'required|array|filled',
        ]);

        if ($validator->fails()) {
            throw new WsException('0512', 'duplicate');
        }

        $validator = Validator::make($params, [
            'dias_ativos' => 'required|array|filled',
        ]);

        if ($validator->fails()) {
            throw new WsException('0513', 'duplicate');
        }

        $idEsquema  = $params['id_esquema'];
        $atividades = $params['atividades'];
        $diasAtivos = $params['dias_ativos'];

        foreach ($diasAtivos as $dia) {
            
            $data = $this->model->where('id_esquema', $idEsquema)
                                ->where('id_dia', $dia)
                                ->get();

            if ($dia < 1 || $dia > 14) {
                throw new WsException('0514', 'duplicate');
            }                                

            if (! $data->isEmpty() ) {
                if (! $this->remove($idEsquema, $dia)) {
                    throw new WsException('0515', 'duplicate');  
                }
            }

            $this->store($atividades, $dia);
        }

        return ['duplicate' => true, 'code' => '0500', 'status' => '01'];
    }

    /**
     * Salva as atividades em TODOS os dias ativos
     * É necessário que tenha sido removidos as atividades 
     * dos dias anteriores
     * para não haver conflitos entre os horários
     * @param  array $atividades  Objeto com as atividades para serem replicadas 
     * @return boolean
     */
    public function saveOnDiasAtivos($atividades)
    {
        $perguntas = new PerguntasBasicas();

        $data  = $perguntas->get();

        foreach ($data['dias'] as $semana) {

            foreach ($semana as $dia) {
                if ($dia['check'] == false) {
                    continue;
                }  
                $this->store($atividades, $dia['id']);
            }
        }
    }

    /**
     * Remove todas as atividades de um determinado de esquema 
     *  e de um determinado dia
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAtividades(Request $request)
    {
        $atividades = $request->input('params');
        
        $model = new AtividadesModel();

        foreach($atividades as $atividade) {

            if ( ! isset($atividade['id']) ) {
                continue;
            
            } try {

                $item = $model->find($atividade['id']);

                if ($item == null) {
                    continue;
                }

                $item->delete();
                
            } catch (Exception $e) {
                return ['remove' => false, 'code' => '0508', 'status' => '00'];
            }
        }

        return ['remove' => true, 'code' => '0500', 'status' => '01'];
    }

    /**
     * Função auxiliar para remover TODAS as atividades 
     * de um determinado esquema
     * @param   $idEsquema 
     * @return  boolean
     */
    protected function removeAllFromEsquema($idEsquema)
    {
        try {
            $this->model->where('id_esquema', $idEsquema)->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }    
    }

    /**
     * Remove as atividades de um determinado esquema e dia
     * @param   $idEsquema 
     * @param   $idDia 
     * @return              
     */
    protected function remove($idEsquema, $idDia)
    {
        try {
            $this->model->where('id_esquema', $idEsquema)
                        ->where('id_dia', $idDia)
                        ->delete();
            return true;
            
        } catch (QueryException $e) {
            return false;
        }
    }


    /**
     * Armazena a atividade no banco de dados
     * @param  $atividade Array com as atividades
     * @param  $dia       Id do dia ativo
     * @todo   Tirar a verificação de esquema dentro do for.
     */
    protected function store($atividades, $dia = null)
    {
        foreach ($atividades as $atividade) {

            $model = new AtividadesModel();  

            if ( isset($atividade['id']) ) {
                $model = $model->find($atividade['id']);
            }

            $duracao = array_get($atividade, 'duracao', 0);
            
            if ($duracao <= 0) {
                throw new WsException('0502', 'created');
            }

            if (! isset($atividade['tipo'])) {
                throw new WsException('0503', 'created');
            }

            if (! isset($atividade['horaInicio'])) {
                throw new WsException('0504', 'created');
            }

            if (! isset($atividade['idEsquema'])) {
                throw new WsException('0505', 'created');
            }

            if (EsquemasModel::find($atividade['idEsquema']) == null) {
                throw new WsException('0516', 'created');
            } 

            if ($dia == null) {

                if (! isset($atividade['idDia']) ) {
                    throw new WsException('0507', 'created');
                }

                $dia = $atividade['idDia'];
            }

            $model->id_dia          = $dia;
            $model->tipo_atividade  = $atividade['tipo'];
            $model->horario_inicio  = $atividade['horaInicio'];
            $model->id_esquema      = $atividade['idEsquema'];
            $model->duracao         = $duracao;

            // se for do tipo atividade fixa
            if ($atividade['tipo'] == 5) {

                if (! isset($atividade['atividade'])) {
                    throw new WsException("0506", "created");
                }

                $model->descricao = sprintf('%s', $atividade['atividade']);
            }

            try {
                $model->save();
            } catch (Exception $e) {
                throw new WsException("0508", "created");
            }
        }
    }


    /**
     * Formata de HH:MM para minutos
     * @param  string $hour 
     * @return string       
     */
    protected function convertHourToMin($time)
    {
        $pieces = explode(':', $time);

        $min = $pieces[0] * 60 + $pieces[1];

        return $min;
    }


    /**
     * Checa que se existe atividade neste dia no mesmo
     * @param   $idEsquema 
     * @param   $idDia     
     * @return  boolean
     */
    protected function check($idEsquema, $idDia)
    {
        $model = new AtividadesModel();

        $itens = $model->where('id_esquema', $idEsquema)
                       ->where('id_dia', $idDia)
                       ->get();
        
        return $itens->isEmpty();
    }


    /**
     * Formata de HH:MM:SS para HH:MM
     * 
     * @param  string $hour 
     * @return string       
     */
    protected function formatHour($hour)
    {
        if ($hour == null) return null;

        $pieces  = explode(':', $hour);

        return $pieces[0] . ':' . $pieces[1];
    }

    
    /**
     * Converte minutos para HH:MM
     * @param  string $min 
     * @return string      
     */
    protected function convertMinToHour($min)
    {
        return $this->formatHour(gmdate("H:i:s", ($min*60)));
    }
}