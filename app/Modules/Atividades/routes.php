<?php
/*
|--------------------------------------------------------------------------
| Atividades Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Atividades module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/
Route::group([
    'prefix' => 'api/atividades',
    'namespace' => 'App\Modules\Atividades\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('get/{idEsquema}/{idDia}', ['as' => 'api/atividades.getAtividades',    'uses' => 'ApiController@get']);
	Route::get('has/{idEsquema}', ['as' => 'api/atividades.has', 	          'uses' => 'ApiController@has']);
	Route::post('duplicate',   	          ['as' => 'api/atividades.duplicate',  	  'uses' => 'ApiController@duplicateDay']);
	Route::post('duplicate-all',          ['as' => 'api/atividades.duplicate-all',    'uses' => 'ApiController@duplicateAll']);
	Route::post('remove', 		          ['as' => 'api/atividades.removeAtividades', 'uses' => 'ApiController@removeAtividades']);
	Route::post('save',   	 	          ['as' => 'api/atividades.save',  	   		  'uses' => 'ApiController@save']);
});