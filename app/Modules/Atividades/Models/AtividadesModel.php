<?php 

namespace App\Modules\Atividades\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class AtividadesModel extends Model
{

	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	protected $table = 'Esquema_Atividades';

	// Connection Database
	protected $connection = 'urania-cliente';


	/**
	 * Get the next Id
	 * @return Integer
	 */
	public function nextId()
	{
		$query = "SELECT max(id_esquema) AS id FROM ".$this->table;

		$ret = DB::connection($this->connection)->select(DB::raw($query));

		$item = $ret[0];

		return ($item->id == null) ? 1 : intval($item->id) + 1; 
	}


	/**
	 * Recupera a quantidade de aulas cadastradas
	 * @param   $esquemasId 
	 * @return              
	 */
	public function getQtdAulasCadastradas($esquemasId)
	{
		$ret = DB::select('SELECT count(id) FROM '. $this->table . ' WHERE id_esquema = '. $esquemasId .' GROUP BY id_esquema, id_dia');

		return $ret;
	}	
}