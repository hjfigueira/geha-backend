<?php

/**
 * @SWG\Get(
 *     path="/atividades/get/{idEsquema}/{idDia}",
 *     summary="Retorna a lista atividades anexadas a um esquema e ao dia da semana.",
 *     description="Retorna a lista atividades anexadas a um esquema e ao dia da semana.",
 *     produces={"application/json"},
 *     tags={"atividades.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID do esquema vinculado à atividade.",
 *         in="path",
 *         name="idEsquema",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="ID do dia vinculado ao dia.",
 *         in="path",
 *         name="idDia",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */

/**
 * @SWG\Get(
 *     path="/atividades/has/{idEsquema}",
 *     summary="Verifica se e o esquema possui alguma atividade cadastrada",
 *     description="Verifica se e o esquema possui alguma atividade cadastrada, de acordo com o id do dia indicado pelo usuário.",
 *     produces={"application/json"},
 *     tags={"atividades.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Parameter(
 *         description="ID do esquema vinculado à atividade.",
 *         in="path",
 *         name="idEsquema",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/atividades/save",
 *     operationId="save",
 *     consumes={"application/json"},
 *     summary="Salva uma lista de atividades",
 *     description="Cria ou atualiza uma coletânea de atividades no sistema. Para atualizar uma atividade existente basta passar o id do objeto. 

 Cada atividade deve ter um tipo que poderão ser:  

 1 - terá aula
 2 - não terá aula
 3 - talvez tenha aula
 4 - intervalo
 5 - atividade fixa
 6 - fim de período

 Possíveis status:
 *     [code=0500] = Efetuado com sucesso.
 *     [code=0501] = Item parâmetros não informado.
 *     [code=0502] = A duração da atividade é menor ou igual a zero ou inválida.
 *     [code=0503] = O campo 'tipo' não foi encontrado nos parâmetros de atividade.
 *     [code=0504] = O campo 'horaInicio' não foi encontrado nos parâmetros de atividade.
 *     [code=0505] = O campo 'idEsquema' não foi encontrado nos parâmetros de atividade.
 *     [code=0506] = A atividade com o tipo 'atividade fixa' necessita do campo 'descrição'.
 *     [code=0507] = O campo 'idDia' não informado.
 *     [code=0508] = Não foi executar a função no banco de dados.
 *     [code=0516] = Esquema não encontrado no banco de dados.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"atividades.basico"},
 *     @SWG\Parameter(
 *         name="atividade",
 *         in="body",
 *         description="Objeto JSON com as propriedades da atividade",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/RelAtividadeModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/atividades/duplicate",
 *     operationId="duplicate",
 *     consumes={"application/json"},
 *     summary="Duplica as atividades em um dia específico",
 *     description="Duplica uma coletânea de atividade em um dia específco ou coletânea de dias especificados pelo usuário. 
 *     
 Possíveis status:
 *     [code=0500] = Efetuado com sucesso.
 *     [code=0501] = Item parâmetros não informado.
 *     [code=0502] = A duração da atividade é menor ou igual a zero ou inválido.
 *     [code=0503] = O campo 'tipo' não foi encontrado nos parâmetros de atividade.
 *     [code=0504] = O campo 'horaInicio' não foi encontrado nos parâmetros de atividade.
 *     [code=0505] = O campo 'idEsquema' não foi encontrado nos parâmetros de atividade.
 *     [code=0506] = A atividade com o tipo 'atividade fixa' necessita do campo 'descrição'.
 *     [code=0507] = O campo 'idDia' não informado.
 *     [code=0508] = Não foi possível executar a função no banco de dados.
 *     [code=0511] = O campo 'id_esquema' não informado.
 *     [code=0512] = O campo 'atividades' é vazio ou inválido.
 *     [code=0513] = O campo 'dias_ativos' é vazio ou inválido.
 *     [code=0514] = O dia ativo informado é inválido.
 *     [code=0515] = Não foi possível remover as atividades existentes deste esquema.
 *     [code=0516] = Esquema não encontrado no banco de dados.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"atividades.basico"},
 *     @SWG\Parameter(
 *         name="atividade",
 *         in="body",
 *         description="Objeto JSON com as propriedades da atividade",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/DuplicateModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/atividades/duplicate-all",
 *     operationId="duplicate",
 *     consumes={"application/json"},
 *     summary="Duplica atividades em todos os dias ativos",
 *     description="Duplica uma coletânea de atividade em todos os dias ativos no sistema. 
 Possíveis status:
 *     [code=0500] = Efetuado com sucesso.
 *     [code=0501] = Item parâmetros não informado.
 *     [code=0502] = A duração da atividade é menor ou igual a zero ou inválido.
 *     [code=0503] = O campo 'tipo' não foi encontrado nos parâmetros de atividade.
 *     [code=0504] = O campo 'horaInicio' não foi encontrado nos parâmetros de atividade.
 *     [code=0505] = O campo 'idEsquema' não foi encontrado nos parâmetros de atividade.
 *     [code=0506] = A atividade com o tipo 'atividade fixa' necessita do campo 'descrição'.
 *     [code=0507] = O campo 'idDia' não informado.
 *     [code=0508] = Não foi possível executar a função no banco de dados.
 *     [code=0511] = O campo 'id_esquema' não informado.
 *     [code=0512] = O campo 'atividades' é vazio ou inválido.
 *     [code=0515] = Não foi possível remover as atividades existentes deste esquema.
 *     [code=0516] = Esquema não encontrado no banco de dados.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"atividades.basico"},
 *     @SWG\Parameter(
 *         name="atividade",
 *         in="body",
 *         description="Objeto JSON com as propriedades da atividade",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/DuplicateAllModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/atividades/remove",
 *     operationId="duplicate",
 *     consumes={"application/json"},
 *     summary="Remove uma lista de atividades",
 *     description="Remove uma lista de atividades no sistema.
 *     [code=0500] = Efetuado com sucesso.
 *     [code=0508] = Não foi possível executar a função no banco de dados.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"atividades.basico"},
 *     @SWG\Parameter(
 *         name="atividade",
 *         in="body",
 *         description="Objeto JSON com as propriedades da atividade",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/RmAtividadeModel"),
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */