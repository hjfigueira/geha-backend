<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="RelAtividadeModel")
 * )
 */
class RelAtividadeModel
{
    /**
     * @var AtividadeModel[]
     * @SWG\Property(@SWG\Xml(name="atividades",wrapped=true))
     */
    public $params;
}