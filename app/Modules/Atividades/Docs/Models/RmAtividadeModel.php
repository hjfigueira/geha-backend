<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="RmAtividadeModel")
 * )
 */
class RmAtividadeModel
{
    /**
     * @var ItemRemoveModel[]
     * @SWG\Property(@SWG\Xml(name="atividades",wrapped=true))
     */
    public $params;
}


/**
 * @SWG\Definition(
 *     type="object", 
 *     @SWG\Xml(name="ItemRemoveModel")
 * )
 */
class ItemRemoveModel
{
    /**
     * @SWG\Property(example=1)
     * @var int
     */
    public $id;
}