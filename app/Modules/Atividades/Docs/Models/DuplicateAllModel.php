<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="DuplicateAllModel")
 * )
 */
class DuplicateAllModel
{
	/**
     * @var int 
     * @SWG\Property(@SWG\Xml(name="atividades",wrapped=true))
     */
    public $id_esquema

    /**
     * @var AtividadeModel[]
     * @SWG\Property(@SWG\Xml(name="atividades",wrapped=true))
     */
    public $atividades;
}