<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="DuplicateModel")
 * )
 */
class DuplicateModel
{
	/**
     * @var int 
     * @SWG\Property(@SWG\Xml(name="atividades",wrapped=true))
     */
    public $id_esquema

    /**
     * @var AtividadeModel[]
     * @SWG\Property(@SWG\Xml(name="atividades",wrapped=true))
     */
    public $atividades;

    /**
     * @var int[]
     * @SWG\Property(@SWG\Xml(name="atividades",wrapped=true))
     */
	public $dias_ativos
}