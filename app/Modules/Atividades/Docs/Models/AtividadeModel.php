<?php 
/**
 * @SWG\Definition(
 *     required={"nome"}, 
 *     type="object", 
 *     @SWG\Xml(name="AtividadeModel")
 * )
 */
class AtividadeModel
{
    /**
     * @SWG\Property(example=1)
     * @var int
     */
    public $idDia;

    /**
     * @SWG\Property(example=1)
     * @var int
     */
    public $tipo;

    /**
     * @SWG\Property(example="09:00")
     * @var time
     */
    public $horaInicio;

    /**
     * @SWG\Property(example=1)
     * @var int
     */
    public $idEsquema;

    /**
     * @SWG\Property(example="60")
     * @var int
     */
    public $duracao;

    /**
     * @SWG\Property(example="Descrição de atividade")
     * @var string
     */
    public $atividade;

}