<?php 

namespace App\Modules\Esquemas\Models;

use Illuminate\Database\Eloquent\Model;

class EsquemasModel extends Model
{

	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	protected $table = 'Esquemas';

	// Connection Database
	protected $connection = 'urania-cliente';


	public function turno() 
	{
		$namespace = 'App\Modules\Turnos\Models\TurnosModel';
		return $this->belongsTo($namespace, 'id_turno', 'id');
	}

	public function atividades()
    {
        return $this->hasMany(
            'App\Modules\Esquemas\Models\EsquemaAtividadesModel',
            'id_esquema');
    }

    public static function isFilled()
    {
        return (self::count() >= 1);
    }
}