<?php

/**
 * @SWG\Get(
 *     path="/esquemas/get/{esquemaID}",
 *     description="Método para salvar um esquema",
 *     produces={"application/json"},
 *     tags={"esquemas.basico"},
 *     @SWG\Parameter(
 *         description="Id do esquema para recuperar detalhes",
 *         in="path",
 *         name="esquemaID",
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Get(
 *     path="/esquemas/all",
 *     description="Método para salvar um esquema",
 *     produces={"application/json"},
 *     tags={"esquemas.basico"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/esquemas/save",
 *     description="
 *     Método para salvar um esquema
 *     Estrutura do objeto para salvar
 *     {
 *          'params' :
 *          {
 *              'nome' : 'Nome',
 *              'id' : 2
 *          }
 *      }
 *     ",
 *     produces={"application/json"},
 *     tags={"esquemas.basico"},
 *     @SWG\Parameter(
 *         description="Objeto para salvar",
 *         in="body",
 *         type="integer",
 *         format="int64",
 *         @SWG\Schema (
 *                  @SWG\Property(
 *                      property="params",
 *                      @SWG\Schema()
 *                  ),
 *              )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/esquemas/remove/{esquemaID}",
 *     description="Método para remover um esquema por completo.
 *     Possíveis status:
 *     [code=0500] = Efetuado com sucesso.
 *     [code=0506] = Não foi possível remover as atividades deste esquema. Verifique se estas atividades não estão sendo utilizadas em outra parte do sistema.
 *     [code=0507] = Não foi possível remover as turmas deste esquema. Verifique se estas turmas não estão sendo utilizadas em outra parte do sistema.     
 *     [code=0508] = Não foi possível remover o esquema. Verifique se este esquema não está sendo utilizado em outra parte do sistema.",
 *     produces={"application/json"},
 *     tags={"esquemas.basico"},
 *     @SWG\Parameter(
 *         description="Id do esquema para remoção",
 *         in="path",
 *         name="esquemaID",
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/esquemas/remove-all-atividades/{esquemaID}/{diaID}",
 *     description="Método para salvar um esquema",
 *     produces={"application/json"},
 *     tags={"esquemas.basico"},
 *     @SWG\Parameter(
 *         description="Id do esquema para remoção",
 *         in="path",
 *         name="esquemaID",
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         description="Id do dia para remoção",
 *         in="path",
 *         name="diaID",
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     )
 * )
 */