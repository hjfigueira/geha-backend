<?php

namespace App\Modules\Esquemas\Controllers;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Exceptions\WsException;
use App\Http\Controllers\Controller;
use App\Modules\Turnos\Models\TurnosModel;
use App\Modules\Esquemas\Models\EsquemasModel;
use App\Modules\Turmas\Models\TurmasEsquemaModel;
use App\Modules\Esquemas\Models\EsquemaAtividadesModel;
use Illuminate\Database\QueryException as QueryException;
use App\Modules\PerguntasBasicas\Models\InformacoesBasicasModel;
use App\Modules\Atividades\Controllers\ApiController as Atividades;
use App\Modules\PerguntasBasicas\Controllers\ApiController as PerguntasBasicas; 

class ApiController extends Controller
{

    protected $esquemas;
    protected $grade;

    public function __construct()
    {
        $this->esquemas = new EsquemasModel();
        $this->grade    = new EsquemaAtividadesModel();
    }

    /**
     * Salva um novo esquema no banco de dados
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->input('params');

        if ($params['id'] == '' || $params['nome']) {
            $error['code']     = '0401';
            $error['status']   = '00';
            $error['message']  = 'Passagem de paramêtros insuficientes. ';
            $error['message'] .= 'Verifique os dados enviados e tente novamente';
        }

        $this->esquemas->id_turno = $params['id'];
        $this->esquemas->nome     = $params['nome'];
        $this->esquemas->save();

        $response['code']      = '0400';
        $response['status']    = '01';
        $response['message']   = 'OK';
        $response['esquemaId'] = $this->esquemas->id;
        
        return $response;
    }

    /**
     * Salva um novo esquema no banco de dados
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(Request $request)
    {
        $esquemas = $this->esquemas->all();
        $list     = [];        

        foreach($esquemas as $esquema) {
            $list[] = ['id'     => $esquema->id,
                       'nome'   => $esquema->nome,
                       'turno'  => $esquema->turno->id,
                       'status' => $this->getStatus($esquema->id)];
        }

        return $list;
    }

    /**
     * Salva um novo esquema no banco de dados
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $esquema = $this->esquemas->find($id);

        if($esquema != null)
        {
            $inicio  = $esquema->turno->hr_inicio;
            $termino = $esquema->turno->hr_termino;

            $semanas =  $this->diasAtivos($id, $inicio, $termino);

            $response['nome']        = $esquema->nome;
            $response['turno']       = $esquema->turno->nome;
            $response['hr_inicio']   = $this->formatHour($inicio);
            $response['hr_termino']  = $this->formatHour($termino);
            $response['dias_semana'] = $semanas;

            return $response;
        }
        else{

            return [ 'getted' => 0, 'code' => '0405', 'status' => false];

        }
    }


    /**
     * Retorna as atividades de um determinado esquema
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAtividades($idEsquema, $idDia)
    {
        $response = [];

        $atividades = $this->grade
                           ->where('id_esquema', $idEsquema)
                           ->where('id_dia', $idDia)
                           ->get();

        foreach ($atividades as $atividade) {

            $response[] = ['atividade'   => $atividade->descricao,
                           'id'          => $atividade->id,
                           'duracao'     => $atividade->duracao,
                           'horaInicio'  => $this->formatHour($atividade->horario_inicio),
                           'idatividade' => $atividade->tipo_atividade,
                           'duracao'     => $this->convertMinToHour($atividade->duracao)
                          ];
        }

        return $response;
    }

    /**
     * Remove um determinada esquema
     * @return \Illuminate\Http\JsonResponse
     * @todo  Dividir a função de remover em funções menores 
     */
    public function remove($idEsquema)
    {
        DB::beginTransaction();

        try {

            $grade = $this->grade
                          ->where('id_esquema', $idEsquema);

            if ($grade != null) {
                $grade->delete();
            }

        } catch (QueryException $e) {
            DB::rollback();
            throw new WsException('0406', 'remove');
        }

        try {

            $turmas = TurmasEsquemaModel::where('id_esquema', $idEsquema);

            if ($turmas != null) {
                $turmas->delete();
            }

        } catch (QueryException $e) {
            DB::rollback();
            die($e->getMessage());
            throw new WsException('0407', 'remove');
        }

        try {
            
            $esquema = $this->esquemas->find($idEsquema);

            if ($esquema != null) {
                $esquema->delete();
            }

        } catch (QueryException $e) {
            DB::rollback();
            throw new WsException('0408', 'remove');
        }
        
        DB::commit();

        return ['remove' => true, 'code' => '0400', 'status' => '01'];
    }

    /**
     * Remove todas as atividades de um determinado de esquema
     *  e de um determinado dia
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAllAtividades($idEsquema, $idDia)
    {
        try {

            $grade = $this->grade
                          ->where('id_esquema', $idEsquema)
                          ->where('id_dia', $idDia)
                          ->delete();

            $success = ['code' => '0402', 'status' => '01', 'message' => 'OK'];

            return response()->json($success);

        } catch (QueryException $e) {

            $msg  = 'Não foi possível remover as atividades do dia selecionado.';
            $msg .= 'Verifique se o dia removido contém outros vínculos';

            $error = ['code' => '0400', 'status' => '00', 'message' => $msg];

            return response()->json($error);
        }
    }

    /**
     * Remove todas as atividades de um determinado de esquema
     *  e de um determinado dia
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAtividades(Request $request)
    {
        $grade = $request->input('params');

        $model = new EsquemaAtividadesModel();

        foreach($grade as $atividade) {

            if ( ! isset($atividade['id']) ) {
                continue;
            }

            $model->find($atividade['id'])->delete();
        }
    }

    /**
     * Verifica se o dia em questão possuí todas
     * @return [type] [description]
     */
    protected function diasAtivos($idEsquema, $inicio, $termino)
    {
        $controller = new PerguntasBasicas();

        $semanas = $controller->get();
        $semanas = $semanas['dias'];

        foreach ($semanas as &$semana) {

            foreach ($semana as &$dia) {

                if (@$dia['check'] == true) {
                    $dia['complete'] = $this->ifDiaComplete($dia['id'], $idEsquema, $inicio, $termino);
                }
            }
        }

        return $semanas;
    }

    /* -------- {Private Functions} -------- */


    /**
     * Formata de HH:MM:SS para HH:MM
     * 
     * @param  string $hour 
     * @return string       
     */
    protected function formatHour($hour)
    {
        if ($hour == null) return null;

        $pieces  = explode(':', $hour);

        return $pieces[0]. ':' .$pieces[1];
    }

    /**
     * Formata de HH:MM para minutos
     * 
     * @param  string $hour 
     * @return string       
     */
    protected function convertHourToMin($time)
    {
        $pieces = explode(':', $time);

        $min = $pieces[0] * 60 + $pieces[1];

        return $min;
    }

    /**
     * Converte minutos para HH:MM
     * @param  string $min 
     * @return string      
     */
    protected function convertMinToHour($min)
    {
        return $this->formatHour(gmdate("H:i:s", ($min*60)));
    }

    /**
     * Verifica o status do esquema, se está incompleto ou não
     * @param  string $min 
     * @return string      
     */
    protected function getStatus($id)
    {
        $model = new InformacoesBasicasModel();

        $info = $model->get();

        $diasAula = @$info[0]->qtde_dias;

        $diasCadastrados = $this->grade->getQtdAulasCadastradas($id);        

        $diasCadastrados = ($diasCadastrados == null) ? 0 : count($diasCadastrados);

        $diff = $diasAula - $diasCadastrados;

        $status = ($diff == 0) ? true : false;

        return $status;
    }


    protected function ifDiaComplete($dia, $esquema, $inicio, $termino)
    {
        $controller = new Atividades();

        $duracaoEsquema = $this->diffHour($inicio, $termino);

        $duracaoAtividades = 0; 

        $atividades = $controller->get($esquema, $dia); 

        foreach ($atividades as $atividade) {

            $duracaoAtividades += $this->convertHourToMin($atividade['duracao']);
        }

        $duracaoAtividades = $this->convertMinToHour($duracaoAtividades);            

        return ($duracaoAtividades == $duracaoEsquema);
    }


    protected function diffHour($inicio, $termino)
    {
        $inicio  = $this->convertHourToMin($inicio);
        $termino = $this->convertHourToMin($termino);

        return $this->convertMinToHour($termino - $inicio);
    }
}
