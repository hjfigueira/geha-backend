<?php
/*
|--------------------------------------------------------------------------
| Esquemas Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Esquemas module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/
Route::group([
    'prefix' => 'api/esquemas',
    'namespace' => 'App\Modules\Esquemas\Controllers',
    'middleware' => 'auth'
], function () {
	Route::get('all',      	          ['as' => 'api/esquemas.all',  	   'uses' => 'ApiController@all']);
	Route::get('get/{id}', 	          ['as' => 'api/esquemas.get',  'uses' => 'ApiController@get']);
	Route::post('save', 			  ['as' => 'api/esquemas.save', 'uses' => 'ApiController@save']);
	Route::post('remove/{idEsquema}', ['as' => 'api/esquemas.remove', 'uses' => 'ApiController@remove']);
	Route::post('remove-all-atividades/{idEsquema}/{idDia}', ['as' => 'api/esquemas.removeAllAtividades', 'uses' => 'ApiController@removeAllAtividades']);
});
