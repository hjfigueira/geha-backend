<?php
/**
 * @SWG\Get(
 *     path="/perguntasespecificas/all",
 *     operationId="all",
 *     summary="Retorna lista de tipos específicos",
 *     description="Retorna lista de tipos específicos",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_especificas.basico"},
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Get(
 *     path="/perguntasespecificas/edit",
 *     operationId="edit",
 *     summary="Retorna lista de opções específicas",
 *     description="Retorna lista de opções específicas",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_especificas.basico"},
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Get(
 *     path="/perguntasespecificas/filtro",
 *     operationId="filtro",
 *     summary="Envia os dados para os filtros",
 *     description="Envia os dados que estarão disponíveis para o usuário realizar a busca",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_especificas.basico"},
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/perguntasespecificas/save",
 *     operationId="save",
 *     summary="Salva a opção ideal e aceitável",
 *     description="Salva a opção ideal e aceitável de uma dupla",
 *     [code=1600] = Efetuado com sucesso.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_especificas.basico"},
 *     @SWG\Parameter(
 *         name="id_opcao",
 *         in="body",
 *         description="ID da Opção",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="id_aceitavel",
 *         in="body",
 *         description="ID da opção aceitável",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="qualidade",
 *         in="body",
 *         description="Valor do Slider",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="matriz",
 *         in="body",
 *         description="ID da Matriz_Curricular",
 *         required=true
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/perguntasespecificas/change",
 *     operationId="change",
 *     summary="Busca as opções aceitáveis.",
 *     description="Busca as opções aceitáveis, baseando-se na opção ideal escolhida.",
 *     [code=1600] = Efetuado com sucesso.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_especificas.basico"},
 *     @SWG\Parameter(
 *         name="tipo",
 *         in="body",
 *         description="Objeto JSON com o tipo da opção escolhida",
 *         required=true
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/perguntasespecificas/opcoesValidas",
 *     operationId="opcoesValidas",
 *     summary="Retorna a listagem de tipos",
 *     description="Retorna a listagem de tipos
 *     [code=1600] = Efetuado com sucesso.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_especificas.basico"},
 *     @SWG\Parameter(
 *         name="id_disciplina",
 *         in="body",
 *         description="ID da Disciplina",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="id_turma",
 *         in="body",
 *         description="ID da Turma",
 *         required=true
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/perguntasespecificas/search",
 *     operationId="search",
 *     summary="Retorna a listagem filtrada de duplas",
 *     description="Retorna a listagem de tipos
 *     [code=1600] = Efetuado com sucesso.",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     tags={"perguntas_especificas.basico"},
 *     @SWG\Parameter(
 *         name="disciplina",
 *         in="body",
 *         description="ID do Grupo de Disciplinas",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="professor",
 *         in="body",
 *         description="ID do Grupo de Professores",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="ideal",
 *         in="body",
 *         description="ID do Tipo Ideal",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="aceitavel",
 *         in="body",
 *         description="ID do Tipo Aceitavel",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="tipos_diferentes",
 *         in="body",
 *         description="Usado para retornar apenas duplas com tipos específicos cadastrados",
 *         required=true
 *     ),
 *     @SWG\Parameter(
 *         name="palavra_chave",
 *         in="body",
 *         description="Palavra de referencia para buscar Disicplinas e Turmas",
 *         required=true
 *     ),
 *     @SWG\Response(
 *     	   response=200,
 *         description="OK"
 *     )
 * )
 */