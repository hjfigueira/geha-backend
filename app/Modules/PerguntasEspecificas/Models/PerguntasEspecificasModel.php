<?php

namespace App\Modules\PerguntasEspecificas\Models;

use App\Models\WsClientModel;
use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use App\Modules\Perguntas\Models\PerguntasMatrzModel;
use App\Modules\Perguntas\Models\PerguntasModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesAceitaveisModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesModel;
use App\Modules\Perguntas\Models\PrefDistribuicaoAulasModal;
use App\Modules\Tipos\Models\TiposModel;
use App\Modules\Turmas\Models\TurmasModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PerguntasEspecificasModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Perguntas_Tipos';

    protected $connection = 'urania-admin';



    /**
     * Método para executar uma busca de turmas.
     * @param array $filtro
     * @return array
     */
    public function busca($id=null)
    {
        if(empty($id))
        {
//        \DB::enableQueryLog();
            $queryMatriz = MatrizCurricularModel::select('*');
            $matriz = $queryMatriz
                ->get();
        }
        else
        {
            $matriz = MatrizCurricularModel::find($id);
        }
        foreach ($matriz as $dados_matriz) {

            $querydisciplina = DisciplinasModel::select('*');
            $disciplinas=$querydisciplina
                ->where('id',$dados_matriz->id_disciplina)
                ->get();

            $queryturmas = TurmasModel::select('*');
            $turmas=$queryturmas
                ->where('id',$dados_matriz->id_turma)
                ->get();
            $queryopcao = PrefDistribuicaoAulasModal::select('*');
            $opcao = $queryopcao
                ->where('id_matriz_curricular',$dados_matriz->id)
                ->get();
            foreach ($opcao as $dados_opcoes)
            {
                $queryopcaoideal = PerguntasOpcoesModel::select('*');
                $opcaoideal = $queryopcaoideal
                    ->where('id',$dados_opcoes->tipo_ideal)
                    ->get();
                foreach ($opcaoideal as $dados_opcao)
                {
                    $querytipoideal = TiposModel::select('*');
                    $tipoideal= $querytipoideal
                        ->where("id",$dados_opcao->id_tipo)
                        ->get();
                }

                $queryopcaoaceitavel = PerguntasOpcoesAceitaveisModel::select('*');
                $opcaoaceitavel = $queryopcaoaceitavel
                    ->where('id',$dados_opcoes->tipo_aceitavel)
                    ->get();
                foreach ($opcaoaceitavel as $dados_opcao)
                {
                    $querytipoaceitavel = TiposModel::select('*');
                    $tipoaceitavel = $querytipoaceitavel
                        ->where("id",$dados_opcao->id_tipo)
                        ->get();
                }
            }
            $searchReturn[] = array_merge(["disciplina"=>$disciplinas->toArray()],
                ["turma"=>$turmas->toArray()],
                ["opcao_ideal"=>$tipoideal->toArray()],
                ["opcao_aceitavel"=>$tipoaceitavel->toArray()]
            );

        }

        return $searchReturn;
    }

    public static function edit()
    {
        $querygrupoOpcoesEspecificas = PerguntaTipoEspecificoOpcoesModel::select('*');
        $grupoOpcoesIdeais = $querygrupoOpcoesEspecificas
            ->groupBy('grupo_opcoes')
            ->get();
        foreach($grupoOpcoesIdeais as $dados_grupos)
        {
            $queryOpcoesEspecificas = PerguntaTipoEspecificoOpcoesModel::select('id','texto','tipo_opcao','ordem','id_tipo');
            $opcoesIdeais = $queryOpcoesEspecificas
                ->where('grupo_opcoes',$dados_grupos->grupo_opcoes)
                ->orderBy('ordem')
                ->get();
            $searchReturn[] = array_merge(
                ['grupo_opcoes'=>$dados_grupos->grupo_opcoes,"opcoes"=>[$opcoesIdeais->toArray()]]
            );
        }



        return $searchReturn;
    }

    public static function exibeAceitavel($tipo)
    {
        $queryopcoesAceitaveis = PerguntasOpcoesAceitaveisModel::select('*');
        $opcoesAceitaveis = $queryopcoesAceitaveis
            ->where('id_tipo',$tipo)
            ->get();

        $searchReturn[] = array_merge(
            ["opcoes_aceitaveis"=>$opcoesAceitaveis->toArray()]
        );

        return $searchReturn;

    }


}