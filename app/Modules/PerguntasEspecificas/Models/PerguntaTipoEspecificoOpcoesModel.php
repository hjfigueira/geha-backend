<?php

namespace App\Modules\PerguntasEspecificas\Models;

use App\Models\WsClientModel;
use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use App\Modules\Perguntas\Models\PerguntasMatrzModel;
use App\Modules\Perguntas\Models\PerguntasModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesAceitaveisModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesModel;
use App\Modules\Perguntas\Models\PrefDistribuicaoAulasModal;
use App\Modules\Tipos\Models\TiposModel;
use App\Modules\Turmas\Models\TurmasModel;
use Illuminate\Support\Facades\DB;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use Illuminate\Database\Query\Builder;


class PerguntaTipoEspecificoOpcoesModel extends WsClientModel
{
    /**
     * Table in database
     * @var string
     */
    public $table = 'Pergunta_Tipo_Especifico_Opcoes';

    protected $connection = 'urania-admin';


}