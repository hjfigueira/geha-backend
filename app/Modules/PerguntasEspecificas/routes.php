<?php
/*
|--------------------------------------------------------------------------
| Perguntas Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Turnos module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group([
    'prefix' => 'api/perguntasespecificas',
    'namespace' => 'App\Modules\PerguntasEspecificas\Controllers',
    'middleware' => 'auth'
], function () {
    Route::get('all',   ['as' => 'api/perguntasespecificas.all',  'uses' => 'ApiController@all']);
    Route::get('edit', ['as' => 'api/perguntasespecificas.edit', 'uses' => 'ApiController@edit']);
    Route::post('save', ['as' => 'api/perguntasespecificas.save', 'uses' => 'ApiController@save']);
    Route::post('change', ['as' => 'api/perguntasespecificas.change', 'uses' => 'ApiController@change']);
    Route::post('search', ['as' => 'api/perguntasespecificas.search', 'uses' => 'ApiController@search']);
    Route::post('opcoesValidas', ['as' => 'api/perguntasespecificas.opcoesValidas', 'uses' => 'ApiController@opcoesValidas']);
    Route::get('filtro', ['as' => 'api/perguntasespecificas.filtro', 'uses' => 'ApiController@filtro']);

});

