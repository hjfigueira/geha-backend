<?php

namespace App\Modules\PerguntasEspecificas\Controllers;

use App\Modules\Atribuicoes\Models\ProfessoresDasTurmas;
use App\Modules\Disciplinas\Models\DisciplinasGrupoModel;
use App\Modules\Disciplinas\Models\RelDisciplinasGruposModel;
use App\Modules\Disciplinas\Models\DisciplinasModel;
use App\Modules\MatrizCurricular\Models\MatrizCurricularModel;
use App\Modules\Perguntas\Models\PerguntasMatrzModel;
use App\Modules\Perguntas\Models\PerguntasModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesAceitaveisModel;
use App\Modules\Professores\Models\ProfessoresGruposModel;
use App\Modules\Professores\Models\RelProfessoresGruposModel;
use App\Modules\Perguntas\Models\PerguntasOpcoesModel;
use App\Modules\Perguntas\Models\PerguntasEspecificaOpcoesModel;
use App\Modules\Perguntas\Models\PrefDistribuicaoAulasEspecificoModal;
use App\Modules\Perguntas\Models\PrefDistribuicaoAulasModal;
use App\Modules\PerguntasEspecificas\Models\PerguntasEspecificasModel;
use App\Modules\PerguntasEspecificas\Models\PerguntaTipoEspecificoOpcoesModel;
use App\Modules\PerguntasEspecificas\Models\TipoValidacaoModel;
use App\Modules\Turmas\Models\TurmasModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\PerguntasBasicas\Models\DiasAulaModel;
use Illuminate\Database\QueryException as QueryException;
use App\Modules\PerguntasBasicas\Models\InformacoesBasicasModel;


/**
 * Class PerguntasEspecificas ApiController
 *
 * @package App\Http\Controllers
 *
 * @SWG\Swagger(
 *     basePath="",
 *     host="localhost:8000",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Sample API",
 *         @SWG\Contact(name="Giuliano Sampaio"),
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */
class ApiController extends Controller
{

    protected $model;

    /**
     * 
     */
    public function __construct()
    {
        $this->model = new PerguntasEspecificasModel();
    }

    /**
     * Salva as informações básicas da instituição
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/perguntas/save",
     *     description="Salva as perguntas",
     *     produces={"application/json"},
     *     tags={"api"},
     *     @SWG\Response(
     *         response=0200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response=0201,
     *         description="assagem de paramêtros insuficientes.
     *         Verifique os dados enviados e tente nov
     *         amente.",
     *     )
     * )
     */

    public function all()
    {
        $this->model= new MatrizCurricularModel();
        $all = $this->model->all();

        $this->model = new PerguntasEspecificasModel();
        $tiposespecificaos = $this->model->busca();

        return $tiposespecificaos;
    }

    public function filtro()
    {
        $queryProfessoresGrupos = ProfessoresGruposModel::select('*');
        $professoresGrupos = $queryProfessoresGrupos
            ->orderBy('nome')
            ->get();

        $queryDisciplinasGrupos = DisciplinasGrupoModel::select('*');
        $disciplinasGrupos = $queryDisciplinasGrupos
            ->orderBy('nome')
            ->get();

        $queryOpcaoIdeal = PerguntasOpcoesModel::select('*');
        $tipoIdeal = $queryOpcaoIdeal
            ->orderBy('ordem')
            ->get();

        $queryOpcaoAceitavel = PerguntasOpcoesAceitaveisModel::select('*');
        $tipoAceitavel = $queryOpcaoAceitavel
            ->orderBy('ordem')
            ->get();

        $searchReturn[] = array_merge(["disciplina_grupo"=>$disciplinasGrupos->toArray()],
            ["professor_grupo"=>$professoresGrupos->toArray()],
            ["opcao_ideal"=>$tipoIdeal->toArray()],
            ["opcao_aceitavel"=>$tipoAceitavel->toArray()]
        );
        Return $searchReturn;
    }

    public function search(Request $request)
    {


        $params=$request->input();
        $matriz_valida = array();

        if(isset($params['disciplina']))
        {
            $matriz_disciplina = array();
            if($params['disciplina']!="")
            {
                $queryRelDisciplinas = RelDisciplinasGruposModel::select('*');
                $relDisciplinas = $queryRelDisciplinas
                    ->where('id_grupo', $params['disciplina'])
                    ->get();
                foreach ($relDisciplinas as $dados_relDisciplinas)
                {
                    $queryMatriz = PerguntasMatrzModel::select('*');
                    $matriz = $queryMatriz
                        ->where('id_disciplina',$dados_relDisciplinas->id_disciplina)
                        ->get();
                    foreach ($matriz as $dados_matriz)
                    {
                        $matriz_disciplina[] = $dados_matriz->id;
                    }
                }
            }
            $matriz_valida=$matriz_disciplina;
        }

        if(isset($params['professor']))
        {
            $matriz_professor = array();
            if($params['professor']!="")
            {
                $queryRelProfessores = RelProfessoresGruposModel::select('*');
                $relprofessores = $queryRelProfessores
                    ->where('id_grupo', $params['professor'])
                    ->get();
                foreach ($relprofessores as $dados_professores)
                {
                    $queryProfessoresTurmas = ProfessoresDasTurmas::select('*');
                    $professoresTurmas = $queryProfessoresTurmas
                        ->get();
                    foreach ($professoresTurmas as $dados_professoresTurmas)
                    {
                        $id_professor = str_replace("[","",$dados_professoresTurmas->professores);
                        $id_professor = str_replace("]","",$id_professor);
                        $id_professor = explode(",",$id_professor);
                        if(in_array($dados_professores->id_professor,$id_professor)==true){
                            $matriz_professor[]=$dados_professoresTurmas->id_matriz_curricular;
                        }
                }

                }
            }
            $matriz_valida=$matriz_professor;
        }

        if(isset($params['ideal']))
        {
            $matriz_ideal = array();
            if($params['ideal']!="")
            {
                $queryideal = PrefDistribuicaoAulasModal::select('*');
                $opcoes_ideais = $queryideal
                    ->where('tipo_ideal',$params['ideal'])
                    ->get();
                foreach ($opcoes_ideais as $dados_opcoes_ideais)
                {
                    $matriz_ideal[]=$dados_opcoes_ideais->id_matriz_curricular;
                }
            }
            $matriz_valida=$matriz_ideal;
        }

        if(isset($params['aceitavel']))
        {
            $matriz_aceitavel = array();
            if($params['aceitavel']!="")
            {
                $queryaceitavel = PrefDistribuicaoAulasModal::select('*');
                $opcoes_aceitaveis = $queryaceitavel
                    ->where('tipo_aceitavel',$params['aceitavel'])
                    ->get();
                foreach ($opcoes_aceitaveis as $dados_opcoes_aceitaveis)
                {
                    $matriz_aceitavel[]=$dados_opcoes_aceitaveis->id_matriz_curricular;
                }
            }
            $matriz_valida=$matriz_aceitavel;
        }

        if(isset($params['tipos_diferentes']))
        {
            $matriz_diferentes = array();
            if($params['tipos_diferentes']!="")
            {
                $queryTiposDiferentes = PrefDistribuicaoAulasEspecificoModal::select('*');
                $tiposDiferentes = $queryTiposDiferentes
                    ->get();
                foreach ($tiposDiferentes as $dados_tipos_diferentes)
                {
                    $matriz_diferentes[]=$dados_tipos_diferentes->id_matriz_curricular;
                }
            }
            $matriz_valida=$matriz_diferentes;
        }

        if(isset($params['palavra_chave']))
        {
            $matriz_palavra = array();
            if($params['palavra_chave']!="")
            {
                $queryTurmas = TurmasModel::select('*');
                $turmas = $queryTurmas
                    ->where('nome','like',"%$params[palavra_chave]%")
                    ->get();
                foreach ($turmas as $dados_turmas)
                {
                    $queryMatriz = MatrizCurricularModel::select('*');
                    $matriz = $queryMatriz
                        ->where('id_turma',$dados_turmas->id)
                        ->get();
                    foreach ($matriz as $dados_matriz)
                    {
                        $matriz_palavra[] = $dados_matriz->id;
                    }
                }

                $queryDisciplinas = DisciplinasModel::select('*');
                $disciplinas = $queryTurmas
                    ->where('nome','like',"%$params[palavra_chave]%")
                    ->get();
                foreach ($disciplinas as $dados_disciplinas)
                {
                    $queryMatriz = MatrizCurricularModel::select('*');
                    $matriz = $queryMatriz
                        ->where('id_disciplina',$dados_disciplinas->id)
                        ->get();
                    foreach ($matriz as $dados_matriz)
                    {
                        $matriz_palavra[] = $dados_matriz->id;
                    }
                }
            }
            $matriz_valida=$matriz_palavra;
        }

        if(isset($params['aulas']))
        {
            $matriz_aulas=array();
            if($params['aulas']!="")
            {
                $queryaulas = PerguntasMatrzModel::select('*');
                $aulas = $queryaulas
                    ->where('qtde_aulas',$params['aulas'])
                    ->get();
                foreach ($aulas as $dados_aulas)
                {
                    $matriz_aulas[]=$dados_aulas->id;
                }
            }
            $matriz_valida=$matriz_aulas;
        }

        $remover = array();
        foreach ($matriz_valida as $dados_valida)
        {
            if(isset($matriz_disciplina))
            {
                $d=1;
                if(in_array($dados_valida,$matriz_disciplina)==false)
                {
                  $remover[]=$dados_valida;
                }
            }

            if(isset($matriz_professor))
            {
                $d=1;
                if(in_array($dados_valida,$matriz_professor)==false)
                {
                    $remover[]=$dados_valida;
                }
            }

            if(isset($matriz_ideal))
            {
                $d=1;
                if(in_array($dados_valida,$matriz_ideal)==false)
                {
                    $remover[]=$dados_valida;
                }
            }

            if(isset($matriz_aceitavel))
            {
                $d=1;
                if(in_array($dados_valida,$matriz_aceitavel)==false)
                {
                    $remover[]=$dados_valida;
                }
            }

            if(isset($matriz_aulas))
            {
                $d=1;
                if(in_array($dados_valida,$matriz_aulas)==false)
                {
                    $remover[]=$dados_valida;
                }
            }

            if(isset($matriz_palavra))
            {
                $d=1;
                if(in_array($dados_valida,$matriz_palavra)==false)
                {
                    $remover[]=$dados_valida;
                }
            }

            if(isset($matriz_diferentes))
            {
                $d=1;
                if(in_array($dados_valida,$matriz_diferentes)==false)
                {
                    $remover[]=$dados_valida;
                }
            }

            $retorno=array_diff($matriz_valida,$remover);

            $this->model= new MatrizCurricularModel();
            $duplas=$this->model->searchId($retorno);
            $dupla= array();
            foreach($duplas as $dados_duplas)
            {
                $dupla[]= $dados_duplas->id;
            }
            $this->model = new PerguntasEspecificasModel();
            $teste = $this->model->busca($dupla);
            return $teste;
        }

    }

    public function edit()
    {

        $this->model = new PerguntasEspecificasModel();
        $opcoes = $this->model->edit();

        return $opcoes;
    }

    public function change(Request $request)
    {
        $params = $request->input();
        $this->model = new PerguntasEspecificasModel();
        $opcoesAceitaveis = $this->model->exibeAceitavel($params['tipo']);

        return $opcoesAceitaveis;
    }

    public function opcoesValidas(Request $request)
    {
        $params = $request->input();

        $queryMatriz = PerguntasMatrzModel::select('*');
        $matriz = $queryMatriz
            ->where('id_disciplina', $params['id_disciplina'])
            ->where('id_turma',$params['id_turma'])
            ->get();

        foreach ($matriz as $dados_matriz)
        {
            $queryMatriz = PerguntasMatrzModel::select('*');
            $aulas = $queryMatriz
                ->where('qtde_aulas',$dados_matriz->qtde_aulas)
                ->count();


                $queryTipoValidacao = TipoValidacaoModel::select('*');
                $tipoValidacao = $queryTipoValidacao
                    ->where('minimo_dias','<=',$aulas)
                    ->get();

        }

        $tipos_validos = array();
        foreach ($tipoValidacao as $dados_validacao)
        {
            $tipos_validos[] =$dados_validacao->id_tipo;
        }

        $queryGrupoOpcoes = PerguntaTipoEspecificoOpcoesModel::select('*');
        $grupoOpcoes = $queryGrupoOpcoes
            ->groupBy('grupo_opcoes')
            ->orderBy('ordem')
            ->get();

        foreach($grupoOpcoes as $dados_grupo)
        {
            $queryOpcoes = PerguntaTipoEspecificoOpcoesModel::select('*');
            $idExiste = $queryOpcoes

                ->where('grupo_opcoes', $dados_grupo->grupo_opcoes)
                ->exists();

            if($idExiste!=false) {
                $teste=array();
                $queryOpcoes = PerguntaTipoEspecificoOpcoesModel::select('id', 'texto', 'tipo_opcao', 'ordem', 'id_tipo');
                $opcoes = $queryOpcoes
                    ->where('grupo_opcoes', $dados_grupo->grupo_opcoes)
                    ->get();
                foreach($opcoes as $dados_opcoes)
                {
                    $teste['id']=$dados_opcoes->id;
                    $teste['texto']=$dados_opcoes->tesxto;
                    $teste['tipo_opcao']=$dados_opcoes->tipo_opcao;
                    $teste['ordem']=$dados_opcoes->ordem;
                    $teste['id_tipo']=$dados_opcoes->id_tipo;
                    if(in_array($teste['id_tipo'],$tipos_validos))
                    {
                        $teste['valido'] = 1;
                    }
                    else{
                        $teste['valido'] = 0;
                    }
                    $opcao[] = array_merge($teste);
                }

                //return $teste;
                $searchReturn[] = array_merge(['grupos_opcoes' => $dados_grupo->grupo_opcoes, "opcao_ideal" => [$opcao]]);
            }

        }



        return $searchReturn;
    }

    public function save(Request $request)
    {
        $params = $request->input();
        $this->model = new PerguntasMatrzModel();
        $idExiste = $this->model->where('id',$params['id_matriz'])->exists();
        if($idExiste==false)
        {
            return ['created' => false, 'code' => '1701', 'status' => '00'];
        }

        $this->model = new PerguntasEspecificaOpcoesModel();
        $idExiste = $this->model->where('id',$params['id_opcao'])->exists();
        if($idExiste==false)
        {
            return ['created' => false, 'code' => '1702', 'status' => '00'];
        }

        $this->model = new PerguntasOpcoesAceitaveisModel();
        $idExiste = $this->model->where('id',$params['id_aceitavel'])->exists();
        if($idExiste==false)
        {
            return ['created' => false, 'code' => '1703', 'status' => '00'];
        }

        $this->model = new PrefDistribuicaoAulasEspecificoModal();
        $idExiste = $this->model->where('id_matriz_curricular',$params['id_matriz']);
        if($idExiste==false) {
            $this->model->id_matriz_curricular = $params['id_matriz'];
            $this->model->tipo_ideal = $params['id_opcao'];
            $this->model->tipo_aceitavel = $params['id_aceitavel'];
            $this->model->save();
            return ['created' => true, 'code' => '1700', 'status' => '01', 'id' => $this->model->id];
        }
        else
        {
            $this->model = new PrefDistribuicaoAulasEspecificoModal();
            $this->model->edit($params);
        }


    }




}
