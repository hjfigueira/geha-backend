<?php 

namespace App\Modules\PerguntasBasicas\Models;

use App\Models\WsClientModel;
use DB;

class DiasAulaModel extends WsClientModel
{
	// Table in database
	protected $table = 'Dias_Aula';

    protected $primaryKey = 'id_dia';

	public function esquemas()
    {
        return $this->hasMany(
            'App\Modules\Esquemas\Models\EsquemaAtividadesModel',
            'id_dia'
        );
    }

}