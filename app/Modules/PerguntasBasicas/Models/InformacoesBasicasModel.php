<?php 

namespace App\Modules\PerguntasBasicas\Models;

use Illuminate\Database\Eloquent\Model;

class InformacoesBasicasModel extends Model
{
	// Disable Laravel timestamp
	public $timestamps = false;

	// Table in database
	protected $table = 'Informacoes_Basicas';

	// Connection Database
	protected $connection = 'urania-cliente';

	public static function isFilled()
    {
        return self::count() == 1;
    }
}