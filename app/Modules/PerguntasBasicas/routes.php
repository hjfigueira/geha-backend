<?php
/*
|--------------------------------------------------------------------------
| PerguntasBasicas Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the PerguntasBasicas module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group([
    'prefix' => 'api/perguntas-basicas',
    'namespace' => 'App\Modules\PerguntasBasicas\Controllers',
    'middleware' => 'auth'
], function () {
	Route::post('save', ['as' => 'api/perguntas-basicas.save', 'uses' => 'ApiController@save']);
	Route::get('get', 	['as' => 'api/perguntas-basicas.get',  'uses' => 'ApiController@get']);
	Route::get('get-dias-aula/{semana}', ['as' => 'api/perguntas-basicas.getDiasAula',  'uses' => 'ApiController@getDiasAula']);
});
