<?php


/**
 * @SWG\Get(
 *     path="/perguntas-basicas/get",
 *     summary="Retorna as informações básicas do sistema.",
 *     description="Retorna as informações básicas do sistema como os dias da semana e quantidade de semanas.",
 *     produces={"application/json"},
 *     tags={"perguntas-basicas.basico"}
 * )
 */


/**
 * @SWG\Get(
 *     path="/perguntas-basicas/get-dias-aula/{semana}",
 *     summary="Retorna as informações básicas do sistema.",
 *     description="Retorna as informações básicas do sistema como os dias da semana e quantidade de semanas.",
 *     produces={"application/json"},
 *     tags={"perguntas-basicas.basico"}
 * )
 */


/**
 *  
 * @SWG\Get(
 *     path="/api/perguntas-basicas/get-dias-aula",
 *     description="Retorna as informações básicas da instituição",
 *     produces={"application/json"},
 *     tags={"api"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK"
 *     ),
 *     @SWG\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 * /
 *
 * @SWG\Post(
 *     path="/api/perguntas-basicas/save",
 *     description="Salva as informações básicas da instituição",
 *     produces={"application/json"},
 *     tags={"api"},
 *     @SWG\Response(
 *         response=0200,
 *         description="OK"
 *     ),
 *     @SWG\Response(
 *         response=0201,
 *         description="assagem de paramêtros insuficientes.
 *         Verifique os dados enviados e tente nov
 *         amente.",
 *     )
 * )