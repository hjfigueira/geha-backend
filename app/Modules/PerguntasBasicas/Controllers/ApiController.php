<?php

namespace App\Modules\PerguntasBasicas\Controllers;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\PerguntasBasicas\Models\DiasAulaModel;
use Illuminate\Database\QueryException as QueryException;
use App\Modules\PerguntasBasicas\Models\InformacoesBasicasModel;

/**
 * Class PerguntasBasicas ApiController
 *
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{

    protected $model;

    /**
     * 
     */
    public function __construct()
    {
        $this->model = new InformacoesBasicasModel();
    }

    /**
     * Salva as informações básicas da instituição
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->input('params');

        if ($params['semana']      == '' || 
            $params['instituicao'] == '' || 
            empty($params['dias']) ) {
  
            $error['code']     = '0201';
            $error['status']   = '00';
            //$error['message']  = 'Passagem de paramêtros insuficientes. ';
            //$error['message'] .= 'Verifique os dados enviados e tente novamente';
            
            return json_encode($error);
        }

        $model = $this->getModel();
        
        $ret = $this->saveDiasAula($params['dias']);

        if ($ret == false) {
            $error['code']     = '0202';
            $error['status']   = '00';
            //$error['message']  = 'Não foi possível remover dia da semana. ';
            //$error['message'] .= 'Verifique se o dia removido contém esquemas cadastrado';
            return $error;
        }

        $model->qtde_semanas  = $params['semana'];
        $model->nome          = $params['instituicao'];
        $model->qtde_dias     = $this->getCountDiasAula($params['dias']);

        $model->save();

        $response['code']    = '0200';
        $response['status']  = '01';
        $response['message'] = 'OK';

        return $response;
    }

    /**
     * Retorna os dados ou model da informações básicas da instituição
     * @param  boolean $responseModel  Flag para indicar que deve retornar o model
     * @return mixed
     */
    public function get()
    {
        $data = $this->model->first();  
        $dias = DiasAulaModel::get();

        $semanas = $this->getDiasAula($data['qtde_semanas']);

        $values['dias']        = $semanas;
        //$values['instituicao'] = $data['nome'];          
        $values['qtd_semanas'] = $data['qtde_semanas'];          

        return $values;
    }

    /**
     * Retorna os dias que terão aula na instituição
     * @todo  Específicar o porquê dessa regra de remoção em um função Get
     * @return mixed
     */
    public function getDiasAula($semana)
    {
        $semanas = $this->getDiasDefault($semana);
        $model   = new DiasAulaModel();

        // O que seria isso??
        if($semana == 1)
        {
            DB::beginTransaction();

            $toRemove = DiasAulaModel::where('id_dia','>=',8)->get();

            foreach($toRemove as $item)
            {
                if($item->esquemas()->count() >= 1)
                {
                    DB::rollback();
                }
                else{
                    $item->delete();
                }
            }

            DB::commit();
        }

        foreach ($semanas as $i => &$semana) {

            foreach ($semana as $k => &$dia) {

                $search = $model->where('id_dia', $dia['id'])->first();

                $dia['check'] = ($search == null) ? 0 : 1;
            }
        }

        return $semanas;
    }


    /* {Private Functions} */



    /**
     * Retorna a quantidade de dias com aula vindo do formulário
     * @return integer
     */
    protected function getCountDiasAula($dias)
    {
        $diasComAula = 0;

        foreach ($dias as $diasDaSemana) {

            foreach ($diasDaSemana as $dia) {
            
                if ($dia['check'] == 0) continue;
                
                $diasComAula++;                
            }
        }

        return $diasComAula;
    }

    /**
     * Salva os dias usados para esquemas
     * @return integer
     */
    protected function saveDiasAula($dias)
    {
        foreach ($dias as $dia) {
            foreach ($dia as $key => $value) {
                if ($value['check'] == 0) {                    
                    if ($this->deleteDia($value) == false) {
                        return false;
                        break;
                    } 
                } else {
                    $ret = $this->insertDia($value);
                }   
            }
        }     

        return true;   
    }

    /**
     * Protocolo  2016 3648 4986 93
     * Remove os dias para esquema. 
     * Caso tenha algum esquema vinculado deve retornar um erro para o usuário
     * @return 
     */
    protected function deleteDia($value)
    {
        $model  = new DiasAulaModel();
        
        try {
            
            $obj = $model->where('id_dia', $value['id'])->first();

            if ($obj == null) return true;

            $model->where('id_dia', $value['id'])->delete();

            return true;

        } catch (QueryException $e) {
            return false;            
        }            
    }

    /**
     * Insere os dias de esquema 
     * @param  $value 
     * @return        
     */
    protected function insertDia($value)
    {
        $model  = new DiasAulaModel();

        $obj = ($model->where('id_dia', $value['id'])->first() != null ) ? 
                $model->where('id_dia', $value['id'])->first() :
                $model;

        $obj->id_dia = $value['id'];
        $obj->nome   = $value['name'];
                
        $obj->save();      

        return true;          
    }


    /**
     * Retorna o model específico para a atualização dos dados básicos da instituição
     * @return InformacoesBasicasModel   
     */
    protected function getModel()
    {
        $data = $this->model->first();  

        return ($data == null) ? $this->model : $data;
    }


    /**
     * Retorna os dias da semana se não tiver nada cadastrado ainda no DB
     * @param  int  $semana 
     * @return array        
     */
    public function getDiasDefault($semana)
    {
        $dias[0][1]  = ['id' => 1, 'abr' => 'seg', 'semana' => 1, 'check' => 0, 'name' => 'segunda'];
        $dias[0][2]  = ['id' => 2, 'abr' => 'ter', 'semana' => 1, 'check' => 0, 'name' => 'terca'];
        $dias[0][3]  = ['id' => 3, 'abr' => 'qua', 'semana' => 1, 'check' => 0, 'name' => 'quarta'];
        $dias[0][4]  = ['id' => 4, 'abr' => 'qui', 'semana' => 1, 'check' => 0, 'name' => 'quinta'];
        $dias[0][5]  = ['id' => 5, 'abr' => 'sex', 'semana' => 1, 'check' => 0, 'name' => 'sexta'];
        $dias[0][6]  = ['id' => 6, 'abr' => 'sab', 'semana' => 1, 'check' => 0, 'name' => 'sabado'];
        $dias[0][7]  = ['id' => 7, 'abr' => 'dom', 'semana' => 1, 'check' => 0, 'name' => 'domingo'];

        if ($semana == 1) return $dias;

        $dias[1][8]  = ['id' => 8,  'abr' => 'seg', 'semana' => 2, 'check' => 0, 'name' => 'segunda'];
        $dias[1][9]  = ['id' => 9,  'abr' => 'ter', 'semana' => 2, 'check' => 0, 'name' => 'terca'];
        $dias[1][10] = ['id' => 10, 'abr' => 'qua', 'semana' => 2, 'check' => 0, 'name' => 'quarta'];
        $dias[1][11] = ['id' => 11, 'abr' => 'qui', 'semana' => 2, 'check' => 0, 'name' => 'quinta'];
        $dias[1][12] = ['id' => 12, 'abr' => 'sex', 'semana' => 2, 'check' => 0, 'name' => 'sexta'];
        $dias[1][13] = ['id' => 13, 'abr' => 'sab', 'semana' => 2, 'check' => 0, 'name' => 'sabado'];
        $dias[1][14] = ['id' => 14, 'abr' => 'dom', 'semana' => 2, 'check' => 0, 'name' => 'domingo'];

        return $dias;
    }


}
