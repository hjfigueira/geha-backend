<?php
/**
 * Created by PhpStorm.
 * User: Bruna
 * Date: 04/04/2017
 * Time: 07:59
 */

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;
use Throwable;

class WsException extends \Exception
{
    private $wsCode;

    private $wsVerb;

    public function __construct( $wsCode, $wsVerb, Throwable $previous = null)
    {
        $this->wsCode = $wsCode;
        $this->wsVerb = $wsVerb;
        parent::__construct($wsVerb, $wsCode, $previous);
    }

    public function getWebserviceCode()
    {
        return $this->wsCode;
    }

    public function getWebserviceVerb()
    {
        return $this->wsVerb;
    }

    public function asJson()
    {
        return new JsonResponse([$this->wsVerb => 'false', 'status' => 0, 'code' => $this->wsCode]);
    }

}