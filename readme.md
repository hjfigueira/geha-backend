# API Geha Urania Web

API Urania Web serve para funcionar as funcionalides essencias para o todo o sistema web. É possível também conectar outros dispositivos na API.

## Regras da API

Sempre pule 2 linhas depois de cada função

Nunca retorne status diferente 200. Isso dificulta a manipulação do retorno do webservice

## Atualizar a Documentação

Para atualizar a documentação do swagger, utilize o comando abaixo :

`` vendor/bin/swagger app/Modules/*/Docs/ --output public/docs/``