<?php
# config/module.php

return  [
    'modules' => [
       'Professor',
       'Atribuicoes',
       'User',
       'PerguntasBasicas',
       'Perguntas',
       'PerguntasEspecificas',
       'Turno',
       'Turnos',
       'Esquemas',
       'Atividades',
       'Turmas',
       'Disciplinas',
       'Professores',
        'MatrizCurricular',
        'Utils'
     ]
];
