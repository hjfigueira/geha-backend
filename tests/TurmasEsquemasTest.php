<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TurmasNomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$params = ['id_turma' => 1, 'id_turnos' => 1]; 

    	$this->post('/api/turmas-grupo/save', $params)
    		 ->seeJson(['created' => true]);
    }
}
