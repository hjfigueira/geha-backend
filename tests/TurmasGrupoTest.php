<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TurmasNomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$param = ['nome' => 'Ensino médio']; 

    	$this->post('/api/turmas-grupo/save', $params)
    		 ->seeJson(['created' => true]);
    }
}
