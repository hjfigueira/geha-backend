<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TurmasNomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$params = ['nome' => 'Teste']; 

    	$this->post('/api/disciplinas/save', $params)
    		 ->seeJson(['created' => true]);
    }
}
