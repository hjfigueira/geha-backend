<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TurmasNomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$params = ['nome' => 'Giuliano Sampaio']; 

    	$this->post('/api/turmas/save', $params)
    		 ->seeJson(['created' => true]);
    }
}
