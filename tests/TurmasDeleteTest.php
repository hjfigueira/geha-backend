<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TurmasDeleteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
       	$params['params'] = ['id' => 1]; 

    	$this->post('/api/turmas/delete', $params)
    		 ->seeJson(['removed' => true]);
    }
}
